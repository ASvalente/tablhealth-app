package magaldinnova.techTablHealth.library;

/**
 * Interfaccia MAC Device Medici
 * @author Magaldi Innova srl
 */
public interface BluetoothUtils {

    public String OSSIMETRO = "Ossimetro";
    public String MAC_OSSIMETRO = "94:21:97";
    public String SFIGMOMANOMETRO = "Sfigmomanometro";
    public String MAC_SFIGMOMANOMETRO = "00:12:A1";
    public String GLUCOMETRO = "Glucometro";
    public String MAC_GLUCOMETRO = "00:12:3E";
    public String BILANCIA = "Bilancia";
    public String MAC_BILANCIA = "8C:DE:52";

    public String MAC_TEST = "35:53:2A";
}
