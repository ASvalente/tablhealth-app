package magaldinnova.techTablHealth.moduli;

import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import magaldinnova.techTablHealth.database.record.BloodPressureRecord;

/**
 * Protocollo accesso Dati Sfigmomanometro
 * @author Magaldi Innova srl
 */
public class TD3128 {

    static String MAC = "";
    static int[] device= new int[]{81,84,0,0,0,0,165,74};

    public TD3128(){}

    /**
     * Creazione Comando per misurazione parte 1
     * @param dataIndex
     * @return
     */
    public static byte[] createTx25Cmd(int dataIndex)
    {
        isOutOfIndex(dataIndex);
        int dataLowIndex = convertDataLowIndex(dataIndex);
        int dataHighIndex = convertDataHighIndex(dataIndex);
        int[] cmd = appendOneByteCheckSumToCmd(new int[] { 81, 37, dataLowIndex, dataHighIndex,
                0, 1, 163 });
        Log.d("createTx25Cmd", convertToHexString(cmd));
        return intArrayToByteArray(cmd);
    }


    /**
     * Creazione Comando per misurazione parte 2
     * @param dataIndex
     * @return
     */
    public static byte[] createTx26Cmd(int dataIndex)
    {
        isOutOfIndex(dataIndex);

        int dataLowIndex = convertDataLowIndex(dataIndex);
        int dataHighIndex = convertDataHighIndex(dataIndex);

        int[] cmd = appendOneByteCheckSumToCmd(new int[] { 81, 38, dataLowIndex, dataHighIndex,
                0, 1, 163 });
        Log.d("createTx26Cmd", convertToHexString(cmd));
        return intArrayToByteArray(cmd);
    }

    // Risposta
    public static BloodPressureRecord getBloodPressureRec(int user, int[] rx25Cmd, int[] rx26Cmd)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/yyyy HH:mm:ss");
        Date measureTime = convertRxCmdToDateObj(rx25Cmd);

        boolean arrhy = false;
        if ((rx25Cmd[4] >> 6 & 0x1) == 1) {
            arrhy = true;
        }

        boolean eve = false;
        if ((rx25Cmd[5] >> 5 & 0x1) == 1) {
            eve = true;
        }

        boolean transmitted = false;
        if ((rx25Cmd[5] >> 6 & 0x1) == 1) {
            transmitted = true;
        }

        boolean avg = false;
        if (rx25Cmd[5] >> 7 == 1) {
            avg = true;
        }

        int systolicValue = rx26Cmd[2];
        int mapValue = rx26Cmd[3];
        int diastolicValue = rx26Cmd[4];
        int pulseValue = rx26Cmd[5];

        Log.d("MeterCmdService", "User (0:CurrentUser , 1:User1 , 2:User2 , 3:User3 , 4:User4) : " + user);
        Log.d("MeterCmdService", "Measure Time : " + measureTime);
        Log.d("MeterCmdService", "Is Arrhythmia or not(true:Arrhythmia , false:Normal) : " + arrhy);
        Log.d("MeterCmdService",
                "Is evening time measurement(true:Evening time measurement, false:Day time measurement) : " +
                        eve);
        Log.d("MeterCmdService", "The reading has been transmitted or not : " + transmitted);
        Log.d("MeterCmdService", "Is average measurement reading(true:Average measurement reading, false:Single measurement reading) : " + avg);
        Log.d("MeterCmdService", "Systolic Value : " + systolicValue);
        Log.d("MeterCmdService", "MAP Value : " + mapValue);
        Log.d("MeterCmdService", "Diastolic Value : " + diastolicValue);
        Log.d("MeterCmdService", "Pulse Value : " + pulseValue);

        int ihb = 0;

        ihb = (rx25Cmd[5] & 0x60) >> 5;
        if (arrhy)
        {
            if (ihb == 0) {
                ihb = 1;
            }
            else {
                ihb++;
            }
        }

        return new BloodPressureRecord(measureTime, user, systolicValue, diastolicValue,
                mapValue, pulseValue, arrhy, transmitted, avg, eve, ihb);
    }


    /**
     * Conversione da int[] a Hex
     * @param cmd
     * @return
     */
    public static String convertToHexString(int[] cmd)
    {
        StringBuffer resultStr = new StringBuffer("[");
        for (int i = 0; i < cmd.length; i++) {
            if (i != cmd.length - 1)
                resultStr.append(Integer.toHexString(cmd[i]) + ", ");
            else {
                resultStr.append(Integer.toHexString(cmd[i]));
            }
        }
        resultStr.append("]");
        return resultStr.toString();
    }

    /**
     * Creazione index fine
     * @param dataIndex
     * @return
     */
    private static int convertDataHighIndex(int dataIndex)
    {
        int dataHighIndex = 0;
        if (dataIndex > 255) {
            dataHighIndex = dataIndex >> 8;
        }
        return dataHighIndex;
    }

    /**
     * Creazione index inizio
     * @param dataIndex
     * @return
     */
    private static int convertDataLowIndex(int dataIndex)
    {
        int dataLowIndex = 0;
        if (dataIndex > 255)
            dataLowIndex = dataIndex & 0xFF;
        else {
            dataLowIndex = dataIndex;
        }
        return dataLowIndex;
    }

    /**
     * Conversione int[] a Data
     * @param rxCmd
     * @return
     */
    private static Date convertRxCmdToDateObj(int[] rxCmd)
    {
        int day = 0;
        int month = 0;
        int year = 0;
        int minute = 0;
        int hour = 0;

        switch (rxCmd[1]) {
            case 35:
            case 37:
                day = rxCmd[2] & 0x1F;
                month = (rxCmd[2] >> 5) + ((rxCmd[3] & 0x1) << 3);

                year = (rxCmd[3] >> 1) + 2000;

                minute = rxCmd[4] & 0x3F;
                hour = rxCmd[5] & 0x1F;
                break;
            case 41:
                day = rxCmd[2] & 0x1F;
                month = (rxCmd[2] >> 5) + ((rxCmd[3] & 0x1) << 3);

                year = (rxCmd[3] >> 1) + 2000;

                break;
            case 132:
                day = rxCmd[4] & 0x1F;
                month = (rxCmd[4] >> 5) + ((rxCmd[5] & 0x1) << 3);

                year = ((rxCmd[5] & 0x1E) >> 1) + 2000;
                minute = rxCmd[6] & 0x3F;
                hour = rxCmd[7] & 0x1F;
                break;
            case 113:
                day = rxCmd[6];
                month = rxCmd[5];
                year = rxCmd[4] + 2000;
                minute = rxCmd[8];
                hour = rxCmd[7];
        }

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month - 1);
        calendar.set(Calendar.DAY_OF_MONTH, day);
        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }

    /**
     * Calcolo CRC
     * @param sourceCmd
     * @return
     */
    public static int[] appendOneByteCheckSumToCmd(int[] sourceCmd)
    {
        int checkSum = calculateOneByteCheckSum(sourceCmd, 0, sourceCmd.length - 1);
        int[] cmdWithCheckSum = new int[sourceCmd.length + 1];
        System.arraycopy(sourceCmd, 0, cmdWithCheckSum, 0, sourceCmd.length);
        cmdWithCheckSum[(cmdWithCheckSum.length - 1)] = checkSum;
        return cmdWithCheckSum;
    }

    /**
     * Calcolo CRC
     * @param cmd
     * @return
     */
    public static int calculateOneByteCheckSum(int[] cmd, int startIndex, int endIndex)
    {
        int checkSum = 0;
        for (int i = startIndex; i <= endIndex; i++) {
            checkSum += cmd[i];
        }
        return checkSum & 0xFF;
    }

    /**
     * Controllo Indici
     * @param dataIndex
     */
    private static void isOutOfIndex(int dataIndex)
    {
        if ((dataIndex > 65535) || (dataIndex < 0))
            throw new RuntimeException("Data index must between 0 and 65535.");
    }


    /**
     * Conversione da int[] a byte[]
     * @param src
     * @return
     */
    public static byte[] intArrayToByteArray(int[] src)
    {
        int srcLength = src.length;
        byte[] dst = new byte[srcLength];

        for (int i = 0; i < srcLength; i++) {
            dst[i] = ((byte)src[i]);
        }

        return dst;
    }

    public static String getMAC() {
        return MAC;
    }

    public static void setMAC(String MAC) {
        TD3128.MAC = MAC;
    }

    public static int[] getDevice() {
        return device;
    }

    public static void setDevice(int[] device) {
        TD3128.device = device;
    }
}
