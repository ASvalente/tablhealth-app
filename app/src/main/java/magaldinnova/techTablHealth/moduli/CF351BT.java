package magaldinnova.techTablHealth.moduli;

import android.util.Log;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.Date;

import magaldinnova.techTablHealth.database.record.WeightRecord;

/**
 * Protocollo accesso Dati Bilancia
 * @author Magaldi Innova srl
 */
public class CF351BT {

    static String MAC = "";

    public CF351BT(){}

    public static byte[] createTxFECmd(){
        int[] cmd = appendOneByteCheckSumToCmd(new int[]{254, 0, 254, 0, 130, 10, 1});
        Log.d("createTxFECmd", convertToHexString(cmd));
        return intArrayToByteArray(cmd);
    }

    public static WeightRecord getWeightRecord(int[] rxFECmd){
        SimpleDateFormat sdf = new SimpleDateFormat("dd/mm/yyyy HH:mm:ss");

        if(rxFECmd.length>=16){
            Date measureTime = new Date();
            int scale_type = rxFECmd[1];
            int eta = rxFECmd[2];
            int altezza = rxFECmd[3];
            double peso = round(((rxFECmd[4] << 8) + rxFECmd[5])* 0.1);
            double grasso_corporeo = round(((rxFECmd[6] << 8) + rxFECmd[7])* 0.1);
            double ossa = round((rxFECmd[8])* 0.1);
            double muscoli = round(((rxFECmd[9] << 8) + rxFECmd[10])* 0.1);
            int grasso_viscerale = rxFECmd[11];
            double acqua_corporea = round(((rxFECmd[12] << 8) + rxFECmd[13])* 0.19);
            int bmr = ((rxFECmd[14] << 8) + rxFECmd[15]);
            double bmi = round((peso /(altezza * altezza))*(double)10000);

            Log.d("getWeightRecord", "Scale Type (0:Normal , 1:Amateur , 2:Professional) : " + scale_type);
            Log.d("getWeightRecord", "Eta : " + eta);
            Log.d("getWeightRecord", "Altezza : " + altezza);
            Log.d("getWeightRecord", "Peso : " + peso);
            Log.d("getWeightRecord", "Grasso Corporeo : " + grasso_corporeo);
            Log.d("getWeightRecord", "Massa Ossea : " + ossa);
            Log.d("getWeightRecord", "Massa Muscolare : " + muscoli);
            Log.d("getWeightRecord", "Grasso Viscerale : " + grasso_viscerale);
            Log.d("getWeightRecord", "Acqua Corporea : " + acqua_corporea);
            Log.d("getWeightRecord", "Indice Metabolismo Basale : " + bmr);
            Log.d("getWeightRecord", "Indice Massa Corporea : " + bmi);
            return new WeightRecord(measureTime, scale_type, eta, altezza, peso, grasso_corporeo, ossa, muscoli,grasso_viscerale, acqua_corporea, bmr, bmi);
        }else{
            return null;
        }
    }


    /**
     * Calcolo CRC
     * @param sourceCmd
     * @return
     */
    public static int[] appendOneByteCheckSumToCmd(int[] sourceCmd)
    {
        int checkSum = calculateOneByteCheckSum(sourceCmd, 1, sourceCmd.length - 1);
        int[] cmdWithCheckSum = new int[sourceCmd.length + 1];
        System.arraycopy(sourceCmd, 0, cmdWithCheckSum, 0, sourceCmd.length);
        cmdWithCheckSum[(cmdWithCheckSum.length - 1)] = checkSum;
        return cmdWithCheckSum;
    }

    /**
     * Calcolo CRC
     * @param cmd
     * @return
     */
    public static int calculateOneByteCheckSum(int[] cmd, int startIndex, int endIndex)
    {
        int checkSum = 0;
        for (int i = startIndex; i <= endIndex; i++) {
            checkSum ^= cmd[i];
        }
        return checkSum & 0xFF;
    }

    /**
     * Conversione da int[] a byte[]
     * @param src
     * @return
     */
    public static byte[] intArrayToByteArray(int[] src)
    {
        int srcLength = src.length;
        byte[] dst = new byte[srcLength];

        for (int i = 0; i < srcLength; i++) {
            dst[i] = ((byte)src[i]);
        }

        return dst;
    }

    /**
     * Conversione da int[] a Hex
     * @param cmd
     * @return
     */
    public static String convertToHexString(int[] cmd)
    {
        StringBuffer resultStr = new StringBuffer("[");
        for (int i = 0; i < cmd.length; i++) {
            if (i != cmd.length - 1)
                resultStr.append(Integer.toHexString(cmd[i]) + ", ");
            else {
                resultStr.append(Integer.toHexString(cmd[i]));
            }
        }
        resultStr.append("]");
        return resultStr.toString();
    }

    public static double round(double value) {
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(2, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public void setMAC(String MAC) {
        CF351BT.MAC = MAC;
    }
}
