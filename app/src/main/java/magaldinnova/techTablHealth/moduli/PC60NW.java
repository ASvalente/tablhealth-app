package magaldinnova.techTablHealth.moduli;

import java.util.Date;

import magaldinnova.techTablHealth.database.record.SpO2Record;

/**
 * Protocollo accesso Dati Ossimetro
 * @author Magaldi Innova srl
 */
public class PC60NW {

    public PC60NW(){ }

    /**
     * Creazione Comando Misurazione
     * @return
     */
    public static byte[] createTx03Cmd()
    {
        //Log.d("createTx03Cmd", convertToHexString(new int[] { 170, 85, 15, 2, 132, 1, 14}));
        int[]cmd = new int[] { 170, 85, 15, 2, 132, 1, 14};
        return intArrayToByteArray(cmd);
    }


    /**
     * Risposta Misurazione
     * @param rx03Cmd
     * @return
     */
    public static SpO2Record getSpO2Rec(int[] rx03Cmd){
        int spO2_res = 0;
        int pulsazioni_res = 0;
        double perfIndex_res = 0;
        int alarm_res = 0;
        double batteria_res = 0;
        Date measureTime = new Date();

        if (rx03Cmd.length >= 12) {
            spO2_res = rx03Cmd[5];
            pulsazioni_res = rx03Cmd[6];
            perfIndex_res = 0.1 * rx03Cmd[8];
            alarm_res = rx03Cmd[9];
            batteria_res = (100 * (0.1 * rx03Cmd[10]) / 3);
        }
        return new SpO2Record(measureTime, spO2_res, pulsazioni_res, perfIndex_res, String.valueOf(alarm_res), batteria_res);
    }

    /**
     * Conversione da int[] a byte[]
     * @param src
     * @return
     */
    public static byte[] intArrayToByteArray(int[] src)
    {
        int srcLength = src.length;
        byte[] dst = new byte[srcLength];

        for (int i = 0; i < srcLength; i++) {
            dst[i] = ((byte)src[i]);
        }

        return dst;
    }
}
