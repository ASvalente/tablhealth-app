package magaldinnova.techTablHealth.database.dbHelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import io.requery.android.database.sqlite.SQLiteDatabase;
import io.requery.android.database.sqlite.SQLiteOpenHelper;
import magaldinnova.techTablHealth.database.oggetti.DataPoint;
import magaldinnova.techTablHealth.database.oggetti.Dispositivi;
import magaldinnova.techTablHealth.library.BluetoothUtils;

/**
 * DatabaseHelper
 * @author Magaldi Innova srl
 */
public class DatabaseHelper extends SQLiteOpenHelper implements BluetoothUtils {

    // Logcat
    private static final String LOG = "DatabaseHelper";
    private static final String SYNC_NOTSEND = "NotSend - ";
    private static final String SYNC_SEND = "Send - ";

    // Versione Database
    private static final int DATABASE_VERSION = 1;

    // Nome Database
    private static final String DATABASE_NAME = "techTablHealth.db";

    // Nomi tabelle
    private static final String TABLE_DEVICE = "Dispositivi";
    private static final String TABLE_OSSIMETRO = "Ossimetro";
    private static final String TABLE_SFIGMOMANOMETRO = "Sfigmomanometro";
    private static final String TABLE_GLUCOMETRO = "Glucometro";
    private static final String TABLE_BILANCIA = "Bilancia";

    // Nomi Colonne Comuni
    private static final String KEY_ID = "_id";
    private static final String KEY_IDASSISTED = "idAssisted";
    private static final String KEY_DATA = "data";
    private static final String KEY_NOME = "nome";
    private static final String KEY_MAC = "mac";
    private static final String KEY_DATANAME = "dataName";
    private static final String KEY_DATAVALUE = "dataValue";
    private static final String KEY_DATATYPE = "dataType";
    private static final String KEY_DATAUNIT = "dataUnit";
    private static final String KEY_SEND = "send";


    // ------------------------ Creazione Tabelle ----------------//

    // Tabella DEVICE
    private static final String CREATE_TABLE_DEVICES = "CREATE TABLE IF NOT EXISTS "
            + TABLE_DEVICE
            + " ( "
            + KEY_ID + " INTEGER PRIMARY KEY NOT NULL UNIQUE, "
            + KEY_NOME + " VARCHAR(20), "
            + KEY_MAC + " VARCHAR(20)"
            + " ) ";

    // Tabella OSSIMETRO
    private static final String CREATE_TABLE_OSSIMETRO = "CREATE TABLE IF NOT EXISTS "
            + TABLE_OSSIMETRO
            + " ( "
            + KEY_ID + " INTEGER PRIMARY KEY NOT NULL UNIQUE, "
            + KEY_IDASSISTED + " INTEGER, "
            + KEY_DATA + " VARCHAR(20) NOT NULL, "
            + KEY_DATANAME + " VARCHAR(20), "
            + KEY_DATAVALUE + " INTEGER, "
            + KEY_DATATYPE + " VARCHAR(20), "
            + KEY_DATAUNIT + " VARCHAR(20),"
            + KEY_SEND + " VARCHAR(20) "
            + " ) ";

    // Tabella SFIGMOMANOMETRO
    private static final String CREATE_TABLE_SFIGMOMANOMETRO = "CREATE TABLE IF NOT EXISTS "
            + TABLE_SFIGMOMANOMETRO
            + " ( "
            + KEY_ID + " INTEGER PRIMARY KEY NOT NULL UNIQUE, "
            + KEY_IDASSISTED + " INTEGER, "
            + KEY_DATA + " VARCHAR(20) NOT NULL, "
            + KEY_DATANAME + " VARCHAR(20), "
            + KEY_DATAVALUE + " INTEGER, "
            + KEY_DATATYPE + " VARCHAR(20), "
            + KEY_DATAUNIT + " VARCHAR(20),"
            + KEY_SEND + " VARCHAR(20) "
            + " ) ";

    // Tabella GLUCOMETRO
    private static final String CREATE_TABLE_GLUCOMETRO = "CREATE TABLE IF NOT EXISTS "
            + TABLE_GLUCOMETRO
            + " ( "
            + KEY_ID + " INTEGER PRIMARY KEY NOT NULL UNIQUE, "
            + KEY_IDASSISTED + " INTEGER, "
            + KEY_DATA + " VARCHAR(20) NOT NULL, "
            + KEY_DATANAME + " VARCHAR(20), "
            + KEY_DATAVALUE + " INTEGER, "
            + KEY_DATATYPE + " VARCHAR(20), "
            + KEY_DATAUNIT + " VARCHAR(20),"
            + KEY_SEND + " VARCHAR(20) "
            + ")";

    // Tabella BILANCIA
    private static final String CREATE_TABLE_BILANCIA = "CREATE TABLE IF NOT EXISTS "
            + TABLE_BILANCIA
            + " ( "
            + KEY_ID + " INTEGER PRIMARY KEY NOT NULL UNIQUE, "
            + KEY_IDASSISTED + " INTEGER, "
            + KEY_DATA + " VARCHAR(20) NOT NULL, "
            + KEY_DATANAME + " VARCHAR(20), "
            + KEY_DATAVALUE + " INTEGER, "
            + KEY_DATATYPE + " VARCHAR(20), "
            + KEY_DATAUNIT + " VARCHAR(20),"
            + KEY_SEND + " VARCHAR(20) "
            + " ) ";


    //Costruttore
    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    /**
     * Creazione Tabelle
     *
     * @param db Database SQLite
     */
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_DEVICES);
        db.execSQL(CREATE_TABLE_OSSIMETRO);
        db.execSQL(CREATE_TABLE_SFIGMOMANOMETRO);
        db.execSQL(CREATE_TABLE_GLUCOMETRO);
        db.execSQL(CREATE_TABLE_BILANCIA);

        Log.i("Database", CREATE_TABLE_DEVICES);
        Log.i("Database", CREATE_TABLE_OSSIMETRO);
        Log.i("Database", CREATE_TABLE_SFIGMOMANOMETRO);
        Log.i("Database", CREATE_TABLE_GLUCOMETRO);
        Log.i("Database", CREATE_TABLE_BILANCIA);
    }

    /**
     * Update Tabelle Database
     *
     * @param db         Database SQLite
     * @param oldVersion Versione DB precedente
     * @param newVersion Versione DB nuova
     */
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //Creazioni tabelle richieste
        Log.i("Database", CREATE_TABLE_DEVICES);
        Log.i("Database", CREATE_TABLE_OSSIMETRO);
        Log.i("Database", CREATE_TABLE_SFIGMOMANOMETRO);
        Log.i("Database", CREATE_TABLE_GLUCOMETRO);
        Log.i("Database", CREATE_TABLE_BILANCIA);

        // on upgrade drop older tables
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DEVICE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_OSSIMETRO);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SFIGMOMANOMETRO);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_GLUCOMETRO);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BILANCIA);

        // ricrea tabelle
        onCreate(db);
    }


    public void resetDatabase() {
        SQLiteDatabase db = this.getReadableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DEVICE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_OSSIMETRO);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SFIGMOMANOMETRO);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_GLUCOMETRO);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BILANCIA);
        onCreate(db);
    }

    public void resetMisurazioni() {
        SQLiteDatabase db = this.getReadableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_OSSIMETRO);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SFIGMOMANOMETRO);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_GLUCOMETRO);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BILANCIA);
        onCreate(db);
    }

    // ------------------------ Metodi Tabella DEVICE ----------------//


    /**
     * Inserire Dispositivo in DB
     *
     * @param dispositivi dispositivo bluetooth
     * @return id DB dispositivo
     */
    public long createDevice(Dispositivi dispositivi) {
        long result = -1;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues v = new ContentValues();
            v.put(KEY_ID, (byte[]) null);
            v.put(KEY_NOME, dispositivi.getNome());
            v.put(KEY_MAC, dispositivi.getMac());
            result = db.insert(TABLE_DEVICE, null, v);
            Log.i(LOG, "createDevice ID: " + result);
            if (result == -1) {
                Log.e(LOG, "Insert failed");
            }
        } catch (SQLiteException e) {
            Log.e(LOG, "createDevice failed", e);
        } finally {
            close();
        }
        return result;
    }


    /**
     * Get Dispositivi richiesto dal nome Dispositivo
     *
     * @param name Nome Dispositivo Bluetooth
     * @return Oggetto Dispositivo
     */
    public Dispositivi getDispositivoByName(String name) {
        Dispositivi dispositivi = null;
        Cursor c = null;
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            String selectQuery = "SELECT  * FROM '" + TABLE_DEVICE + "' WHERE "
                    + KEY_NOME + " = '" + name + "'";
            Log.i(LOG, selectQuery);
            c = db.rawQuery(selectQuery, null);
            dispositivi = new Dispositivi();
            Log.e(LOG, "Size: " + c.getCount());
            if (c.getCount() != 0) {
                c.moveToFirst();
                dispositivi.setID(c.getInt(c.getColumnIndex(KEY_ID)));
                dispositivi.setNome(c.getString(c.getColumnIndex(KEY_NOME)));
                dispositivi.setMac(c.getString(c.getColumnIndex(KEY_MAC)));
            } else {
                dispositivi = null;
            }
        } catch (SQLiteException e) {
            Log.e(LOG, "getDispositivoByName failed", e);
        } finally {
            if (c != null) {
                c.close();
            }
            close();
        }
        return dispositivi;
    }

    /**
     * Get Dispositivi dal MAC
     *
     * @param MAC indirizzo fisico Dispositivo
     * @return Oggetto Dispositivo
     */
    public Dispositivi getDeviceByMAC(String MAC) {
        Dispositivi dispositivi = null;
        Cursor c = null;
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            String selectQuery = "SELECT  * FROM '" + TABLE_DEVICE + "' WHERE "
                    + KEY_MAC + " = '" + MAC.trim() + "'";
            Log.i(LOG, selectQuery);
            c = db.rawQuery(selectQuery, null);
            dispositivi = new Dispositivi();
            if (c.getCount() != 0) {
                c.moveToFirst();
                dispositivi.setID(c.getInt(c.getColumnIndex(KEY_ID)));
                if (c.getString(c.getColumnIndex(KEY_MAC)).startsWith(BluetoothUtils.MAC_OSSIMETRO)) {
                    dispositivi.setNome(BluetoothUtils.OSSIMETRO);
                } else if (c.getString(c.getColumnIndex(KEY_MAC)).startsWith(BluetoothUtils.MAC_SFIGMOMANOMETRO)) {
                    dispositivi.setNome(BluetoothUtils.SFIGMOMANOMETRO);
                } else if (c.getString(c.getColumnIndex(KEY_MAC)).startsWith(BluetoothUtils.MAC_GLUCOMETRO)) {
                    dispositivi.setNome(BluetoothUtils.GLUCOMETRO);
                } else if (c.getString(c.getColumnIndex(KEY_MAC)).startsWith(BluetoothUtils.MAC_BILANCIA)) {
                    dispositivi.setNome(BluetoothUtils.BILANCIA);
                } else if (c.getString(c.getColumnIndex(KEY_MAC)).startsWith(BluetoothUtils.MAC_TEST)) { //TEST
                    dispositivi.setNome(BluetoothUtils.OSSIMETRO);
                } else {
                    dispositivi.setNome(c.getString(c.getColumnIndex(KEY_NOME)));
                }
                dispositivi.setMac(c.getString(c.getColumnIndex(KEY_MAC)));
                Log.i("getDispositivoByMAC", dispositivi.getNome());
                Log.i("getDispositivoByMAC", dispositivi.getMac());
            } else {
                dispositivi = null;
            }
        } catch (SQLiteException e) {
            Log.e(LOG, "getDeviceByMAC failed", e);
        } finally {
            if (c != null) {
                c.close();
            }
            close();
        }
        return dispositivi;
    }

    /**
     * Get Tutti i Dispositivi
     *
     * @return Elenco Dispositivi
     */
    public ArrayList<Dispositivi> getAllDispositivi() {
        ArrayList<Dispositivi> lista = new ArrayList<>();
        Cursor c = null;
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            String selectQuery = "SELECT  * FROM " + TABLE_DEVICE;
            c = db.rawQuery(selectQuery, null);
            if (c.moveToFirst()) {
                do {
                    Dispositivi dispositivi = new Dispositivi();
                    dispositivi.setID(c.getInt(c.getColumnIndex(KEY_ID)));
                    if (c.getString(c.getColumnIndex(KEY_MAC)).startsWith(BluetoothUtils.MAC_OSSIMETRO)) {
                        dispositivi.setNome(BluetoothUtils.OSSIMETRO);
                    } else if (c.getString(c.getColumnIndex(KEY_MAC)).startsWith(BluetoothUtils.MAC_SFIGMOMANOMETRO)) {
                        dispositivi.setNome(BluetoothUtils.SFIGMOMANOMETRO);
                    } else if (c.getString(c.getColumnIndex(KEY_MAC)).startsWith(BluetoothUtils.MAC_GLUCOMETRO)) {
                        dispositivi.setNome(BluetoothUtils.GLUCOMETRO);
                    } else if (c.getString(c.getColumnIndex(KEY_MAC)).startsWith(BluetoothUtils.MAC_BILANCIA)) {
                        dispositivi.setNome(BluetoothUtils.BILANCIA);
                    } else if (c.getString(c.getColumnIndex(KEY_MAC)).startsWith(BluetoothUtils.MAC_TEST)) { //TEST
                        dispositivi.setNome(BluetoothUtils.OSSIMETRO);
                    } else {
                        dispositivi.setNome(c.getString(c.getColumnIndex(KEY_NOME)));
                    }
                    dispositivi.setMac(c.getString(c.getColumnIndex(KEY_MAC)));
                    lista.add(dispositivi);
                } while (c.moveToNext());
            }
        } catch (SQLiteException e) {
            Log.e(LOG, "getAllDispositivi failed", e);
        } finally {
            if (c != null) {
                c.close();
            }
            close();
        }
        return lista;
    }

    /**
     * Aggiorna Dispositivi
     *
     * @param dispositivi Oggetto Dispositivo
     * @return intero di controllo operazione
     */
    public int updateDevice(Dispositivi dispositivi) {
        int result = -1;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(KEY_NOME, dispositivi.getNome());
            values.put(KEY_MAC, dispositivi.getMac());
            result = db.update(TABLE_DEVICE, values, KEY_ID + " = ?",
                    new String[]{String.valueOf(dispositivi.getID())});
            if (result == 0) {
                Log.e(LOG, "updateDevice Update failed");
            }
        } catch (SQLiteException e) {
            Log.e(LOG, "updateDevice failed", e);
        } finally {
            close();
        }
        return result;
    }

    public long resetDevice(String device) {
        long result = -1;
        SQLiteDatabase db = this.getWritableDatabase();
        result = db.delete(TABLE_DEVICE, KEY_NOME + " = ?", new String[]{device});
        db.close();
        return result;
    }

    // ------------------------ Metodi Tabella OSSIMETRO ----------------//


    /**
     * Creazione Misurazione Ossimetro (SPO_MEAN_HIS)
     *
     * @param misurazione Oggetto Misurazione
     * @return id DB Misurazione
     */
    public long inserisciMisuraSPO(DataPoint misurazione) {
        long result = -1;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.ITALY);
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(KEY_ID, (byte[]) null);
            values.put(KEY_IDASSISTED, misurazione.getIdAssisted());
            values.put(KEY_DATA, sdf.format(misurazione.getData()));
            values.put(KEY_DATANAME, misurazione.getName());
            values.put(KEY_DATAVALUE, misurazione.getValue());
            values.put(KEY_DATATYPE, misurazione.getType());
            values.put(KEY_DATAUNIT, misurazione.getUnit());
            values.put(KEY_SEND, misurazione.getSend());
            result = db.insert(TABLE_OSSIMETRO, null, values);
            Log.i(LOG, "inserisciMisuraOssimetro: " + result);
            if (result == -1) {
                Log.e(LOG, "inserisciMisuraSPO Insert failed");
            }
        } catch (SQLiteException e) {
            Log.e(LOG, "inserisciMisuraSPO failed", e);
        } finally {
            close();
        }
        return result;
    }

    /**
     * Lettura misurazioni SPO (Saturazione Ossigeno)
     *
     * @return elenco misurazioni
     */
    public ArrayList<DataPoint> getElencoMisurazioniSPO() {
        ArrayList<DataPoint> lista = new ArrayList<>();
        Cursor c = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.ITALY);
            SQLiteDatabase db = this.getReadableDatabase();
            String selectQuery = "SELECT  * FROM " + TABLE_OSSIMETRO + " WHERE "
                    + KEY_DATANAME + " = 'Saturazione' ORDER BY " + KEY_DATA + " DESC";
            Log.i(LOG, selectQuery);
            c = db.rawQuery(selectQuery, null);
            if (c.moveToFirst()) {
                do {
                    try {
                        int id = c.getInt(c.getColumnIndex(KEY_ID));
                        int idAssisted = c.getInt(c.getColumnIndex(KEY_IDASSISTED));
                        Date data = sdf.parse(c.getString(c.getColumnIndex(KEY_DATA)));
                        String name = c.getString(c.getColumnIndex(KEY_DATANAME));
                        String type = c.getString(c.getColumnIndex(KEY_DATATYPE));
                        Double value = c.getDouble(c.getColumnIndex(KEY_DATAVALUE));
                        String unit = c.getString(c.getColumnIndex(KEY_DATAUNIT));
                        String send = c.getString(c.getColumnIndex(KEY_SEND));
                        DataPoint misurazione = new DataPoint(id, idAssisted, data, name, type, value, unit, send);
                        lista.add(misurazione);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } while (c.moveToNext());
            }
        } catch (SQLiteException e) {
            Log.e(LOG, "getElencoMisurazioniSPO failed", e);
        } finally {
            if (c != null) {
                c.close();
            }
            close();
        }
        return lista;
    }

    /**
     * Lettura misurazioni SPO_Pulse (Pulsazioni)
     *
     * @return elenco misurazioni
     */
    public ArrayList<DataPoint> getElencoMisurazioniSPO_Pulse() {
        ArrayList<DataPoint> lista = new ArrayList<>();
        Cursor c = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.ITALY);
            SQLiteDatabase db = this.getReadableDatabase();
            String selectQuery = "SELECT  * FROM " + TABLE_OSSIMETRO + " WHERE "
                    + KEY_DATANAME + " = 'Pulsazioni_ossimetro' ORDER BY " + KEY_DATA + " DESC";
            Log.i(LOG, selectQuery);
            c = db.rawQuery(selectQuery, null);
            if (c.moveToFirst()) {
                do {
                    try {
                        int id = c.getInt(c.getColumnIndex(KEY_ID));
                        int idAssisted = c.getInt(c.getColumnIndex(KEY_IDASSISTED));
                        Date data = sdf.parse(c.getString(c.getColumnIndex(KEY_DATA)));
                        String name = c.getString(c.getColumnIndex(KEY_DATANAME));
                        String type = c.getString(c.getColumnIndex(KEY_DATATYPE));
                        Double value = c.getDouble(c.getColumnIndex(KEY_DATAVALUE));
                        String unit = c.getString(c.getColumnIndex(KEY_DATAUNIT));
                        String send = c.getString(c.getColumnIndex(KEY_SEND));
                        DataPoint misurazione = new DataPoint(id, idAssisted, data, name, type, value, unit, send);
                        lista.add(misurazione);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } while (c.moveToNext());
            }
        } catch (SQLiteException e) {
            Log.e(LOG, "getElencoMisurazioniSPO_Pulse failed", e);
        } finally {
            if (c != null) {
                c.close();
            }
            close();
        }
        return lista;
    }

    /**
     * Lettura misurazioni non inviate SPO
     *
     * @return elenco misurazioni
     */
    public ArrayList<DataPoint> getElencoMisurazioniSPO_NotSend() {
        ArrayList<DataPoint> lista = new ArrayList<>();
        Cursor c = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.ITALY);
            SQLiteDatabase db = this.getReadableDatabase();
            String selectQuery = "SELECT  * FROM " + TABLE_OSSIMETRO + " WHERE "
                    + KEY_SEND + " = 'false'";
            Log.i(LOG, selectQuery);
            c = db.rawQuery(selectQuery, null);
            if (c.moveToFirst()) {
                do {
                    try {
                        int id = c.getInt(c.getColumnIndex(KEY_ID));
                        int idAssisted = c.getInt(c.getColumnIndex(KEY_IDASSISTED));
                        Date data = sdf.parse(c.getString(c.getColumnIndex(KEY_DATA)));
                        String name = c.getString(c.getColumnIndex(KEY_DATANAME));
                        String type = c.getString(c.getColumnIndex(KEY_DATATYPE));
                        Double value = c.getDouble(c.getColumnIndex(KEY_DATAVALUE));
                        String unit = c.getString(c.getColumnIndex(KEY_DATAUNIT));
                        String send = c.getString(c.getColumnIndex(KEY_SEND));
                        Log.i(SYNC_NOTSEND, "----------------------");
                        Log.i(SYNC_NOTSEND + KEY_ID, String.valueOf(id));
                        Log.i(SYNC_NOTSEND + KEY_DATA, sdf.format(data));
                        Log.i(SYNC_NOTSEND + KEY_IDASSISTED, String.valueOf(idAssisted));
                        Log.i(SYNC_NOTSEND + KEY_DATANAME, name);
                        Log.i(SYNC_NOTSEND + KEY_DATATYPE, type);
                        Log.i(SYNC_NOTSEND + KEY_DATAVALUE, String.valueOf(value));
                        Log.i(SYNC_NOTSEND + KEY_DATAUNIT, unit);
                        Log.i(SYNC_NOTSEND + KEY_SEND, String.valueOf(send));
                        Log.i(SYNC_NOTSEND, "----------------------");
                        DataPoint misurazione = new DataPoint(id, idAssisted, data, name, type, value, unit, send);
                        lista.add(misurazione);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } while (c.moveToNext());
            }
        } catch (SQLiteException e) {
            Log.e(LOG, "getElencoMisurazioniSPO_NotSend failed", e);
        } finally {
            if (c != null) {
                c.close();
            }
            close();
        }
        return lista;
    }

    /**
     * Lettura misurazioni inviate SPO
     *
     * @return elenco misurazioni
     */
    public ArrayList<DataPoint> getElencoMisurazioniSPO_Send() {
        ArrayList<DataPoint> lista = new ArrayList<>();
        Cursor c = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", Locale.ITALY);
            SQLiteDatabase db = this.getReadableDatabase();
            String selectQuery = "SELECT  * FROM " + TABLE_OSSIMETRO + " WHERE "
                    + KEY_SEND + " = 'true'";
            Log.i(LOG, selectQuery);
            c = db.rawQuery(selectQuery, null);
            if (c.moveToFirst()) {
                do {
                    try {
                        int id = c.getInt(c.getColumnIndex(KEY_ID));
                        int idAssisted = c.getInt(c.getColumnIndex(KEY_IDASSISTED));
                        Date data = sdf.parse(c.getString(c.getColumnIndex(KEY_DATA)));
                        String name = c.getString(c.getColumnIndex(KEY_DATANAME));
                        String type = c.getString(c.getColumnIndex(KEY_DATATYPE));
                        Double value = c.getDouble(c.getColumnIndex(KEY_DATAVALUE));
                        String unit = c.getString(c.getColumnIndex(KEY_DATAUNIT));
                        String send = c.getString(c.getColumnIndex(KEY_SEND));
                        Log.i(SYNC_SEND, "----------------------");
                        Log.i(SYNC_SEND + KEY_ID, String.valueOf(id));
                        Log.i(SYNC_SEND + KEY_DATA, sdf.format(data));
                        Log.i(SYNC_SEND + KEY_IDASSISTED, String.valueOf(idAssisted));
                        Log.i(SYNC_SEND + KEY_DATANAME, name);
                        Log.i(SYNC_SEND + KEY_DATATYPE, type);
                        Log.i(SYNC_SEND + KEY_DATAVALUE, String.valueOf(value));
                        Log.i(SYNC_SEND + KEY_DATAUNIT, unit);
                        Log.i(SYNC_SEND + KEY_SEND, String.valueOf(send));
                        Log.i(SYNC_SEND, "----------------------");
                        DataPoint misurazione = new DataPoint(id, idAssisted, data, name, type, value, unit, send);
                        lista.add(misurazione);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } while (c.moveToNext());
            }
        } catch (SQLiteException e) {
            Log.e(LOG, "getElencoMisurazioniSPO failed", e);
        } finally {
            if (c != null) {
                c.close();
            }
            close();
        }
        return lista;
    }

    /**
     * Update misurazioni SPO
     *
     * @param misurazione oggetto misurazione
     * @return valore true o false
     */
    public boolean updateOssimetro(DataPoint misurazione) {
        boolean result = false;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(KEY_IDASSISTED, misurazione.getIdAssisted());
            values.put(KEY_SEND, misurazione.getSend());
            result = db.update(TABLE_OSSIMETRO, values, KEY_ID + "='" + misurazione.getId() + "'", null) > 0;
            if (result == false) {
                Log.e(LOG, "updateOssimetro Update failed");
            }
        } catch (SQLiteException e) {
            Log.e(LOG, "updateOssimetro failed", e);
        } finally {
            close();
        }
        return result;
    }


    // ------------------------ Metodi Tabella SFIGMOMANOMETRO ----------------//


    /**
     * Creazione Misurazione Sfigmomanometro
     *
     * @param misurazione oggetto misurazione
     * @return id DB misurazione
     */
    public long inserisciMisuraBPM(DataPoint misurazione) {
        long result = -1;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(KEY_IDASSISTED, misurazione.getIdAssisted());
            values.put(KEY_DATA, sdf.format(misurazione.getData()));
            values.put(KEY_DATANAME, misurazione.getName());
            values.put(KEY_DATAVALUE, misurazione.getValue());
            values.put(KEY_DATATYPE, misurazione.getType());
            values.put(KEY_DATAUNIT, misurazione.getUnit());
            values.put(KEY_SEND, misurazione.getSend());
            result = db.insert(TABLE_SFIGMOMANOMETRO, null, values);
            Log.i(LOG, "inserisciMisuraBPM: " + result);
            if (result == -1) {
                Log.e(LOG, "inserisciMisuraBPM Insert failed");
            }
        } catch (SQLiteException e) {
            Log.e(LOG, "inserisciMisuraBPM failed", e);
        } finally {
            close();
        }
        return result;
    }

    /**
     * Lettura misurazioni Sfigmomanometro (BPM_Diastolic)
     *
     * @return elenco misurazioni
     */
    public ArrayList<DataPoint> getElencoMisurazioniBPM_Diastolic() {
        ArrayList<DataPoint> lista = new ArrayList<>();
        Cursor c = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            SQLiteDatabase db = this.getReadableDatabase();
            String selectQuery = "SELECT  * FROM " + TABLE_SFIGMOMANOMETRO + " WHERE "
                    + KEY_DATANAME + " = 'Diastolica' ORDER BY " + KEY_DATA + " DESC";
            Log.i(LOG, selectQuery);
            c = db.rawQuery(selectQuery, null);
            if (c.moveToFirst()) {
                do {
                    try {
                        int id = c.getInt(c.getColumnIndex(KEY_ID));
                        int idAssisted = c.getInt(c.getColumnIndex(KEY_IDASSISTED));
                        Date data = sdf.parse(c.getString(c.getColumnIndex(KEY_DATA)));
                        String name = c.getString(c.getColumnIndex(KEY_DATANAME));
                        String type = c.getString(c.getColumnIndex(KEY_DATATYPE));
                        Double value = c.getDouble(c.getColumnIndex(KEY_DATAVALUE));
                        String unit = c.getString(c.getColumnIndex(KEY_DATAUNIT));
                        String send = c.getString(c.getColumnIndex(KEY_SEND));
                        DataPoint misurazione = new DataPoint(id, idAssisted, data, name, type, value, unit, send);
                        lista.add(misurazione);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } while (c.moveToNext());
            }
        } catch (SQLiteException e) {
            Log.e(LOG, "getElencoMisurazioniBPM_Diastolic failed", e);
        } finally {
            if (c != null) {
                c.close();
            }
            close();
        }
        return lista;
    }

    /**
     * Lettura misurazioni Sfigmomanometro (BPM_Systolic)
     *
     * @return elenco misurazioni
     */
    public ArrayList<DataPoint> getElencoMisurazioniBPM_Systolic() {
        ArrayList<DataPoint> lista = new ArrayList<>();
        Cursor c = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            SQLiteDatabase db = this.getReadableDatabase();
            String selectQuery = "SELECT  * FROM " + TABLE_SFIGMOMANOMETRO + " WHERE "
                    + KEY_DATANAME + " = 'Sistolica' ORDER BY " + KEY_DATA + " DESC";
            Log.i(LOG, selectQuery);
            c = db.rawQuery(selectQuery, null);
            if (c.moveToFirst()) {
                do {
                    try {
                        int id = c.getInt(c.getColumnIndex(KEY_ID));
                        int idAssisted = c.getInt(c.getColumnIndex(KEY_IDASSISTED));
                        Date data = sdf.parse(c.getString(c.getColumnIndex(KEY_DATA)));
                        String name = c.getString(c.getColumnIndex(KEY_DATANAME));
                        String type = c.getString(c.getColumnIndex(KEY_DATATYPE));
                        Double value = c.getDouble(c.getColumnIndex(KEY_DATAVALUE));
                        String unit = c.getString(c.getColumnIndex(KEY_DATAUNIT));
                        String send = c.getString(c.getColumnIndex(KEY_SEND));
                        DataPoint misurazione = new DataPoint(id, idAssisted, data, name, type, value, unit, send);
                        lista.add(misurazione);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } while (c.moveToNext());
            }
        } catch (SQLiteException e) {
            Log.e(LOG, "getElencoMisurazioniBPM_Systolic failed", e);
        } finally {
            if (c != null) {
                c.close();
            }
            close();
        }
        return lista;
    }

    /**
     * Lettura misurazioni Arrhythmia_TAIDOC
     *
     * @return elenco misurazioni
     */
    public ArrayList<DataPoint> getElencoMisurazioniArrhythmia() {
        ArrayList<DataPoint> lista = new ArrayList<>();
        Cursor c = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            SQLiteDatabase db = this.getReadableDatabase();
            String selectQuery = "SELECT  * FROM " + TABLE_SFIGMOMANOMETRO + " WHERE "
                    + KEY_DATANAME + " = 'Aritmia' ORDER BY " + KEY_DATA + " DESC";
            Log.i(LOG, selectQuery);
            c = db.rawQuery(selectQuery, null);
            if (c.moveToFirst()) {
                do {
                    try {
                        int id = c.getInt(c.getColumnIndex(KEY_ID));
                        int idAssisted = c.getInt(c.getColumnIndex(KEY_IDASSISTED));
                        Date data = sdf.parse(c.getString(c.getColumnIndex(KEY_DATA)));
                        String name = c.getString(c.getColumnIndex(KEY_DATANAME));
                        String type = c.getString(c.getColumnIndex(KEY_DATATYPE));
                        Double value = c.getDouble(c.getColumnIndex(KEY_DATAVALUE));
                        String unit = c.getString(c.getColumnIndex(KEY_DATAUNIT));
                        String send = c.getString(c.getColumnIndex(KEY_SEND));
                        DataPoint misurazione = new DataPoint(id, idAssisted, data, name, type, value, unit, send);
                        lista.add(misurazione);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } while (c.moveToNext());
            }
        } catch (SQLiteException e) {
            Log.e(LOG, "getElencoMisurazioniArrhythmia failed", e);
        } finally {
            if (c != null) {
                c.close();
            }
            close();
        }
        return lista;
    }

    /**
     * Lettura misurazioni Pulse_TAIDOC
     *
     * @return elenco misurazioni
     */
    public ArrayList<DataPoint> getElencoMisurazioniPulse() {
        ArrayList<DataPoint> lista = new ArrayList<>();
        Cursor c = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            SQLiteDatabase db = this.getReadableDatabase();
            String selectQuery = "SELECT  * FROM " + TABLE_SFIGMOMANOMETRO + " WHERE "
                    + KEY_DATANAME + " = 'Pulsazioni_sfigmomanometro' ORDER BY " + KEY_DATA + " DESC";
            Log.i(LOG, selectQuery);
            c = db.rawQuery(selectQuery, null);
            if (c.moveToFirst()) {
                do {
                    try {
                        int id = c.getInt(c.getColumnIndex(KEY_ID));
                        int idAssisted = c.getInt(c.getColumnIndex(KEY_IDASSISTED));
                        Date data = sdf.parse(c.getString(c.getColumnIndex(KEY_DATA)));
                        String name = c.getString(c.getColumnIndex(KEY_DATANAME));
                        String type = c.getString(c.getColumnIndex(KEY_DATATYPE));
                        Double value = c.getDouble(c.getColumnIndex(KEY_DATAVALUE));
                        String unit = c.getString(c.getColumnIndex(KEY_DATAUNIT));
                        String send = c.getString(c.getColumnIndex(KEY_SEND));
                        DataPoint misurazione = new DataPoint(id, idAssisted, data, name, type, value, unit, send);
                        lista.add(misurazione);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } while (c.moveToNext());
            }
        } catch (SQLiteException e) {
            Log.e(LOG, "getElencoMisurazioniPulse failed", e);
        } finally {
            if (c != null) {
                c.close();
            }
            close();
        }
        return lista;
    }


    /**
     * Lettura misurazioni MAP_TAIDOC
     *
     * @return elenco misurazioni
     */
    public ArrayList<DataPoint> getElencoMisurazioniMAP() {
        ArrayList<DataPoint> lista = new ArrayList<>();
        Cursor c = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            SQLiteDatabase db = this.getReadableDatabase();
            String selectQuery = "SELECT  * FROM " + TABLE_SFIGMOMANOMETRO + " WHERE "
                    + KEY_DATANAME + " = 'MAP_TAIDOC' ORDER BY " + KEY_DATA + " DESC";
            Log.i(LOG, selectQuery);
            c = db.rawQuery(selectQuery, null);
            if (c.moveToFirst()) {
                do {
                    try {
                        int id = c.getInt(c.getColumnIndex(KEY_ID));
                        int idAssisted = c.getInt(c.getColumnIndex(KEY_IDASSISTED));
                        Date data = sdf.parse(c.getString(c.getColumnIndex(KEY_DATA)));
                        String name = c.getString(c.getColumnIndex(KEY_DATANAME));
                        String type = c.getString(c.getColumnIndex(KEY_DATATYPE));
                        Double value = c.getDouble(c.getColumnIndex(KEY_DATAVALUE));
                        String unit = c.getString(c.getColumnIndex(KEY_DATAUNIT));
                        String send = c.getString(c.getColumnIndex(KEY_SEND));
                        DataPoint misurazione = new DataPoint(id, idAssisted, data, name, type, value, unit, send);
                        lista.add(misurazione);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } while (c.moveToNext());
            }
        } catch (SQLiteException e) {
            Log.e(LOG, "getElencoMisurazioniMAP failed", e);
        } finally {
            if (c != null) {
                c.close();
            }
            close();
        }
        return lista;
    }

    /**
     * Update misurazione Sfigmomanometro
     *
     * @param misurazione oggetto misurazione
     * @return valore true o false
     */
    public boolean updateSfigmomanometro(DataPoint misurazione) {
        boolean result = false;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(KEY_IDASSISTED, misurazione.getIdAssisted());
            values.put(KEY_SEND, misurazione.getSend());
            result = db.update(TABLE_SFIGMOMANOMETRO, values, KEY_ID + "='" + misurazione.getId() + "'", null) > 0;
            if (result == false) {
                Log.e(LOG, "updateSfigmomanometro Update failed");
            }
        } catch (SQLiteException e) {
            Log.e(LOG, "updateSfigmomanometro failed", e);
        } finally {
            close();
        }
        return result;
    }

    /**
     * Lettura misurazioni BPM non inviate
     *
     * @return elenco misurazioni
     */
    public ArrayList<DataPoint> getElencoMisurazioniBPM_NotSend() {
        ArrayList<DataPoint> lista = new ArrayList<>();
        Cursor c = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            SQLiteDatabase db = this.getReadableDatabase();
            String selectQuery = "SELECT  * FROM " + TABLE_SFIGMOMANOMETRO + " WHERE "
                    + KEY_SEND + " = 'false'";
            Log.i(LOG, selectQuery);
            c = db.rawQuery(selectQuery, null);
            if (c.moveToFirst()) {
                do {
                    try {
                        int id = c.getInt(c.getColumnIndex(KEY_ID));
                        int idAssisted = c.getInt(c.getColumnIndex(KEY_IDASSISTED));
                        Date data = sdf.parse(c.getString(c.getColumnIndex(KEY_DATA)));
                        String name = c.getString(c.getColumnIndex(KEY_DATANAME));
                        String type = c.getString(c.getColumnIndex(KEY_DATATYPE));
                        Double value = c.getDouble(c.getColumnIndex(KEY_DATAVALUE));
                        String unit = c.getString(c.getColumnIndex(KEY_DATAUNIT));
                        String send = c.getString(c.getColumnIndex(KEY_SEND));
                        Log.i(SYNC_NOTSEND, "----------------------");
                        Log.i(SYNC_NOTSEND + KEY_ID, String.valueOf(id));
                        Log.i(SYNC_NOTSEND + KEY_DATA, sdf.format(data));
                        Log.i(SYNC_NOTSEND + KEY_IDASSISTED, String.valueOf(idAssisted));
                        Log.i(SYNC_NOTSEND + KEY_DATANAME, name);
                        Log.i(SYNC_NOTSEND + KEY_DATATYPE, type);
                        Log.i(SYNC_NOTSEND + KEY_DATAVALUE, String.valueOf(value));
                        Log.i(SYNC_NOTSEND + KEY_DATAUNIT, unit);
                        Log.i(SYNC_NOTSEND + KEY_SEND, String.valueOf(send));
                        Log.i(SYNC_NOTSEND, "----------------------");
                        DataPoint misurazione = new DataPoint(id, idAssisted, data, name, type, value, unit, send);
                        lista.add(misurazione);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } while (c.moveToNext());
            }
        } catch (SQLiteException e) {
            Log.e(LOG, "getElencoMisurazioniBPM_NotSend failed", e);
        } finally {
            if (c != null) {
                c.close();
            }
            close();
        }
        return lista;
    }

    /**
     * Lettura misurazioni BPM inviate
     *
     * @return elenco misurazioni
     */
    public ArrayList<DataPoint> getElencoMisurazioniBPM_Send() {
        ArrayList<DataPoint> lista = new ArrayList<>();
        Cursor c = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            SQLiteDatabase db = this.getReadableDatabase();
            String selectQuery = "SELECT  * FROM " + TABLE_SFIGMOMANOMETRO + " WHERE "
                    + KEY_SEND + " = 'true'";
            Log.i(LOG, selectQuery);
            c = db.rawQuery(selectQuery, null);
            if (c.moveToFirst()) {
                do {
                    try {
                        int id = c.getInt(c.getColumnIndex(KEY_ID));
                        int idAssisted = c.getInt(c.getColumnIndex(KEY_IDASSISTED));
                        Date data = sdf.parse(c.getString(c.getColumnIndex(KEY_DATA)));
                        String name = c.getString(c.getColumnIndex(KEY_DATANAME));
                        String type = c.getString(c.getColumnIndex(KEY_DATATYPE));
                        Double value = c.getDouble(c.getColumnIndex(KEY_DATAVALUE));
                        String unit = c.getString(c.getColumnIndex(KEY_DATAUNIT));
                        String send = c.getString(c.getColumnIndex(KEY_SEND));
                        Log.i(SYNC_SEND, "----------------------");
                        Log.i(SYNC_SEND + KEY_ID, String.valueOf(id));
                        Log.i(SYNC_SEND + KEY_DATA, sdf.format(data));
                        Log.i(SYNC_SEND + KEY_IDASSISTED, String.valueOf(idAssisted));
                        Log.i(SYNC_SEND + KEY_DATANAME, name);
                        Log.i(SYNC_SEND + KEY_DATATYPE, type);
                        Log.i(SYNC_SEND + KEY_DATAVALUE, String.valueOf(value));
                        Log.i(SYNC_SEND + KEY_DATAUNIT, unit);
                        Log.i(SYNC_SEND + KEY_SEND, String.valueOf(send));
                        Log.i(SYNC_SEND, "----------------------");
                        DataPoint misurazione = new DataPoint(id, idAssisted, data, name, type, value, unit, send);
                        lista.add(misurazione);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } while (c.moveToNext());
            }
        } catch (SQLiteException e) {
            Log.e(LOG, "getElencoMisurazioniBPM_Send failed", e);
        } finally {
            if (c != null) {
                c.close();
            }
            close();
        }
        return lista;
    }


    // ------------------------ Metodi Tabella GLUCOMETRO ----------------//


    /**
     * Creazione Misurazione Glucometro
     *
     * @param misurazione oggetto misurazione
     * @return id DB misurazione
     */
    public long inserisciMisuraGLM(DataPoint misurazione) {
        long result = -1;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(KEY_IDASSISTED, misurazione.getIdAssisted());
            values.put(KEY_DATA, sdf.format(misurazione.getData()));
            values.put(KEY_DATANAME, misurazione.getName());
            values.put(KEY_DATAVALUE, misurazione.getValue());
            values.put(KEY_DATATYPE, misurazione.getType());
            values.put(KEY_DATAUNIT, misurazione.getUnit());
            values.put(KEY_SEND, misurazione.getSend());
            result = db.insert(TABLE_GLUCOMETRO, null, values);
            Log.i(LOG, "inserisciMisuraGLM: " + result);
            if (result == -1) {
                Log.e(LOG, "inserisciMisuraGLM Insert failed");
            }
        } catch (SQLiteException e) {
            Log.e(LOG, "inserisciMisuraGLM failed", e);
        } finally {
            close();
        }
        return result;
    }

    /**
     * Lettura tutte misurazioni cliente
     *
     * @return elenco misurazioni
     */
    public ArrayList<DataPoint> getMisurazioniGLM_Glucose() {
        ArrayList<DataPoint> lista = new ArrayList<>();
        Cursor c = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            SQLiteDatabase db = this.getReadableDatabase();
            String selectQuery = "SELECT  * FROM " + TABLE_GLUCOMETRO + " WHERE "
                    + KEY_DATANAME + " = 'Glu' ORDER BY " + KEY_DATA + " DESC";
            Log.i(LOG, selectQuery);
            c = db.rawQuery(selectQuery, null);
            if (c.moveToFirst()) {
                do {
                    try {
                        int id = c.getInt(c.getColumnIndex(KEY_ID));
                        int idAssisted = c.getInt(c.getColumnIndex(KEY_IDASSISTED));
                        Date data = sdf.parse(c.getString(c.getColumnIndex(KEY_DATA)));
                        String name = c.getString(c.getColumnIndex(KEY_DATANAME));
                        String type = c.getString(c.getColumnIndex(KEY_DATATYPE));
                        Double value = c.getDouble(c.getColumnIndex(KEY_DATAVALUE));
                        String unit = c.getString(c.getColumnIndex(KEY_DATAUNIT));
                        String send = c.getString(c.getColumnIndex(KEY_SEND));
                        DataPoint misurazione = new DataPoint(id, idAssisted, data, name, type, value, unit, send);
                        lista.add(misurazione);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } while (c.moveToNext());
            }
        } catch (SQLiteException e) {
            Log.e(LOG, "getMisurazioniGLM_Glucose failed", e);
        } finally {
            if (c != null) {
                c.close();
            }
            close();
        }
        return lista;
    }

    /**
     * Lettura tutte misurazioni BGM non inviate
     *
     * @return elenco misurazioni
     */
    public ArrayList<DataPoint> getElencoMisurazioniBGM_NotSend() {
        ArrayList<DataPoint> lista = new ArrayList<>();
        Cursor c = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            SQLiteDatabase db = this.getReadableDatabase();
            String selectQuery = "SELECT  * FROM " + TABLE_GLUCOMETRO + " WHERE "
                    + KEY_SEND + " = 'false'";
            Log.i(LOG, selectQuery);
            c = db.rawQuery(selectQuery, null);
            if (c.moveToFirst()) {
                do {
                    try {
                        int id = c.getInt(c.getColumnIndex(KEY_ID));
                        int idAssisted = c.getInt(c.getColumnIndex(KEY_IDASSISTED));
                        Date data = sdf.parse(c.getString(c.getColumnIndex(KEY_DATA)));
                        String name = c.getString(c.getColumnIndex(KEY_DATANAME));
                        String type = c.getString(c.getColumnIndex(KEY_DATATYPE));
                        Double value = c.getDouble(c.getColumnIndex(KEY_DATAVALUE));
                        String unit = c.getString(c.getColumnIndex(KEY_DATAUNIT));
                        String send = c.getString(c.getColumnIndex(KEY_SEND));
                        Log.i(SYNC_NOTSEND, "----------------------");
                        Log.i(SYNC_NOTSEND + KEY_ID, String.valueOf(id));
                        Log.i(SYNC_NOTSEND + KEY_DATA, sdf.format(data));
                        Log.i(SYNC_NOTSEND + KEY_IDASSISTED, String.valueOf(idAssisted));
                        Log.i(SYNC_NOTSEND + KEY_DATANAME, name);
                        Log.i(SYNC_NOTSEND + KEY_DATATYPE, type);
                        Log.i(SYNC_NOTSEND + KEY_DATAVALUE, String.valueOf(value));
                        Log.i(SYNC_NOTSEND + KEY_DATAUNIT, unit);
                        Log.i(SYNC_NOTSEND + KEY_SEND, String.valueOf(send));
                        Log.i(SYNC_NOTSEND, "----------------------");
                        DataPoint misurazione = new DataPoint(id, idAssisted, data, name, type, value, unit, send);
                        lista.add(misurazione);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } while (c.moveToNext());
            }
        } catch (SQLiteException e) {
            Log.e(LOG, "getElencoMisurazioniBGM_NotSend failed", e);
        } finally {
            if (c != null) {
                c.close();
            }
            close();
        }
        return lista;
    }

    /**
     * Lettura misurazioni BGM inviate
     *
     * @return elenco misurazioni
     */
    public ArrayList<DataPoint> getElencoMisurazioniBGM_Send() {
        ArrayList<DataPoint> lista = new ArrayList<>();
        Cursor c = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            SQLiteDatabase db = this.getReadableDatabase();
            String selectQuery = "SELECT  * FROM " + TABLE_GLUCOMETRO + " WHERE "
                    + KEY_SEND + " = 'true'";
            Log.i(LOG, selectQuery);
            c = db.rawQuery(selectQuery, null);
            if (c.moveToFirst()) {
                do {
                    try {
                        int id = c.getInt(c.getColumnIndex(KEY_ID));
                        int idAssisted = c.getInt(c.getColumnIndex(KEY_IDASSISTED));
                        Date data = sdf.parse(c.getString(c.getColumnIndex(KEY_DATA)));
                        String name = c.getString(c.getColumnIndex(KEY_DATANAME));
                        String type = c.getString(c.getColumnIndex(KEY_DATATYPE));
                        Double value = c.getDouble(c.getColumnIndex(KEY_DATAVALUE));
                        String unit = c.getString(c.getColumnIndex(KEY_DATAUNIT));
                        String send = c.getString(c.getColumnIndex(KEY_SEND));
                        Log.i(SYNC_SEND, "----------------------");
                        Log.i(SYNC_SEND + KEY_ID, String.valueOf(id));
                        Log.i(SYNC_SEND + KEY_DATA, sdf.format(data));
                        Log.i(SYNC_SEND + KEY_IDASSISTED, String.valueOf(idAssisted));
                        Log.i(SYNC_SEND + KEY_DATANAME, name);
                        Log.i(SYNC_SEND + KEY_DATATYPE, type);
                        Log.i(SYNC_SEND + KEY_DATAVALUE, String.valueOf(value));
                        Log.i(SYNC_SEND + KEY_DATAUNIT, unit);
                        Log.i(SYNC_SEND + KEY_SEND, String.valueOf(send));
                        Log.i(SYNC_SEND, "----------------------");
                        DataPoint misurazione = new DataPoint(id, idAssisted, data, name, type, value, unit, send);
                        lista.add(misurazione);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } while (c.moveToNext());
            }
        } catch (SQLiteException e) {
            Log.e(LOG, "getElencoMisurazioniBGM_Send failed", e);
        } finally {
            if (c != null) {
                c.close();
            }
            close();
        }
        return lista;
    }


    /**
     * Update Glucometro
     *
     * @param misurazione oggetto misurazione
     * @return valore true o false
     */
    public boolean updateGlucometro(DataPoint misurazione) {
        boolean result = false;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(KEY_IDASSISTED, misurazione.getIdAssisted());
            values.put(KEY_SEND, misurazione.getSend());
            result = db.update(TABLE_GLUCOMETRO, values, KEY_ID + "='" + misurazione.getId() + "'", null) > 0;
            if (result == false) {
                Log.e(LOG, "updateGlucometro Update failed");
            }
        } catch (SQLiteException e) {
            Log.e(LOG, "updateGlucometro failed", e);
        } finally {
            close();
        }
        return result;
    }

    // ------------------------ Metodi Tabella BILANCIA ----------------//


    /**
     * Creazione Misurazione Bilancia
     *
     * @param misurazione oggetto misurazione
     * @return id DB misurazione
     */
    public long inserisciMisuraHIS(DataPoint misurazione) {
        long result = -1;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(KEY_IDASSISTED, misurazione.getIdAssisted());
            values.put(KEY_DATA, sdf.format(misurazione.getData()));
            values.put(KEY_DATANAME, misurazione.getName());
            values.put(KEY_DATAVALUE, misurazione.getValue());
            values.put(KEY_DATATYPE, misurazione.getType());
            values.put(KEY_DATAUNIT, misurazione.getUnit());
            values.put(KEY_SEND, misurazione.getSend());
            result = db.insert(TABLE_BILANCIA, null, values);
            Log.i(LOG, "inserisciMisuraHIS: " + result);
        } catch (SQLiteException e) {
            Log.e(LOG, "inserisciMisuraHIS failed", e);
        } finally {
            close();
        }
        return result;
    }

    /**
     * Lettura tutte misurazioni Weight_HIS
     *
     * @return elenco misurazioni
     */
    public ArrayList<DataPoint> getMisurazioniWeight() {
        ArrayList<DataPoint> lista = new ArrayList<>();
        Cursor c = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            SQLiteDatabase db = this.getReadableDatabase();
            String selectQuery = "SELECT  * FROM " + TABLE_BILANCIA + " WHERE "
                    + KEY_DATANAME + " = 'Peso' ORDER BY " + KEY_DATA + " DESC";
            Log.i(LOG, selectQuery);
            c = db.rawQuery(selectQuery, null);
            if (c.moveToFirst()) {
                do {
                    try {
                        int id = c.getInt(c.getColumnIndex(KEY_ID));
                        int idAssisted = c.getInt(c.getColumnIndex(KEY_IDASSISTED));
                        Date data = sdf.parse(c.getString(c.getColumnIndex(KEY_DATA)));
                        String name = c.getString(c.getColumnIndex(KEY_DATANAME));
                        String type = c.getString(c.getColumnIndex(KEY_DATATYPE));
                        Double value = c.getDouble(c.getColumnIndex(KEY_DATAVALUE));
                        String unit = c.getString(c.getColumnIndex(KEY_DATAUNIT));
                        String send = c.getString(c.getColumnIndex(KEY_SEND));
                        DataPoint misurazione = new DataPoint(id, idAssisted, data, name, type, value, unit, send);
                        lista.add(misurazione);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } while (c.moveToNext());
            }
        } catch (SQLiteException e) {
            Log.e(LOG, "getMisurazioniWeight failed", e);
        } finally {
            if (c != null) {
                c.close();
            }
            close();
        }
        return lista;
    }

    /**
     * Lettura tutte misurazioni BoadyFat_HIS
     *
     * @return elenco misurazioni
     */
    public ArrayList<DataPoint> getMisurazioniBoadyFat() {
        ArrayList<DataPoint> lista = new ArrayList<>();
        Cursor c = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            SQLiteDatabase db = this.getReadableDatabase();
            String selectQuery = "SELECT  * FROM " + TABLE_BILANCIA + " WHERE "
                    + KEY_DATANAME + " = 'BoadyFat_HIS' ORDER BY " + KEY_DATA + " DESC";
            Log.i(LOG, selectQuery);
            c = db.rawQuery(selectQuery, null);
            if (c.moveToFirst()) {
                do {
                    try {
                        int id = c.getInt(c.getColumnIndex(KEY_ID));
                        int idAssisted = c.getInt(c.getColumnIndex(KEY_IDASSISTED));
                        Date data = sdf.parse(c.getString(c.getColumnIndex(KEY_DATA)));
                        String name = c.getString(c.getColumnIndex(KEY_DATANAME));
                        String type = c.getString(c.getColumnIndex(KEY_DATATYPE));
                        Double value = c.getDouble(c.getColumnIndex(KEY_DATAVALUE));
                        String unit = c.getString(c.getColumnIndex(KEY_DATAUNIT));
                        String send = c.getString(c.getColumnIndex(KEY_SEND));
                        DataPoint misurazione = new DataPoint(id, idAssisted, data, name, type, value, unit, send);
                        lista.add(misurazione);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } while (c.moveToNext());
            }
        } catch (SQLiteException e) {
            Log.e(LOG, "getMisurazioniBoadyFat failed", e);
        } finally {
            if (c != null) {
                c.close();
            }
            close();
        }
        return lista;
    }

    /**
     * Lettura tutte misurazioni BoadyWater_HIS
     *
     * @return elenco misurazioni
     */
    public ArrayList<DataPoint> getMisurazioniBoadyWater() {
        ArrayList<DataPoint> lista = new ArrayList<>();
        Cursor c = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            SQLiteDatabase db = this.getReadableDatabase();
            String selectQuery = "SELECT  * FROM " + TABLE_BILANCIA + " WHERE "
                    + KEY_DATANAME + " = 'BoadyWater_HIS' ORDER BY " + KEY_DATA + " DESC";
            Log.i(LOG, selectQuery);
            c = db.rawQuery(selectQuery, null);
            if (c.moveToFirst()) {
                do {
                    try {
                        int id = c.getInt(c.getColumnIndex(KEY_ID));
                        int idAssisted = c.getInt(c.getColumnIndex(KEY_IDASSISTED));
                        Date data = sdf.parse(c.getString(c.getColumnIndex(KEY_DATA)));
                        String name = c.getString(c.getColumnIndex(KEY_DATANAME));
                        String type = c.getString(c.getColumnIndex(KEY_DATATYPE));
                        Double value = c.getDouble(c.getColumnIndex(KEY_DATAVALUE));
                        String unit = c.getString(c.getColumnIndex(KEY_DATAUNIT));
                        String send = c.getString(c.getColumnIndex(KEY_SEND));
                        DataPoint misurazione = new DataPoint(id, idAssisted, data, name, type, value, unit, send);
                        lista.add(misurazione);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } while (c.moveToNext());
            }
        } catch (SQLiteException e) {
            Log.e(LOG, "getMisurazioniBoadyWater failed", e);
        } finally {
            if (c != null) {
                c.close();
            }
            close();
        }
        return lista;
    }

    /**
     * Lettura tutte misurazioni BoadyMuscle_HIS
     *
     * @return elenco misurazioni
     */
    public ArrayList<DataPoint> getMisurazioniBoadyMuscle() {
        ArrayList<DataPoint> lista = new ArrayList<>();
        Cursor c = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            SQLiteDatabase db = this.getReadableDatabase();
            String selectQuery = "SELECT  * FROM " + TABLE_BILANCIA + " WHERE "
                    + KEY_DATANAME + " = 'BoadyMuscle_HIS' ORDER BY " + KEY_DATA + " DESC";
            Log.i(LOG, selectQuery);
            c = db.rawQuery(selectQuery, null);
            if (c.moveToFirst()) {
                do {
                    try {
                        int id = c.getInt(c.getColumnIndex(KEY_ID));
                        int idAssisted = c.getInt(c.getColumnIndex(KEY_IDASSISTED));
                        Date data = sdf.parse(c.getString(c.getColumnIndex(KEY_DATA)));
                        String name = c.getString(c.getColumnIndex(KEY_DATANAME));
                        String type = c.getString(c.getColumnIndex(KEY_DATATYPE));
                        Double value = c.getDouble(c.getColumnIndex(KEY_DATAVALUE));
                        String unit = c.getString(c.getColumnIndex(KEY_DATAUNIT));
                        String send = c.getString(c.getColumnIndex(KEY_SEND));
                        DataPoint misurazione = new DataPoint(id, idAssisted, data, name, type, value, unit, send);
                        lista.add(misurazione);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } while (c.moveToNext());
            }
        } catch (SQLiteException e) {
            Log.e(LOG, "getMisurazioniBoadyMuscle failed", e);
        } finally {
            if (c != null) {
                c.close();
            }
            close();
        }
        return lista;
    }

    /**
     * Lettura misurazioni HIS non inviate
     *
     * @return elenco misurazioni
     */
    public ArrayList<DataPoint> getElencoMisurazioniHIS_NotSend() {
        ArrayList<DataPoint> lista = new ArrayList<>();
        Cursor c = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            SQLiteDatabase db = this.getReadableDatabase();
            String selectQuery = "SELECT  * FROM " + TABLE_BILANCIA + " WHERE "
                    + KEY_SEND + " = 'false'";
            Log.i(LOG, selectQuery);
            c = db.rawQuery(selectQuery, null);
            if (c.moveToFirst()) {
                do {
                    try {
                        int id = c.getInt(c.getColumnIndex(KEY_ID));
                        int idAssisted = c.getInt(c.getColumnIndex(KEY_IDASSISTED));
                        Date data = sdf.parse(c.getString(c.getColumnIndex(KEY_DATA)));
                        String name = c.getString(c.getColumnIndex(KEY_DATANAME));
                        String type = c.getString(c.getColumnIndex(KEY_DATATYPE));
                        Double value = c.getDouble(c.getColumnIndex(KEY_DATAVALUE));
                        String unit = c.getString(c.getColumnIndex(KEY_DATAUNIT));
                        String send = c.getString(c.getColumnIndex(KEY_SEND));
                        Log.i(SYNC_NOTSEND, "----------------------");
                        Log.i(SYNC_NOTSEND + KEY_ID, String.valueOf(id));
                        Log.i(SYNC_NOTSEND + KEY_DATA, sdf.format(data));
                        Log.i(SYNC_NOTSEND + KEY_IDASSISTED, String.valueOf(idAssisted));
                        Log.i(SYNC_NOTSEND + KEY_DATANAME, name);
                        Log.i(SYNC_NOTSEND + KEY_DATATYPE, type);
                        Log.i(SYNC_NOTSEND + KEY_DATAVALUE, String.valueOf(value));
                        Log.i(SYNC_NOTSEND + KEY_DATAUNIT, unit);
                        Log.i(SYNC_NOTSEND + KEY_SEND, String.valueOf(send));
                        Log.i(SYNC_NOTSEND, "----------------------");
                        DataPoint misurazione = new DataPoint(id, idAssisted, data, name, type, value, unit, send);
                        lista.add(misurazione);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } while (c.moveToNext());
            }
        } catch (SQLiteException e) {
            Log.e(LOG, "getElencoMisurazioniHIS_NotSend failed", e);
        } finally {
            if (c != null) {
                c.close();
            }
            close();
        }
        return lista;
    }

    /**
     * Lettura misurazioni HIS inviate
     *
     * @return elenco misurazioni
     */
    public ArrayList<DataPoint> getElencoMisurazioniHIS_Send() {
        ArrayList<DataPoint> lista = new ArrayList<>();
        Cursor c = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            SQLiteDatabase db = this.getReadableDatabase();
            String selectQuery = "SELECT  * FROM " + TABLE_BILANCIA + " WHERE "
                    + KEY_SEND + " = 'true'";
            Log.i(LOG, selectQuery);
            c = db.rawQuery(selectQuery, null);
            if (c.moveToFirst()) {
                do {
                    try {
                        int id = c.getInt(c.getColumnIndex(KEY_ID));
                        int idAssisted = c.getInt(c.getColumnIndex(KEY_IDASSISTED));
                        Date data = sdf.parse(c.getString(c.getColumnIndex(KEY_DATA)));
                        String name = c.getString(c.getColumnIndex(KEY_DATANAME));
                        String type = c.getString(c.getColumnIndex(KEY_DATATYPE));
                        Double value = c.getDouble(c.getColumnIndex(KEY_DATAVALUE));
                        String unit = c.getString(c.getColumnIndex(KEY_DATAUNIT));
                        String send = c.getString(c.getColumnIndex(KEY_SEND));
                        Log.i(SYNC_SEND, "----------------------");
                        Log.i(SYNC_SEND + KEY_ID, String.valueOf(id));
                        Log.i(SYNC_SEND + KEY_DATA, sdf.format(data));
                        Log.i(SYNC_SEND + KEY_IDASSISTED, String.valueOf(idAssisted));
                        Log.i(SYNC_SEND + KEY_DATANAME, name);
                        Log.i(SYNC_SEND + KEY_DATATYPE, type);
                        Log.i(SYNC_SEND + KEY_DATAVALUE, String.valueOf(value));
                        Log.i(SYNC_SEND + KEY_DATAUNIT, unit);
                        Log.i(SYNC_SEND + KEY_SEND, String.valueOf(send));
                        Log.i(SYNC_SEND, "----------------------");
                        DataPoint misurazione = new DataPoint(id, idAssisted, data, name, type, value, unit, send);
                        lista.add(misurazione);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } while (c.moveToNext());
            }
        } catch (SQLiteException e) {
            Log.e(LOG, "getElencoMisurazioniHIS_Send failed", e);
        } finally {
            if (c != null) {
                c.close();
            }
            close();
        }
        return lista;
    }

    /**
     * Update Bilancia
     *
     * @param misurazione oggetto misurazione
     * @return valore true o false
     */
    public boolean updateBilancia(DataPoint misurazione) {
        boolean result = false;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(KEY_IDASSISTED, misurazione.getIdAssisted());
            values.put(KEY_SEND, misurazione.getSend());
            result = db.update(TABLE_BILANCIA, values, KEY_ID + "='" + misurazione.getId() + "'", null) > 0;
            if (result == false) {
                Log.e(LOG, "updateBilancia Update failed");
            }
        } catch (SQLiteException e) {
            Log.e(LOG, "updateBilancia failed", e);
        } finally {
            close();
        }
        return result;
    }
}