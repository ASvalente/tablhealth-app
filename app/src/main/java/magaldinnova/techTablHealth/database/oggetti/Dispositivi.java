package magaldinnova.techTablHealth.database.oggetti;

import java.io.Serializable;

/**
 * Dispositivi
 * @author Magaldi Innova srl
 */
public class Dispositivi implements Serializable {

    private int ID;
    private String nome;
    private int immagine;
    private String mac;

    //Costruttori
    public Dispositivi() {
    }

    public Dispositivi(int ID, String nome, String mac) {
        this.ID = ID;
        this.nome = nome;
        this.mac = mac;
    }

    public Dispositivi(String nome, String mac) {
        this.nome = nome;
        this.mac = mac;
    }

    public Dispositivi(String nome, int immagine) {
        this.nome = nome;
        this.immagine = immagine;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public int getImmagine() {
        return immagine;
    }

    public void setImmagine(int immagine) {
        this.immagine = immagine;
    }
}
