package magaldinnova.techTablHealth.database.oggetti;

import org.ksoap2.serialization.SoapObject;

import java.io.Serializable;
import java.util.Date;

/**
 * DataPoint
 * @author Magaldi Innova srl
 */
public class DataPoint implements Serializable {

    int id;
    int idAssisted;
    Date data;
    String name;
    String type;
    Double value;
    String unit;
    String send;

    public DataPoint(int id, int idAssisted, Date data, String name, String type, Double value, String unit, String send) {
        this.id = id;
        this.idAssisted = idAssisted;
        this.data = data;
        this.name = name;
        this.type = type;
        this.value = value;
        this.unit = unit;
        this.send = send;
    }

    public DataPoint(Date data, String name, String type, Double value, String unit, String send) {
        this.data = data;
        this.name = name;
        this.type = type;
        this.value = value;
        this.unit = unit;
        this.send = send;
    }

    public DataPoint() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdAssisted() {
        return idAssisted;
    }

    public void setIdAssisted(int idAssisted) {
        this.idAssisted = idAssisted;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getSend() {
        return send;
    }

    public void setSend(String send) {
        this.send = send;
    }

    /**
     * Creazione SoapObject per invio rilevazioni
     * @return
     */
    public SoapObject createSoapObject(int ID, int idDispositivo){
        SoapObject dati = new SoapObject();
        dati.addProperty("IdRilevazione" , ID);
        dati.addProperty("DataName" , getName());
        dati.addProperty("DataValue", getValue());
        dati.addProperty("DataType", getType());
        dati.addProperty("DataUnit", getUnit());
        dati.addProperty("DataDbId", "0");
        dati.addProperty("IdDispositivo", idDispositivo);
        dati.addProperty("IdPai", "0");
        return dati;
    }
}
