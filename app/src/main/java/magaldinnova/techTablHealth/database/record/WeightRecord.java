package magaldinnova.techTablHealth.database.record;

import java.util.Date;

/**
 * Oggetto Misurazione Bilancia
 * @author Magaldi Innova srl
 */
public class WeightRecord {

    private Date mMeasureTime;
    private int scale_type;
    private int eta;
    private int altezza;
    private double peso;
    private double grasso_corporeo;
    private double ossa;
    private double muscoli;
    private int grasso_viscerale;
    private double acqua_corporea;
    private int bmr;
    private double bmi;

    public WeightRecord(Date mMeasureTime, int scale_type, int eta, int altezza, double peso, double grasso_corporeo, double ossa, double muscoli, int grasso_viscerale, double acqua_corporea, int bmr, double bmi) {
        this.mMeasureTime = mMeasureTime;
        this.scale_type = scale_type;
        this.eta = eta;
        this.altezza = altezza;
        this.peso = peso;
        this.grasso_corporeo = grasso_corporeo;
        this.ossa = ossa;
        this.muscoli = muscoli;
        this.grasso_viscerale = grasso_viscerale;
        this.acqua_corporea = acqua_corporea;
        this.bmr = bmr;
        this.bmi = bmi;
    }

    public WeightRecord(Date mMeasureTime, double peso, double grasso_corporeo, double ossa, double muscoli, int grasso_viscerale, double acqua_corporea, int bmr, double bmi) {
        this.mMeasureTime = mMeasureTime;
        this.peso = peso;
        this.grasso_corporeo = grasso_corporeo;
        this.ossa = ossa;
        this.muscoli = muscoli;
        this.grasso_viscerale = grasso_viscerale;
        this.acqua_corporea = acqua_corporea;
        this.bmr = bmr;
        this.bmi = bmi;
    }

    public Date getmMeasureTime() {
        return mMeasureTime;
    }

    public void setmMeasureTime(Date mMeasureTime) {
        this.mMeasureTime = mMeasureTime;
    }

    public int getScale_type() {
        return scale_type;
    }

    public void setScale_type(int scale_type) {
        this.scale_type = scale_type;
    }

    public int getEta() {
        return eta;
    }

    public void setEta(int eta) {
        this.eta = eta;
    }

    public int getAltezza() {
        return altezza;
    }

    public void setAltezza(int altezza) {
        this.altezza = altezza;
    }

    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public double getGrasso_corporeo() {
        return grasso_corporeo;
    }

    public void setGrasso_corporeo(double grasso_corporeo) {
        this.grasso_corporeo = grasso_corporeo;
    }

    public double getOssa() {
        return ossa;
    }

    public void setOssa(double ossa) {
        this.ossa = ossa;
    }

    public double getMuscoli() {
        return muscoli;
    }

    public void setMuscoli(double muscoli) {
        this.muscoli = muscoli;
    }

    public int getGrasso_viscerale() {
        return grasso_viscerale;
    }

    public void setGrasso_viscerale(int grasso_viscerale) {
        this.grasso_viscerale = grasso_viscerale;
    }

    public double getAcqua_corporea() {
        return acqua_corporea;
    }

    public void setAcqua_corporea(double acqua_corporea) {
        this.acqua_corporea = acqua_corporea;
    }

    public int getBmr() {
        return bmr;
    }

    public void setBmr(int bmr) {
        this.bmr = bmr;
    }

    public double getBmi() {return bmi; }

    public void setBmi(double bmi) { this.bmi = bmi; }
}
