package magaldinnova.techTablHealth.database.record;

import java.util.Date;

/**
 * Oggetto Misurazione Glucometro
 * @author Magaldi Innova srl
 */
public class BloodGlucoseRecord {

    private Date mMeasureTime;
    private boolean mTransmitted;
    private int mGlucoseValue;
    private int mCodeNo;
    private int mAmbientValue;
    private int mType; //General(0), AC(1), PC(2), QC(3),
    private int mType2; //HEMATOCRIT(6), KETONE(7);


    public BloodGlucoseRecord(Date mMeasureTime, boolean mTransmitted, int mGlucoseValue, int mCodeNo, int mAmbientValue, int mType, int mType2){
        this.mMeasureTime=mMeasureTime;
        this.mTransmitted=mTransmitted;
        this.mGlucoseValue=mGlucoseValue;
        this.mCodeNo=mCodeNo;
        this.mAmbientValue=mAmbientValue;
        this.mType=mType;
        this.mType2=mType2;
    }

    public BloodGlucoseRecord(Date mMeasureTime, int mGlucoseValue, int mType2) {
        this.mMeasureTime = mMeasureTime;
        this.mGlucoseValue = mGlucoseValue;
        this.mType2 = mType2;
    }

    public Date getmMeasureTime() {
        return mMeasureTime;
    }

    public void setmMeasureTime(Date mMeasureTime) {
        this.mMeasureTime = mMeasureTime;
    }

    public boolean ismTransmitted() {
        return mTransmitted;
    }

    public void setmTransmitted(boolean mTransmitted) {
        this.mTransmitted = mTransmitted;
    }

    public int getmGlucoseValue() {
        return mGlucoseValue;
    }

    public void setmGlucoseValue(int mGlucoseValue) {
        this.mGlucoseValue = mGlucoseValue;
    }

    public int getmCodeNo() {
        return mCodeNo;
    }

    public void setmCodeNo(int mCodeNo) {
        this.mCodeNo = mCodeNo;
    }

    public int getmAmbientValue() {
        return mAmbientValue;
    }

    public void setmAmbientValue(int mAmbientValue) {
        this.mAmbientValue = mAmbientValue;
    }

    public int getmType() {
        return mType;
    }

    public void setmType(int mType) {
        this.mType = mType;
    }

    public int getmType2() {
        return mType2;
    }

    public void setmType2(int mType2) {
        this.mType2 = mType2;
    }
}
