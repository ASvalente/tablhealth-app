package magaldinnova.techTablHealth.database.record;

import java.util.Date;

/**
 * Oggetto Misurazione Sfigmomanometro
 * @author Magaldi Innova srl
 */
public class BloodPressureRecord
{
    private Date mMeasureTime;
    private int mSystolicValue;
    private int mDiastolicValue;
    private int mMapValue;
    private int mPulseValue;
    private boolean mArrhy;
    private boolean mTransmitted;
    private boolean mAvg;
    private boolean mEve;
    private int mIHB;
    private int mUser;

    public BloodPressureRecord(Date measureTime, int user, int systolicValue, int diastolicValue, int mapValue, int pulseValue, boolean arrhy, boolean transmitted, boolean avg, boolean eve, int ihb)
    {
        this.mMeasureTime = measureTime;
        this.mSystolicValue = systolicValue;
        this.mDiastolicValue = diastolicValue;
        this.mMapValue = mapValue;
        this.mPulseValue = pulseValue;
        this.mArrhy = arrhy;
        this.mTransmitted = transmitted;
        this.mAvg = avg;
        this.mEve = eve;
        this.mIHB = ihb;
        this.mUser = user;
    }

    public BloodPressureRecord(Date mMeasureTime, int mSystolicValue, int mDiastolicValue, int mMapValue, int mPulseValue) {
        this.mMeasureTime = mMeasureTime;
        this.mSystolicValue = mSystolicValue;
        this.mDiastolicValue = mDiastolicValue;
        this.mMapValue = mMapValue;
        this.mPulseValue = mPulseValue;
    }

    public Date getmMeasureTime() {
        return mMeasureTime;
    }

    public void setmMeasureTime(Date mMeasureTime) {
        this.mMeasureTime = mMeasureTime;
    }

    public int getmSystolicValue() {
        return mSystolicValue;
    }

    public void setmSystolicValue(int mSystolicValue) {
        this.mSystolicValue = mSystolicValue;
    }

    public int getmDiastolicValue() {
        return mDiastolicValue;
    }

    public void setmDiastolicValue(int mDiastolicValue) {
        this.mDiastolicValue = mDiastolicValue;
    }

    public int getmMapValue() {
        return mMapValue;
    }

    public void setmMapValue(int mMapValue) {
        this.mMapValue = mMapValue;
    }

    public int getmPulseValue() {
        return mPulseValue;
    }

    public void setmPulseValue(int mPulseValue) {
        this.mPulseValue = mPulseValue;
    }

    public boolean ismArrhy() {
        return mArrhy;
    }

    public void setmArrhy(boolean mArrhy) {
        this.mArrhy = mArrhy;
    }

    public boolean ismTransmitted() {
        return mTransmitted;
    }

    public void setmTransmitted(boolean mTransmitted) {
        this.mTransmitted = mTransmitted;
    }

    public boolean ismAvg() {
        return mAvg;
    }

    public void setmAvg(boolean mAvg) {
        this.mAvg = mAvg;
    }

    public boolean ismEve() {
        return mEve;
    }

    public void setmEve(boolean mEve) {
        this.mEve = mEve;
    }

    public int getmIHB() {
        return mIHB;
    }

    public void setmIHB(int mIHB) {
        this.mIHB = mIHB;
    }

    public int getmUser() {
        return mUser;
    }

    public void setmUser(int mUser) {
        this.mUser = mUser;
    }
}