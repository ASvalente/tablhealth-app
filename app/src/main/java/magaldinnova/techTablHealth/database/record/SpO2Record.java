package magaldinnova.techTablHealth.database.record;


import java.util.Date;

/**
 * Oggetto Misurazione Ossimetro
 * @author Magaldi Innova srl
 */
public class SpO2Record
{
    private Date mMeasureTime;
    private int mSpO2;
    private int mPulse;
    private Double mPerfIndex;
    private String mAlarm;
    private Double mBatteria;


    public SpO2Record(Date measureTime, int spO2, int pulse, Double perfIndex)
    {
        this.mMeasureTime = measureTime;
        this.mSpO2 = spO2;
        this.mPulse = pulse;
        this.mPerfIndex=perfIndex;
    }

    public SpO2Record(Date mMeasureTime, int mSpO2, int mPulse, Double mPerfIndex, String mAlarm, Double mBatteria) {
        this.mMeasureTime = mMeasureTime;
        this.mSpO2 = mSpO2;
        this.mPulse = mPulse;
        this.mPerfIndex = mPerfIndex;
        this.mAlarm = mAlarm;
        this.mBatteria = mBatteria;
    }

    public Date getmMeasureTime() {
        return mMeasureTime;
    }

    public void setmMeasureTime(Date mMeasureTime) {
        this.mMeasureTime = mMeasureTime;
    }

    public int getmSpO2() {
        return mSpO2;
    }

    public void setmSpO2(int mSpO2) {
        this.mSpO2 = mSpO2;
    }

    public int getmPulse() {
        return mPulse;
    }

    public void setmPulse(int mPulse) {
        this.mPulse = mPulse;
    }

    public Double getmPerfIndex() {
        return mPerfIndex;
    }

    public void setmPerfIndex(Double mPerfIndex) {
        this.mPerfIndex = mPerfIndex;
    }

    public String getmAlarm() {
        return mAlarm;
    }

    public void setmAlarm(String mAlarm) {
        this.mAlarm = mAlarm;
    }

    public Double getmBatteria() {
        return mBatteria;
    }

    public void setmBatteria(Double mBatteria) {
        this.mBatteria = mBatteria;
    }
}