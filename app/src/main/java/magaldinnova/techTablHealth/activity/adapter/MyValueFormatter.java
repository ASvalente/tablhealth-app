package magaldinnova.techTablHealth.activity.adapter;


import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.text.DecimalFormat;

/**
 * Formattatore dei Dati Rilevati device (Usato nei Grafici)
 * @author Magaldi Innova srl
 */
public class MyValueFormatter implements ValueFormatter {

    private DecimalFormat mFormat;

    public MyValueFormatter(String dispositivo) {
        if(dispositivo.equals("Ossimetro")){
            mFormat = new DecimalFormat("###"); // use one decimal
        }
        if(dispositivo.equals("Sfigmomanometro")){
            mFormat = new DecimalFormat("###"); // use one decimal
        }
        if(dispositivo.equals("Glucometro")){
            mFormat = new DecimalFormat("###"); // use one decimal
        }
        if(dispositivo.equals("Bilancia")){
            mFormat = new DecimalFormat("###.0"); // use one decimal
        }


    }

    @Override
    public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {
        return mFormat.format(value);
    }
}
