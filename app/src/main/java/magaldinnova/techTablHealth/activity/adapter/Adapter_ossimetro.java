package magaldinnova.techTablHealth.activity.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import magaldinnova.techTablHealth.database.oggetti.DataPoint;
import magaldinnova.techTablHealth.R;

/**
 * Adapter Lista Misurazioni Ossimetro
 * @author Magaldi Innova srl
 */
public class Adapter_ossimetro extends BaseAdapter{

    ArrayList<DataPoint> mOssimetro, mOssimetro_Pulse;

    public Adapter_ossimetro(ArrayList<DataPoint> mOssimetro, ArrayList<DataPoint> mOssimetro_Pulse) {
        this.mOssimetro = mOssimetro;
        this.mOssimetro_Pulse = mOssimetro_Pulse;
    }

    @Override
    public int getCount() {
        return mOssimetro.size();
    }

    @Override
    public DataPoint getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Holder holder=new Holder();
        View rowView = LayoutInflater.from(parent.getContext()).inflate(R.layout.oss_adapter, parent, false);
        holder.spo2_value = (TextView)rowView.findViewById(R.id.spo2_value);
        holder.pr_value = (TextView)rowView.findViewById(R.id.pr_value);
        holder.data_value = (TextView)rowView.findViewById(R.id.data_value);

        DataPoint itemSPO = mOssimetro.get(position);
        DataPoint itemSPO_pulse = mOssimetro_Pulse.get(position);

        holder.spo2_value.setText(new BigDecimal(itemSPO.getValue()).setScale(0,BigDecimal.ROUND_HALF_UP).toString() + " " + itemSPO.getUnit());
        holder.pr_value.setText(new BigDecimal(itemSPO_pulse.getValue()).setScale(0,BigDecimal.ROUND_HALF_UP).toString() + " " + itemSPO_pulse.getUnit());
        holder.data_value.setText(sdf.format(itemSPO.getData()));

        return rowView;
    }

    public class Holder
    {
        TextView spo2_value, pr_value, data_value;
    }
}
