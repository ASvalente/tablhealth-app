package magaldinnova.techTablHealth.activity.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import magaldinnova.techTablHealth.database.oggetti.DataPoint;
import magaldinnova.techTablHealth.R;

/**
 * Adapter Lista Misurazioni Glucometro
 * @author Magaldi Innova srl
 */
public class Adapter_glucometro extends BaseAdapter{

    ArrayList<DataPoint> mGlucometro;

    public Adapter_glucometro(ArrayList<DataPoint> mGlucometro) {
        this.mGlucometro = mGlucometro;
    }

    @Override
    public int getCount() {
        return mGlucometro.size();
    }

    @Override
    public DataPoint getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Holder holder=new Holder();
        View rowView = LayoutInflater.from(parent.getContext()).inflate(R.layout.glu_adapter, parent, false);
        holder.gli_value = (TextView)rowView.findViewById(R.id.gli_value);
        holder.data_value = (TextView)rowView.findViewById(R.id.data_value);

        DataPoint itemGli = mGlucometro.get(position);

        holder.gli_value.setText(new BigDecimal(itemGli.getValue()).setScale(0,BigDecimal.ROUND_HALF_UP).toString() + " " + itemGli.getUnit());
        holder.data_value.setText(sdf.format(itemGli.getData()));

        return rowView;
    }

    public class Holder
    {
        TextView gli_value, data_value;
    }
}
