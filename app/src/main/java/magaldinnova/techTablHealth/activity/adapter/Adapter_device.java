package magaldinnova.techTablHealth.activity.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import magaldinnova.techTablHealth.database.oggetti.Dispositivi;
import magaldinnova.techTablHealth.R;

/**
 * Adapter Lista Device
 * @author Magaldi Innova srl
 */
public class Adapter_device extends BaseAdapter{

    ArrayList<Dispositivi> lista;

    public Adapter_device(ArrayList<Dispositivi> lista) {
        this.lista = lista;
    }

    @Override
    public int getCount() {
        return lista.size();
    }

    @Override
    public Dispositivi getItem(int position) {
        return lista.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Holder holder=new Holder();
        View rowView = LayoutInflater.from(parent.getContext()).inflate(R.layout.dev_adapter, parent, false);
        holder.txt_device = (TextView)rowView.findViewById(R.id.txt_device);
        holder.img_device = (ImageView)rowView.findViewById(R.id.img_device);

        Dispositivi item = getItem(position);

        holder.txt_device.setText(item.getNome());
        holder.img_device.setImageResource(item.getImmagine());

        return rowView;
    }

    public class Holder
    {
        TextView txt_device;
        ImageView img_device;
    }
}
