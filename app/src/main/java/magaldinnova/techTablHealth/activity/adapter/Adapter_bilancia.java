package magaldinnova.techTablHealth.activity.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import magaldinnova.techTablHealth.R;

import magaldinnova.techTablHealth.database.oggetti.DataPoint;

/**
 * Adapter Lista Misutazioni Bilancia
 * @author Magaldi Innova srl
 */
public class Adapter_bilancia extends BaseAdapter{

    ArrayList<DataPoint> mBilancia;

    public Adapter_bilancia(ArrayList<DataPoint> mBilancia) {
        this.mBilancia = mBilancia;
    }

    @Override
    public int getCount() {
        return mBilancia.size();
    }

    @Override
    public DataPoint getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Holder holder=new Holder();
        View rowView = LayoutInflater.from(parent.getContext()).inflate(R.layout.bil_adapter, parent, false);
        holder.bil_value = (TextView)rowView.findViewById(R.id.bil_value);
        holder.data_value = (TextView)rowView.findViewById(R.id.data_value);

        DataPoint itemBil = mBilancia.get(position);

        holder.bil_value.setText(new BigDecimal(itemBil.getValue()).setScale(1,BigDecimal.ROUND_HALF_UP).toString() + " " + itemBil.getUnit());
        holder.data_value.setText(sdf.format(itemBil.getData()));

        return rowView;
    }

    public class Holder
    {
        TextView bil_value, data_value;
    }
}
