package magaldinnova.techTablHealth.activity.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import magaldinnova.techTablHealth.database.oggetti.DataPoint;
import magaldinnova.techTablHealth.R;

/**
 * Adapter Lista Misurazione Sfigmomanometro
 * @author Magaldi Innova srl
 */
public class Adapter_sfigmomanometro extends BaseAdapter{

    ArrayList<DataPoint> mSfigmomanometro_sys, mSfigmomanometro_dia;

    public Adapter_sfigmomanometro(ArrayList<DataPoint> mSfigmomanometro_sys, ArrayList<DataPoint> mSfigmomanometro_dia) {
        this.mSfigmomanometro_sys = mSfigmomanometro_sys;
        this.mSfigmomanometro_dia = mSfigmomanometro_dia;
    }

    @Override
    public int getCount() {
        return mSfigmomanometro_sys.size();
    }

    @Override
    public DataPoint getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Holder holder=new Holder();
        View rowView = LayoutInflater.from(parent.getContext()).inflate(R.layout.sfig_adapter, parent, false);
        holder.sys_value = (TextView)rowView.findViewById(R.id.sys_value);
        holder.dia_value = (TextView)rowView.findViewById(R.id.dia_value);
        holder.data_value = (TextView)rowView.findViewById(R.id.data_value);

        DataPoint itemSys = mSfigmomanometro_sys.get(position);
        DataPoint itemDia = mSfigmomanometro_dia.get(position);

        holder.sys_value.setText(new BigDecimal(itemSys.getValue()).setScale(0,BigDecimal.ROUND_HALF_UP).toString() + " " + itemSys.getUnit());
        holder.dia_value.setText(new BigDecimal(itemDia.getValue()).setScale(0,BigDecimal.ROUND_HALF_UP).toString() + " " + itemSys.getUnit());
        holder.data_value.setText(sdf.format(itemSys.getData()));

        return rowView;
    }

    public class Holder
    {
        TextView sys_value, dia_value, data_value;
    }
}
