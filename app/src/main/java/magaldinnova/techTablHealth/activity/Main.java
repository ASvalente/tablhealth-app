package magaldinnova.techTablHealth.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import magaldinnova.techTablHealth.R;
import magaldinnova.techTablHealth.activity.areaTelemonitoraggio.Area_Telemonitoraggio;

/**
 * Modulo SplashScreen Iniziale
 * @author Magaldi Innova srl
 */
public class Main extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

        TextView text_version = findViewById(R.id.text_version);
        Context context = getApplicationContext(); // or activity.getApplicationContext()
        PackageManager packageManager = context.getPackageManager();
        String packageName = context.getPackageName();

        String myVersionName; // initialize String

        try {
            myVersionName = packageManager.getPackageInfo(packageName, 0).versionName;
            text_version.setText(String.format("Versione: %s", myVersionName));
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        CheckDevice task = new CheckDevice();
        task.execute();

        Log.e("Device Unique ID", "" +getDeviceUniqueID(Main.this));
        Log.e("DEvice NAME", "" +getPhoneName());
    }

    /**
     * Controllo se è un tablet o smartphone
     * @param context contesto
     * @return booleano
     */
    public static boolean isTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }

    /**
     * Get ID univoco del device
     * @param activity contesto
     * @return codice device
     */
    public String getDeviceUniqueID(Activity activity){
        @SuppressLint("HardwareIds") String device_unique_id = Settings.Secure.getString(activity.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        return device_unique_id;
    }


    /**
     * Get nome dispositivo
     * @return Nome dispositivo
     */
    public String getPhoneName() {
        BluetoothAdapter myDevice = BluetoothAdapter.getDefaultAdapter();
        return myDevice.getName();
    }

    /**
     * Sleep di 3 secondi e poi passiamo all'areaMonitoraggio
     */
    public class CheckDevice extends AsyncTask<Void, Void, Boolean>{

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                return false;
            }
            return isTablet(getApplicationContext());
        }

        @Override
        protected void onPostExecute(final Boolean tablet) {
            Intent a = new Intent(Main.this, Area_Telemonitoraggio.class);
            a.putExtra("MAC", getDeviceUniqueID(Main.this));
            a.putExtra("Device", "");
            startActivity(a);
        }

    }
}
