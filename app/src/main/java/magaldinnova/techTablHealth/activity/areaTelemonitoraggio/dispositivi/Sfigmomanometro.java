package magaldinnova.techTablHealth.activity.areaTelemonitoraggio.dispositivi;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import magaldinnova.techTablHealth.activity.adapter.DevicesAdapter;
import magaldinnova.techTablHealth.activity.detector.MobileInternetConnectionDetector;
import magaldinnova.techTablHealth.activity.detector.WIFIInternetConnectionDetector;
import magaldinnova.techTablHealth.database.dbHelper.DatabaseHelper;
import magaldinnova.techTablHealth.database.oggetti.DataPoint;
import magaldinnova.techTablHealth.database.oggetti.Dispositivi;
import magaldinnova.techTablHealth.library.BluetoothSPP;
import magaldinnova.techTablHealth.moduli.TD3128;
import magaldinnova.techTablHealth.R;
import magaldinnova.techTablHealth.database.record.BloodPressureRecord;
import magaldinnova.techTablHealth.webService.oggetti.InsertDataMeasurementResponse;
import magaldinnova.techTablHealth.webService.oggetti.InsertMeasurementResponse;
import magaldinnova.techTablHealth.webService.oggetti.Rilevazione;
import magaldinnova.techTablHealth.webService.WebServiceTechTablHealth;
import magaldinnova.techTablHealth.library.Device;

/**
 * Modulo Sfigmomanometro
 * @author Magaldi Innova srl
 */
public class Sfigmomanometro extends AppCompatActivity{

    //Caricamento Variabili
    private static final int REQUEST_ENABLE_BLUETOOTH = 1;
    private Context thisContext;
    private MobileInternetConnectionDetector dm;
    private WIFIInternetConnectionDetector dw;
    private android.app.AlertDialog.Builder alertDialogBuilder;

    //Caricamento Bluetooth
    private BluetoothSPP mSmoothBluetooth;
    private int i=0;
    private int state = 0;
    private Dispositivi dispositivi_selezionato, Sfigmomanometro;
    private String MAC;

    //Parametri risposta
    private int[] risposta;
    private int j=0;

    //Caricamento oggettoDispositivo
    private TD3128 td3128;
    private ArrayList<DataPoint> misurazioni;

    //Autoconnessione
    private CountDownTimer countdown;

    //Caricamento elementi grafici
    private RelativeLayout layout_header;
    private LinearLayout layout_menu, layout_mesure, layout_sync, layout_arrth;
    private TextView sys_value;
    private TextView dia_value;
    private TextView pr_value;
    private TextView data_text;
    private TextView progress_text;
    private TextView sync_message;
    private RoundCornerProgressBar progress_mesure;
    private AlertDialog.Builder alert;
    private ProgressBar progress_sync;

    //Caricamento DB
    private DatabaseHelper databaseHelper;
    private ArrayList<DataPoint>  sync_lista_NotSend;

    //Caricamento VOCE
    private TextToSpeech t1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sfigmomanometro);

        getSupportActionBar().setElevation(0);

        // --------- CARICAMENTO ELEMENTI GRAFICI ---------- //
        //MISURAZIONI
        layout_mesure=(LinearLayout)findViewById(R.id.layout_measure);
        layout_arrth = (LinearLayout)findViewById(R.id.layout_arrth);
        sys_value = (TextView)findViewById(R.id.sys_value);
        dia_value = (TextView)findViewById(R.id.dia_value);
        pr_value = (TextView)findViewById(R.id.pr_value);
        data_text = (TextView)findViewById(R.id.data_text);
        progress_mesure = (RoundCornerProgressBar) findViewById(R.id.progress_mesure);
        progress_mesure.setMax(5);
        progress_mesure.setProgressColor(getResources().getColor(R.color.status));
        progress_mesure.setProgressBackgroundColor(Color.parseColor("#e4ebff"));
        //STATUS CONNESSIONE
        layout_header = (RelativeLayout)findViewById(R.id.layout_header);
        progress_text = (TextView)findViewById(R.id.progress_text);
        // INVIO DATI
        layout_menu = (LinearLayout) findViewById(R.id.layout_menu);
        LinearLayout btn_fine = (LinearLayout) findViewById(R.id.btn_fine);
        LinearLayout btn_nuova_misurazione = (LinearLayout) findViewById(R.id.btn_nuova_misurazione);
        //SINCRONIZZAZIONE
        layout_sync = (LinearLayout)findViewById(R.id.layout_sync);
        sync_message = (TextView)findViewById(R.id.sync_message);
        progress_sync = (ProgressBar)findViewById(R.id.progress_sync);
        // --------- CARICAMENTO ELEMENTI GRAFICI ---------- //

        // Create a new instance of Bluetooth
        mSmoothBluetooth = new BluetoothSPP(this);
        mSmoothBluetooth.setListener(mListener);
        td3128 = new TD3128();

        misurazioni = new ArrayList<>();

        thisContext = this;

        //Controllo connessione
        dm=new MobileInternetConnectionDetector(thisContext);
        dw=new WIFIInternetConnectionDetector(thisContext);
        alertDialogBuilder = new android.app.AlertDialog.Builder(thisContext);

        //SetTitle
        getSupportActionBar().setTitle("Sfigmomanometro");

        layout_mesure.setVisibility(View.INVISIBLE);

        //Caricamento Assistito
        Intent intent= getIntent();
        Bundle b = intent.getExtras();

        if(b!=null)
        {
            MAC = b.getString("MAC");
            Sfigmomanometro = (Dispositivi)b.getSerializable("Sfigmomanometro");
        }

        //Aperture DB
        databaseHelper = new DatabaseHelper(this);
        dispositivi_selezionato = databaseHelper.getDispositivoByName("Sfigmomanometro");

        if(Sfigmomanometro!=null) {
            if (!dispositivi_selezionato.getMac().equals(Sfigmomanometro.getMac())) {
                dispositivi_selezionato = Sfigmomanometro;
                databaseHelper.updateDevice(Sfigmomanometro);
            }
        }


        //Autoconnesione
        countdown = new CountDownTimer(1000, 1000) {
            public void onTick(long millisUntilFinished) {
                Log.i("CountDown Thread: " + Thread.currentThread().getId(), getString(R.string.tryconnect) + (int) (millisUntilFinished / 1000) % 60);
                progress_text.setVisibility(View.VISIBLE);
                progress_text.setText(getString(R.string.connecting));
            }

            public void onFinish() {
                if (dispositivi_selezionato != null) {
                    layout_header.setVisibility(View.VISIBLE);
                    progress_text.setText(getString(R.string.connecting));
                    mSmoothBluetooth.connect(dispositivi_selezionato.getMac());
                }else{
                    Log.i("CountDown Thread: " + Thread.currentThread().getId(), getString(R.string.pair));
                    layout_header.setVisibility(View.VISIBLE);
                    progress_text.setText(getString(R.string.pair));
                }
                Thread.currentThread().interrupt();
            }
        };

        btn_fine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                databaseHelper.close();
                finish();

            }
        });

        btn_nuova_misurazione.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("Nuova Misurazione", String.valueOf(misurazioni.size()));
                if(misurazioni.size()!=0){
                    j=0;
                    risposta=null;
                    misurazioni.clear();
                    countdown.start();
                }
                layout_header.setVisibility(View.VISIBLE);
                progress_text.setText(getString(R.string.connecting));
                layout_menu.setVisibility(View.INVISIBLE);
                layout_mesure.setVisibility(View.INVISIBLE);
                layout_sync.setVisibility(View.INVISIBLE);
                state=0;
            }
        });

        countdown.start();

        ArrayList<DataPoint> NotSend = databaseHelper.getElencoMisurazioniBPM_NotSend();
        Log.i("Sync NotSend - Size", String.valueOf(NotSend.size()));
        ArrayList<DataPoint> Send = databaseHelper.getElencoMisurazioniBPM_Send();
        Log.i("Sync Send - Size", String.valueOf(Send.size()));

        //Sincronizzazione Rilevazioni
        layout_sync.setVisibility(View.GONE);
        sync_lista_NotSend = databaseHelper.getElencoMisurazioniBPM_NotSend();
        if(sync_lista_NotSend.size()>0){
           runTaskSync();
        }else{
            layout_sync.setVisibility(View.GONE);
        }

        //Caricamento Voce
        t1=new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    t1.setLanguage(Locale.ITALIAN);
                }
            }
        });
    }

    protected void onStart(){
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSmoothBluetooth.stop();
        countdown.cancel();
    }


    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onStop() {
        super.onStop();
        // Disconnect from the remote device and close the serial port
        mSmoothBluetooth.stop();
    }

    @Override
    public void onBackPressed()
    {
        Thread.currentThread().interrupt();
        countdown.cancel();
        state=-1;
        databaseHelper.close();
        finish();
        super.onBackPressed();  // optional depending on your needs
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_ENABLE_BLUETOOTH) {
            if(resultCode == RESULT_OK) {
                mSmoothBluetooth.tryConnection();
            }
        }
    }


    // Implementazione dei listener
    private final BluetoothSPP.Listener mListener = new BluetoothSPP.Listener() {

        @Override
        public void onBluetoothNotSupported() {
            //Toast.makeText(Ossimetro.this, "Bluetooth not found", Toast.LENGTH_SHORT).show();
            alert = new AlertDialog.Builder(Sfigmomanometro.this);
            alert.setMessage(getString(R.string.nobluetooth));
            alert.setPositiveButton(getString(R.string.exit), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
            alert.setCancelable(false);
            alert.show();

        }

        @Override
        public void onBluetoothNotEnabled() {
            Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBluetooth, REQUEST_ENABLE_BLUETOOTH);
        }

        @Override
        public void onConnecting(Device device) {
            layout_header.setVisibility(View.VISIBLE);
            progress_text.setText(getString(R.string.connecting));
        }

        @Override
        public void onConnected(Device device) {
            countdown.cancel();
            layout_header.setVisibility(View.INVISIBLE);
            mSmoothBluetooth.send(TD3128.createTx25Cmd(0));
        }

        @Override
        public void onDisconnected() {

            if (state == 0) {
                layout_mesure.setVisibility(View.INVISIBLE);
                //Pulisco misurazioni
                misurazioni.clear();
                //inizio thread countdown
                countdown.start();
            }

        }

        @Override
        public void onConnectionFailed(Device device) {
            //Toast.makeText(Ossimetro.this, "Failed to connect", Toast.LENGTH_SHORT).show();

            if (state == 0) {
                //Pulisco misurazioni
                misurazioni.clear();
                //inizio thread countdown
                countdown.start();
            } else {
                countdown.cancel();
            }
        }

        @Override
        public void onDiscoveryStarted() {
            //Toast.makeText(Ossimetro.this, "Searching", Toast.LENGTH_SHORT).show();
            layout_header.setVisibility(View.VISIBLE);
            progress_text.setText(getString(R.string.discovery_started));
        }

        @Override
        public void onDiscoveryFinished() {

        }

        @Override
        public void onNoDevicesFound() {
            //Toast.makeText(Ossimetro.this, "No device found", Toast.LENGTH_SHORT).show();
            alert = new AlertDialog.Builder(Sfigmomanometro.this);
            alert.setTitle(getString(R.string.discovery_title));
            alert.setMessage(getString(R.string.discovery_notfound));
            alert.setPositiveButton(getString(R.string.riprova), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    mSmoothBluetooth.doDiscovery();
                }
            });
            alert.setNeutralButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    alert.show().cancel();
                    progress_text.setText("");
                }
            });
            alert.setCancelable(false);
            alert.show();
        }

        @Override
        public void onDevicesFound(final List<Device> deviceList, final BluetoothSPP.ConnectionCallback connectionCallback) {
            final MaterialDialog dialog = new MaterialDialog.Builder(Sfigmomanometro.this)
                    .title(getString(R.string.devices))
                    .adapter(new DevicesAdapter(Sfigmomanometro.this, deviceList), null)
                    .build();

            ListView listView = dialog.getListView();
            if (listView != null) {
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        dispositivi_selezionato = new Dispositivi("Sfigmomanometro", deviceList.get(position).getAddress());
                        databaseHelper.createDevice(dispositivi_selezionato);
                        connectionCallback.connectTo(deviceList.get(position));
                        dialog.dismiss();
                    }

                });
            }

            dialog.show();
        }

        @Override
        public void onDataReceived(int[] data) {
            if (state != 2) {
                layout_header.setVisibility(View.INVISIBLE);
                risposta = new int[32];
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                data_text.setText(sdf.format(new Date().getTime()));
                misurazioni = new ArrayList<>();

                int[] rx25Cmd = new int[8];
                int[] rx26Cmd = new int[8];

                for (int i = 0; i < data.length; i++) {
                    risposta[j++] = data[i];
                }

                if (j == 16) {
                    mSmoothBluetooth.send(TD3128.createTx26Cmd(0));
                }

                if (j == 24) {
                    rx25Cmd[0] = risposta[8];
                    rx25Cmd[1] = risposta[9];
                    rx25Cmd[2] = risposta[10];
                    rx25Cmd[3] = risposta[11];
                    rx25Cmd[4] = risposta[12];
                    rx25Cmd[5] = risposta[13];
                    rx25Cmd[6] = risposta[14];
                    rx25Cmd[7] = risposta[15];
                    rx26Cmd[0] = risposta[16];
                    rx26Cmd[1] = risposta[17];
                    rx26Cmd[2] = risposta[18];
                    rx26Cmd[3] = risposta[19];
                    rx26Cmd[4] = risposta[20];
                    rx26Cmd[5] = risposta[21];
                    rx26Cmd[6] = risposta[22];
                    rx26Cmd[7] = risposta[23];
                    final BloodPressureRecord result = TD3128.getBloodPressureRec(1, rx25Cmd, rx26Cmd);

                    //Creazione DataPoint
                    DataPoint Systolic_TAIDOC = new DataPoint(new Date(), "Sistolica", "INT", (double)result.getmSystolicValue(), "mmhg", "false");
                    DataPoint Diastolic_TAIDOC = new DataPoint(new Date(), "Diastolica", "INT", (double)result.getmDiastolicValue(), "mmhg", "false");
                    DataPoint MAP_TAIDOC = new DataPoint(new Date(), "MAP_TAIDOC", "INT", (double)result.getmMapValue(), "mmhg", "false");
                    DataPoint Arrhythmia_TAIDOC;
                    DataPoint Pulse_TAIDOC = new DataPoint(new Date(), "Pulsazioni_Sfigmomanometro", "INT", (double)result.getmPulseValue(), "bpm", "false");

                    sys_value.setText(String.valueOf(result.getmSystolicValue()));
                    dia_value.setText(String.valueOf(result.getmDiastolicValue()));
                    pr_value.setText(String.valueOf(result.getmPulseValue()));

                    if(result.ismArrhy()==true){
                        Arrhythmia_TAIDOC = new DataPoint(new Date(), "Aritmia", "INT", (double)1, "0", "false");
                        layout_arrth.setVisibility(View.VISIBLE);
                    }else{
                        Arrhythmia_TAIDOC = new DataPoint(new Date(), "Aritmia", "INT", (double)0, "0", "false");
                        layout_arrth.setVisibility(View.GONE);
                    }

                    layout_header.setVisibility(View.INVISIBLE);
                    layout_mesure.setVisibility(View.VISIBLE);


                    misurazioni.add(Systolic_TAIDOC);
                    misurazioni.add(Diastolic_TAIDOC);
                    //misurazioni.add(MAP_TAIDOC);
                    misurazioni.add(Arrhythmia_TAIDOC);
                    misurazioni.add(Pulse_TAIDOC);

                    progress_mesure.setVisibility(View.VISIBLE);

                    new CountDownTimer(4000, 1000) {
                        public void onTick(long millisUntilFinished) {
                            progress_mesure.setProgress((int) (millisUntilFinished / 1000) % 60);
                        }

                        @Override
                        public void onFinish() {
                            if(misurazioni.size()==4) {
                                progress_mesure.setProgress(0);
                                state = 1;
                                mSmoothBluetooth.disconnect();
                                t1.speak("Pressione " + result.getmSystolicValue() + " su " + result.getmDiastolicValue(), TextToSpeech.QUEUE_FLUSH, null);
                                runTask();
                            }
                        }
                    }.start();


                }
            }
        }

        @Override
        public void onDeviceFound(BluetoothDevice device, Context context, BroadcastReceiver mReceiver) {

        }
    };


    /**
     * Esecuzione dei task e controlli
     */
    private void runTask(){
        if(dm.checkMobileInternetConn() || dw.checkMobileInternetConn()) {
            new InvioDati(misurazioni).execute();
        }else{
            alertDialogBuilder.setTitle("Esito");
            alertDialogBuilder.setMessage("Nessuna Connessione Internet");
            alertDialogBuilder.setCancelable(false);
            alertDialogBuilder.setPositiveButton("Riprova", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    runTask();
                }
            });
            alertDialogBuilder.setNeutralButton("Chiudi", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            android.app.AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
    }

    /**
     * Esecuzione dei task e controlli
     */
    private void runTaskSync(){
        Sincronizza sync = new Sincronizza(sync_lista_NotSend);
        sync.execute();
    }

    /**
     * Metodo di Invio Dati al Server
     */
    private class InvioDati extends AsyncTask<Void, Void, String> {
        ArrayList<DataPoint> misurazioni;

        public InvioDati(ArrayList<DataPoint> misurazioni) {
            this.misurazioni = misurazioni;
        }

        protected void onPreExecute() {
            layout_sync.setBackgroundColor(Color.parseColor("#FFCC00"));
            layout_sync.setVisibility(View.VISIBLE);
            sync_message.setText(getString(R.string.invio_misurazione));
        }

        protected String doInBackground(Void... params) {
            //Caricamento WS
            WebServiceTechTablHealth serviceEasyDom = new WebServiceTechTablHealth();
            SimpleDateFormat sdf_res = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            Rilevazione rilevazione = new Rilevazione(0, 0, 0, "4", "BPM", "0","0", dispositivi_selezionato.getMac() ,sdf_res.format(misurazioni.get(0).getData().getTime()), MAC, misurazioni);

            String message = "0"; //0 Errore 1 Solo Locale 2 WebLocale

            InsertMeasurementResponse IDRilevazione = serviceEasyDom.InsertMeasurement(rilevazione);
            if (IDRilevazione.getValue() != 0 && IDRilevazione.getValue()!=-1) {
                Log.i("IDRilevazione", String.valueOf(IDRilevazione));
                Log.i("WS", "Inizio Invio Dati Rilevazione");

                //Inserimento in DB Locale
                ArrayList<Long> listID = new ArrayList<>();
                Log.e("SIZE", String.valueOf(misurazioni.size()));
                for(DataPoint misura: misurazioni) {
                    long idBPM = databaseHelper.inserisciMisuraBPM(misura);
                    listID.add(idBPM);
                    Log.i("insert DB mBPM", String.valueOf(idBPM));
                    if(idBPM!=0){
                        misura.setId((int) idBPM);
                    }
                }

                for (DataPoint misura: misurazioni) {
                    misura.setIdAssisted(IDRilevazione.getValue());
                    InsertDataMeasurementResponse controllo = serviceEasyDom.InsertDataMeasurement(misura, IDRilevazione.getValue(), 4);
                    if (controllo != null) {
                        Log.i("Controllo WS", String.valueOf(controllo.isValue()));
                        if (controllo.isValue()) {
                            misura.setSend("true");
                            Log.i("UPDATE TRUE", String.valueOf(databaseHelper.updateSfigmomanometro(misura)));
                            message = controllo.getMessage();
                        } else {
                            misura.setSend("false");
                            Log.i("UPDATE FALSE", String.valueOf(databaseHelper.updateSfigmomanometro(misura)));
                            message = controllo.getMessage();
                        }
                    } else {
                        misura.setSend("false");
                        Log.i("UPDATE FALSE", String.valueOf(databaseHelper.updateSfigmomanometro(misura)));
                        message = controllo.getMessage();
                    }
                }
            }else{
                message = IDRilevazione.getMessage();
                if(IDRilevazione.getValue() != 0 && IDRilevazione.getValue() != -1){


                }else {
                    //Inserimento in DB Locale
                    ArrayList<Long> listID = new ArrayList<>();
                    for(DataPoint misura: misurazioni) {
                        long idBPM = databaseHelper.inserisciMisuraBPM(misura);
                        listID.add(idBPM);
                        Log.i("insert DB mBPM", String.valueOf(idBPM));
                        if(idBPM!=0){
                            misura.setId((int) idBPM);
                        }
                    }

                    for (DataPoint misura : misurazioni) {
                        misura.setIdAssisted(0);
                        misura.setSend("false");
                        Log.i("UPDATE FALSE", String.valueOf(databaseHelper.updateSfigmomanometro(misura)));
                    }
                }
            }

            Log.i("Status Invio ", message);

            return message;
        }
        protected void onPostExecute(String status) {
            Log.e("Status", String.valueOf(status));
            //0 Errore 1 Solo Locale 2 WebLocale
            new android.app.AlertDialog.Builder(thisContext)
                    .setTitle("Esito")
                    .setMessage(status)
                    .setNeutralButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            layout_sync.setVisibility(View.INVISIBLE);
                            progress_mesure.setVisibility(View.INVISIBLE);
                        }
                    })
                    .show();
            state = 2;

            layout_menu.setVisibility(View.VISIBLE);
            mSmoothBluetooth.disconnect();
        }
    }

    /**
     * Metodo di Sincronizzazione dei dati
     */
    private class Sincronizza extends AsyncTask<Void, Void, Boolean> {
        ArrayList<DataPoint> misurazioni;

        public Sincronizza(ArrayList<DataPoint> misurazioni) {
            this.misurazioni = misurazioni;
        }

        protected void onPreExecute() {
            layout_sync.setBackgroundColor(Color.parseColor("#FFCC00"));
            layout_sync.setVisibility(View.VISIBLE);
            progress_sync.setVisibility(View.VISIBLE);
        }

        protected Boolean doInBackground(Void... params) {
            //Caricamento WS
            WebServiceTechTablHealth serviceEasyDom = new WebServiceTechTablHealth();
            SimpleDateFormat sdf_res = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            int contatore = 0;
            for(DataPoint misura:misurazioni){
                if(misura.getIdAssisted()!=0){
                    InsertDataMeasurementResponse controllo = serviceEasyDom.InsertDataMeasurement(misura, misura.getIdAssisted(), 4);
                    if(controllo!=null) {
                        if (controllo.isValue()) {
                            contatore++;
                            misura.setSend("true");
                            Log.i("UPDATE TRUE", String.valueOf(databaseHelper.updateSfigmomanometro(misura)));
                        } else {
                            misura.setSend("false");
                            Log.i("UPDATE FALSE", String.valueOf(databaseHelper.updateSfigmomanometro(misura)));
                        }
                    }else{
                        misura.setSend("false");
                        Log.i("UPDATE FALSE", String.valueOf(databaseHelper.updateSfigmomanometro(misura)));
                    }
                }else{
                    Rilevazione rilevazione = new Rilevazione(0, 0, 0, "4", "BPM", "0","0", dispositivi_selezionato.getMac(), sdf_res.format(misura.getData().getTime()),MAC, misurazioni);
                    InsertMeasurementResponse IDRilevazione = serviceEasyDom.InsertMeasurement(rilevazione);
                    if (IDRilevazione.getValue() != 0 && IDRilevazione.getValue()!=-1) {
                        misura.setIdAssisted(IDRilevazione.getValue());
                        InsertDataMeasurementResponse controllo = serviceEasyDom.InsertDataMeasurement(misura, IDRilevazione.getValue(), 4);
                        if (controllo.isValue()) {
                            contatore++;
                            misura.setSend("true");
                            Log.i("UPDATE TRUE", String.valueOf(databaseHelper.updateSfigmomanometro(misura)));
                        } else {
                            misura.setSend("false");
                            Log.i("UPDATE FALSE", String.valueOf(databaseHelper.updateSfigmomanometro(misura)));
                        }
                    } else {
                        misura.setIdAssisted(0);
                        misura.setSend("false");
                        Log.i("UPDATE FALSE", String.valueOf(databaseHelper.updateSfigmomanometro(misura)));
                        return false;
                    }
                }
            }
            Log.i("Contatore Sync", String.valueOf(contatore));
            return contatore >= misurazioni.size();
        }
        protected void onPostExecute(Boolean success) {
            if(success) {
                layout_sync.setBackgroundColor(Color.parseColor("#078446"));
                sync_message.setText("Sincronizzazione Completata");
                progress_sync.setVisibility(View.INVISIBLE);
                new CountDownTimer(3000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }
                    @Override
                    public void onFinish() {
                        layout_sync.setVisibility(View.INVISIBLE);
                    }
                }.start();
            }else{
                sync_message.setText("Sincronizzazione Non Completata");
                progress_sync.setVisibility(View.INVISIBLE);
                new CountDownTimer(3000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }
                    @Override
                    public void onFinish() {
                        layout_sync.setVisibility(View.INVISIBLE);
                    }
                }.start();
            }
        }
    }
}
