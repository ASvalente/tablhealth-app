package magaldinnova.techTablHealth.activity.areaTelemonitoraggio;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;

import magaldinnova.techTablHealth.R;
import magaldinnova.techTablHealth.activity.adapter.Adapter_bilancia;
import magaldinnova.techTablHealth.activity.adapter.Adapter_glucometro;
import magaldinnova.techTablHealth.activity.adapter.Adapter_misurazioni_device;
import magaldinnova.techTablHealth.activity.adapter.Adapter_ossimetro;
import magaldinnova.techTablHealth.activity.adapter.Adapter_sfigmomanometro;
import magaldinnova.techTablHealth.activity.adapter.MyValueFormatter;
import magaldinnova.techTablHealth.database.dbHelper.DatabaseHelper;
import magaldinnova.techTablHealth.database.oggetti.DataPoint;
import magaldinnova.techTablHealth.database.oggetti.Dispositivi;

/**
 * Modulo Raccolta Misurazioni Locali
 * @author Magaldi Innova srl
 */
public class Area_Misurazioni extends AppCompatActivity {

    //Caricamento Elementi Grafici
    private GridView lista_dispositivi, lista_misurazioni;
    private LinearLayout layout_dettagli;
    private LineChart grafico_dettagli, grafico_dettagli1, grafico_dettagli2;
    private ProgressBar progressBar;
    private RelativeLayout layout_no_misurazioni;
    private TextView txt_dispositivo;
    private ArrayList<Dispositivi> listaDevice;
    private Context thisContext;
    private ArrayList<DataPoint> mOssimetro, mOssimetro_Pulse;
    private ArrayList<DataPoint> mSfigmomanometro_sys, mSfigmomanometro_dia;
    private ArrayList<DataPoint> mGlucometro;
    private ArrayList<DataPoint> mBilancia;
    private DatabaseHelper databaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_area_misurazioni);

        if(getSupportActionBar() != null){
            getSupportActionBar().setElevation(0);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Area Misurazioni");
        }

        //Caricamento Elementi Grafici
        lista_dispositivi = (GridView)findViewById(R.id.lista_dispositivi);
        lista_misurazioni = (GridView)findViewById(R.id.lista_misurazioni);
        grafico_dettagli = (LineChart)findViewById(R.id.grafico_dettagli);
        grafico_dettagli1 = (LineChart)findViewById(R.id.grafico_dettagli1);
        grafico_dettagli2 = (LineChart)findViewById(R.id.grafico_dettagli2);
        progressBar =(ProgressBar)findViewById(R.id.progressBar);
        layout_no_misurazioni = (RelativeLayout)findViewById(R.id.layout_no_misurazioni);
        layout_dettagli = (LinearLayout) findViewById(R.id.layout_dettagli);
        txt_dispositivo = (TextView)findViewById(R.id.txt_dispositivo);

        //Apertura DB
        databaseHelper = new DatabaseHelper(this);

        Intent intent= getIntent();
        Bundle b = intent.getExtras();

        if(b!=null)
        {
            //Caricamento Variabili
            listaDevice = (ArrayList<Dispositivi>) b.getSerializable("Devices");
        }

        thisContext = this;

        lista_dispositivi.setAdapter(new Adapter_misurazioni_device(listaDevice));
        lista_dispositivi.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Object o = lista_dispositivi.getItemAtPosition(position);
                Dispositivi selezionato = (Dispositivi) o;
                if(selezionato.getNome().equals("Ossimetro")){
                    GetmOssimetroAsync task = new GetmOssimetroAsync(selezionato);
                    task.execute();
                }
                if(selezionato.getNome().equals("Sfigmomanometro")){
                    GetmSfigmomanometroAsync task = new GetmSfigmomanometroAsync(selezionato);
                    task.execute();
                }
                if(selezionato.getNome().equals("Glucometro")){
                    GetmGlucometroAsync task = new GetmGlucometroAsync(selezionato);
                    task.execute();
                }
                if(selezionato.getNome().equals("Bilancia")){
                    GetmBilanciaAsync task = new GetmBilanciaAsync(selezionato);
                    task.execute();
                }
            }
        });

        txt_dispositivo.setText("Seleziona Dispositivo");

        layout_dettagli.setVisibility(View.INVISIBLE);
        layout_no_misurazioni.setVisibility(View.INVISIBLE);
        grafico_dettagli.setVisibility(View.INVISIBLE);
        grafico_dettagli1.setVisibility(View.INVISIBLE);
        grafico_dettagli2.setVisibility(View.INVISIBLE);
        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                if (getParentActivityIntent() == null) {
                    onBackPressed();
                } else {
                    NavUtils.navigateUpFromSameTask(this);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Lettura delle Misurazioni nel DB
     */
    public class GetmOssimetroAsync extends AsyncTask<Void, Void, Boolean> {
        private Dispositivi device;

        public GetmOssimetroAsync(Dispositivi device) {
            this.device = device;
        }

        protected void onPreExecute(){
            progressBar.setVisibility(View.VISIBLE);
            layout_dettagli.setVisibility(View.INVISIBLE);
            layout_no_misurazioni.setVisibility(View.INVISIBLE);

            //Configurazione Grafici
            grafico_dettagli.setVisibility(View.GONE);
            grafico_dettagli1.clear();
            grafico_dettagli1.setDrawGridBackground(false);
            grafico_dettagli1.setDrawBorders(false);
            grafico_dettagli1.setDescription("SPO2");
            grafico_dettagli1.setPinchZoom(false);
            grafico_dettagli1.setTouchEnabled(false);
            Legend l = grafico_dettagli1.getLegend();
            l.setEnabled(false);
            XAxis xAxis = grafico_dettagli1.getXAxis();
            xAxis.setEnabled(false);
            YAxis leftAxis = grafico_dettagli1.getAxisLeft();
            leftAxis.setDrawAxisLine(false);
            leftAxis.setDrawZeroLine(false);
            leftAxis.setDrawGridLines(false);
            leftAxis.setEnabled(false);
            grafico_dettagli1.getAxisRight().setEnabled(false);
            grafico_dettagli1.invalidate();

            grafico_dettagli2.clear();
            grafico_dettagli2.setDrawGridBackground(false);
            grafico_dettagli2.setDrawBorders(false);
            grafico_dettagli2.setDescription("PR");
            grafico_dettagli2.setPinchZoom(false);
            grafico_dettagli2.setTouchEnabled(false);
            Legend l2 = grafico_dettagli2.getLegend();
            l2.setEnabled(false);
            XAxis xAxis2 = grafico_dettagli2.getXAxis();
            xAxis2.setEnabled(false);
            YAxis leftAxis2 = grafico_dettagli2.getAxisLeft();
            leftAxis2.setDrawAxisLine(false);
            leftAxis2.setDrawZeroLine(false);
            leftAxis2.setDrawGridLines(false);
            leftAxis2.setEnabled(false);
            grafico_dettagli2.getAxisRight().setEnabled(false);
            grafico_dettagli2.invalidate();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            ArrayList<DataPoint> listaSPO = databaseHelper.getElencoMisurazioniSPO();
            ArrayList<DataPoint> listaSPO_pulse = databaseHelper.getElencoMisurazioniSPO_Pulse();
            if(!listaSPO.isEmpty() && !listaSPO_pulse.isEmpty()){
                mOssimetro = listaSPO;
                mOssimetro_Pulse = listaSPO_pulse;
                return true;
            }else{
                mOssimetro = null;
                mOssimetro_Pulse = null;
                return false;
            }
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            progressBar.setVisibility(View.GONE);
            if (success) {
                ArrayList<Entry> eOssimetro = new ArrayList<>();
                ArrayList<String> lOssimetro = new ArrayList<>();
                //Riempio i campi
                Collections.reverse(mOssimetro);
                if(mOssimetro.size()>10){
                    for(int i=mOssimetro.size()-10;i<mOssimetro.size();i++){
                        DataPoint item = mOssimetro.get(i);
                        BigDecimal dec = new BigDecimal(item.getValue());
                        eOssimetro.add(new Entry(i, dec.floatValue()));
                        lOssimetro.add(String.valueOf(i));
                    }
                }else{
                    for (int i = 0; i < mOssimetro.size(); i++) {
                        DataPoint item = mOssimetro.get(i);
                        BigDecimal dec = new BigDecimal(item.getValue());
                        eOssimetro.add(new Entry(i, dec.floatValue()));
                        lOssimetro.add(String.valueOf(i));
                    }
                }
                LineDataSet dataset = new LineDataSet(eOssimetro, "SPO2");
                dataset.setDrawFilled(true);
                dataset.setColor(Color.parseColor("#BE39C0"));
                dataset.setFillColor(Color.parseColor("#BE39C0"));
                dataset.setCircleColor(Color.parseColor("#BE39C0"));
                dataset.setValueTextSize(10f);
                dataset.setValueFormatter(new MyValueFormatter(device.getNome()));
                dataset.setValueTypeface(Typeface.DEFAULT_BOLD);
                dataset.setHighlightEnabled(false);
                LineData data = new LineData(dataset);
                grafico_dettagli1.setData(data);

                ArrayList<Entry> eOssimetro_Pulse = new ArrayList<>();
                ArrayList<String> lOssimetro_Pulse = new ArrayList<>();

                //Riempio i campi
                Collections.reverse(mOssimetro_Pulse);
                if(mOssimetro_Pulse.size()>10) {
                    for (int i = mOssimetro_Pulse.size() - 10; i < mOssimetro_Pulse.size(); i++) {
                        DataPoint item = mOssimetro_Pulse.get(i);
                        BigDecimal dec = new BigDecimal(item.getValue()).setScale(0, BigDecimal.ROUND_HALF_UP);
                        eOssimetro_Pulse.add(new Entry(i, dec.floatValue()));
                        lOssimetro_Pulse.add(String.valueOf(i));
                    }
                }else{
                    for (int i = 0; i < mOssimetro_Pulse.size(); i++) {
                        DataPoint item = mOssimetro_Pulse.get(i);
                        BigDecimal dec = new BigDecimal(item.getValue()).setScale(0, BigDecimal.ROUND_HALF_UP);
                        eOssimetro_Pulse.add(new Entry(i, dec.floatValue()));
                        lOssimetro_Pulse.add(String.valueOf(i));
                    }
                }
                LineDataSet dataset2 = new LineDataSet(eOssimetro_Pulse, "PR");
                dataset2.setDrawFilled(true);
                dataset2.setColor(Color.parseColor("#137613"));
                dataset2.setFillColor(Color.parseColor("#137613"));
                dataset2.setCircleColor(Color.parseColor("#137613"));
                dataset2.setValueTextSize(10f);
                dataset2.setValueFormatter(new MyValueFormatter(device.getNome()));
                dataset2.setValueTypeface(Typeface.DEFAULT_BOLD);
                dataset2.setHighlightEnabled(false);
                LineData data2 = new LineData(dataset2);
                grafico_dettagli2.setData(data2);

                txt_dispositivo.setText(device.getNome());
                grafico_dettagli1.setVisibility(View.VISIBLE);
                grafico_dettagli2.setVisibility(View.VISIBLE);
                grafico_dettagli.setVisibility(View.GONE);
                Collections.reverse(mOssimetro);
                Collections.reverse(mOssimetro_Pulse);
                Adapter_ossimetro adapter = new Adapter_ossimetro(mOssimetro, mOssimetro_Pulse);
                lista_misurazioni.setAdapter(adapter);

                layout_dettagli.setVisibility(View.VISIBLE);
            } else {
                txt_dispositivo.setText(device.getNome());
                progressBar.setVisibility(View.GONE);
                layout_no_misurazioni.setVisibility(View.VISIBLE);
                layout_dettagli.setVisibility(View.INVISIBLE);
            }
        }
    }

    /**
     * Lettura delle Misurazioni nel DB
     */
    public class GetmSfigmomanometroAsync extends AsyncTask<Void, Void, Boolean> {
        private Dispositivi device;

        public GetmSfigmomanometroAsync(Dispositivi device) {
            this.device = device;
        }

        protected void onPreExecute(){
            progressBar.setVisibility(View.VISIBLE);
            layout_dettagli.setVisibility(View.INVISIBLE);
            layout_no_misurazioni.setVisibility(View.INVISIBLE);

            //Configurazione Grafici
            grafico_dettagli.setVisibility(View.GONE);
            grafico_dettagli1.clear();
            grafico_dettagli1.setDrawGridBackground(false);
            grafico_dettagli1.setDrawBorders(false);
            grafico_dettagli1.setDescription("SYS");
            grafico_dettagli1.setPinchZoom(false);
            grafico_dettagli1.setTouchEnabled(false);
            Legend l = grafico_dettagli1.getLegend();
            l.setEnabled(false);
            XAxis xAxis = grafico_dettagli1.getXAxis();
            xAxis.setEnabled(false);
            YAxis leftAxis = grafico_dettagli1.getAxisLeft();
            leftAxis.setDrawAxisLine(false);
            leftAxis.setDrawZeroLine(false);
            leftAxis.setDrawGridLines(false);
            leftAxis.setEnabled(false);
            grafico_dettagli1.getAxisRight().setEnabled(false);
            grafico_dettagli1.invalidate();

            grafico_dettagli2.clear();
            grafico_dettagli2.setDrawGridBackground(false);
            grafico_dettagli2.setDrawBorders(false);
            grafico_dettagli2.setDescription("DIA");
            grafico_dettagli2.setPinchZoom(false);
            grafico_dettagli2.setTouchEnabled(false);
            Legend l2 = grafico_dettagli2.getLegend();
            l2.setEnabled(false);
            XAxis xAxis2 = grafico_dettagli2.getXAxis();
            xAxis2.setEnabled(false);
            YAxis leftAxis2 = grafico_dettagli2.getAxisLeft();
            leftAxis2.setDrawAxisLine(false);
            leftAxis2.setDrawZeroLine(false);
            leftAxis2.setDrawGridLines(false);
            leftAxis2.setEnabled(false);
            grafico_dettagli2.getAxisRight().setEnabled(false);
            grafico_dettagli2.invalidate();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            ArrayList<DataPoint> listaSYS = databaseHelper.getElencoMisurazioniBPM_Systolic();
            ArrayList<DataPoint> listaDIA = databaseHelper.getElencoMisurazioniBPM_Diastolic();
            if(!listaSYS.isEmpty() && !listaDIA.isEmpty()){
                mSfigmomanometro_sys = listaSYS;
                mSfigmomanometro_dia = listaDIA;
                return true;
            }else{
                mSfigmomanometro_sys = null;
                mSfigmomanometro_dia = null;
                return false;
            }
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            progressBar.setVisibility(View.GONE);
            if (success) {
                ArrayList<Entry> eSfigmomanometro_sys = new ArrayList<>();
                ArrayList<String> lSfigmomanometro_sys = new ArrayList<>();
                //Riempio i campi
                Collections.reverse(mSfigmomanometro_sys);
                if(mSfigmomanometro_sys.size()>10){
                    for(int i=mSfigmomanometro_sys.size()-10;i<mSfigmomanometro_sys.size();i++){
                        DataPoint item = mSfigmomanometro_sys.get(i);
                        BigDecimal dec = new BigDecimal(item.getValue()).setScale(0, BigDecimal.ROUND_HALF_UP);
                        eSfigmomanometro_sys.add(new Entry(i, dec.floatValue()));
                        lSfigmomanometro_sys.add(String.valueOf(i));
                    }
                }else{
                    for (int i = 0; i < mSfigmomanometro_sys.size(); i++) {
                        DataPoint item = mSfigmomanometro_sys.get(i);
                        BigDecimal dec = new BigDecimal(item.getValue()).setScale(0, BigDecimal.ROUND_HALF_UP);
                        eSfigmomanometro_sys.add(new Entry(i, dec.floatValue()));
                        lSfigmomanometro_sys.add(String.valueOf(i));
                    }
                }
                LineDataSet dataset = new LineDataSet(eSfigmomanometro_sys, "SYS");
                dataset.setDrawFilled(true);
                dataset.setColor(Color.parseColor("#00b21b"));
                dataset.setFillColor(Color.parseColor("#00b21b"));
                dataset.setCircleColor(Color.parseColor("#00b21b"));
                dataset.setValueTextSize(10f);
                dataset.setValueFormatter(new MyValueFormatter(device.getNome()));
                dataset.setValueTypeface(Typeface.DEFAULT_BOLD);
                dataset.setHighlightEnabled(false);
                LineData data = new LineData(dataset);
                grafico_dettagli1.setData(data);

                ArrayList<Entry> eSfigmomanometro_dia = new ArrayList<>();
                ArrayList<String> lSfigmomanometro_dia = new ArrayList<>();

                //Riempio i campi
                Collections.reverse(mSfigmomanometro_dia);
                if(mSfigmomanometro_dia.size()>10) {
                    for (int i = mSfigmomanometro_dia.size() - 10; i < mSfigmomanometro_dia.size(); i++) {
                        DataPoint item = mSfigmomanometro_dia.get(i);
                        BigDecimal dec = new BigDecimal(item.getValue()).setScale(0, BigDecimal.ROUND_HALF_UP);
                        eSfigmomanometro_dia.add(new Entry(i, dec.floatValue()));
                        lSfigmomanometro_dia.add(String.valueOf(i));
                    }
                }else{
                    for (int i = 0; i < mSfigmomanometro_dia.size(); i++) {
                        DataPoint item = mSfigmomanometro_dia.get(i);
                        BigDecimal dec = new BigDecimal(item.getValue()).setScale(0, BigDecimal.ROUND_HALF_UP);
                        eSfigmomanometro_dia.add(new Entry(i, dec.floatValue()));
                        lSfigmomanometro_dia.add(String.valueOf(i));
                    }
                }
                LineDataSet dataset2 = new LineDataSet(eSfigmomanometro_dia, "DIA");
                dataset2.setDrawFilled(true);
                dataset2.setColor(Color.parseColor("#008651"));
                dataset2.setFillColor(Color.parseColor("#008651"));
                dataset2.setCircleColor(Color.parseColor("#008651"));
                dataset2.setValueTextSize(10f);
                dataset2.setValueFormatter(new MyValueFormatter(device.getNome()));
                dataset2.setValueTypeface(Typeface.DEFAULT_BOLD);
                dataset2.setHighlightEnabled(false);
                LineData data2 = new LineData(dataset2);
                grafico_dettagli2.setData(data2);
                txt_dispositivo.setText(device.getNome());
                grafico_dettagli1.setVisibility(View.VISIBLE);
                grafico_dettagli2.setVisibility(View.VISIBLE);
                grafico_dettagli.setVisibility(View.GONE);
                Collections.reverse(mSfigmomanometro_sys);
                Collections.reverse(mSfigmomanometro_dia);
                Adapter_sfigmomanometro adapter = new Adapter_sfigmomanometro(mSfigmomanometro_sys, mSfigmomanometro_dia);
                lista_misurazioni.setAdapter(adapter);
                layout_dettagli.setVisibility(View.VISIBLE);
            } else {
                txt_dispositivo.setText(device.getNome());
                progressBar.setVisibility(View.GONE);
                layout_no_misurazioni.setVisibility(View.VISIBLE);
                layout_dettagli.setVisibility(View.INVISIBLE);
            }
        }
    }

    /**
     * Lettura delle Misurazioni nel DB
     */
    public class GetmGlucometroAsync extends AsyncTask<Void, Void, Boolean> {
        private Dispositivi device;

        public GetmGlucometroAsync(Dispositivi device) {
            this.device = device;
        }

        protected void onPreExecute(){
            progressBar.setVisibility(View.VISIBLE);
            layout_dettagli.setVisibility(View.INVISIBLE);
            layout_no_misurazioni.setVisibility(View.INVISIBLE);

            //Configurazione Grafici
            grafico_dettagli1.setVisibility(View.GONE);
            grafico_dettagli2.setVisibility(View.GONE);
            grafico_dettagli.clear();
            grafico_dettagli.setDrawGridBackground(false);
            grafico_dettagli.setDrawBorders(false);
            grafico_dettagli.setDescription("GLI");
            grafico_dettagli.setPinchZoom(false);
            grafico_dettagli.setTouchEnabled(false);
            Legend l = grafico_dettagli.getLegend();
            l.setEnabled(false);
            XAxis xAxis = grafico_dettagli.getXAxis();
            xAxis.setEnabled(false);
            YAxis leftAxis = grafico_dettagli.getAxisLeft();
            leftAxis.setDrawAxisLine(false);
            leftAxis.setDrawZeroLine(false);
            leftAxis.setDrawGridLines(false);
            leftAxis.setEnabled(false);
            grafico_dettagli.getAxisRight().setEnabled(false);
            grafico_dettagli.invalidate();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            ArrayList<DataPoint> listaGlucose = databaseHelper.getMisurazioniGLM_Glucose();
            if(!listaGlucose.isEmpty()){
                mGlucometro = listaGlucose;
                return true;
            }else{
                mGlucometro = null;
                return false;
            }
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            progressBar.setVisibility(View.GONE);
            if (success) {
                ArrayList<Entry> eGlucometro = new ArrayList<>();
                ArrayList<String> lGlucometro = new ArrayList<>();
                //Riempio i campi
                Collections.reverse(mGlucometro);
                if(mGlucometro.size()>10){
                    for(int i=mGlucometro.size()-10;i<mGlucometro.size();i++){
                        DataPoint item = mGlucometro.get(i);
                        BigDecimal dec = new BigDecimal(item.getValue()).setScale(0, BigDecimal.ROUND_HALF_UP);
                        eGlucometro.add(new Entry(i, dec.floatValue()));
                        lGlucometro.add(String.valueOf(i));
                    }
                }else{
                    for (int i = 0; i < mGlucometro.size(); i++) {
                        DataPoint item = mGlucometro.get(i);
                        BigDecimal dec = new BigDecimal(item.getValue()).setScale(0, BigDecimal.ROUND_HALF_UP);
                        eGlucometro.add(new Entry(i, dec.floatValue()));
                        lGlucometro.add(String.valueOf(i));
                    }
                }
                LineDataSet dataset = new LineDataSet(eGlucometro, "GLI");
                dataset.setDrawFilled(true);
                dataset.setColor(Color.parseColor("#ff8800"));
                dataset.setFillColor(Color.parseColor("#ff8800"));
                dataset.setCircleColor(Color.parseColor("#ff8800"));
                dataset.setValueTextSize(10f);
                dataset.setValueFormatter(new MyValueFormatter(device.getNome()));
                dataset.setValueTypeface(Typeface.DEFAULT_BOLD);
                dataset.setHighlightEnabled(false);
                LineData data = new LineData(dataset);
                grafico_dettagli.setData(data);
                grafico_dettagli.setVisibility(View.VISIBLE);
                txt_dispositivo.setText(device.getNome());
                grafico_dettagli1.setVisibility(View.GONE);
                grafico_dettagli2.setVisibility(View.GONE);
                Collections.reverse(mGlucometro);
                Adapter_glucometro adapter = new Adapter_glucometro(mGlucometro);
                lista_misurazioni.setAdapter(adapter);
                layout_dettagli.setVisibility(View.VISIBLE);
            } else {
                txt_dispositivo.setText(device.getNome());
                progressBar.setVisibility(View.GONE);
                layout_no_misurazioni.setVisibility(View.VISIBLE);
                layout_dettagli.setVisibility(View.INVISIBLE);
            }
        }
    }

    /**
     * Lettura delle Misurazioni nel DB
     */
    public class GetmBilanciaAsync extends AsyncTask<Void, Void, Boolean> {
        private Dispositivi device;

        public GetmBilanciaAsync(Dispositivi device) {
            this.device = device;
        }

        protected void onPreExecute(){
            progressBar.setVisibility(View.VISIBLE);
            layout_dettagli.setVisibility(View.INVISIBLE);
            layout_no_misurazioni.setVisibility(View.INVISIBLE);

            //Configurazione Grafici
            grafico_dettagli1.setVisibility(View.GONE);
            grafico_dettagli2.setVisibility(View.GONE);
            grafico_dettagli.clear();
            grafico_dettagli.setDrawGridBackground(false);
            grafico_dettagli.setDrawBorders(false);
            grafico_dettagli.setDescription("PESO");
            grafico_dettagli.setPinchZoom(false);
            grafico_dettagli.setTouchEnabled(false);
            Legend l = grafico_dettagli.getLegend();
            l.setEnabled(false);
            XAxis xAxis = grafico_dettagli.getXAxis();
            xAxis.setEnabled(false);
            YAxis leftAxis = grafico_dettagli.getAxisLeft();
            leftAxis.setDrawAxisLine(false);
            leftAxis.setDrawZeroLine(false);
            leftAxis.setDrawGridLines(false);
            leftAxis.setEnabled(false);
            grafico_dettagli.getAxisRight().setEnabled(false);
            grafico_dettagli.invalidate();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            ArrayList<DataPoint> listaW = databaseHelper.getMisurazioniWeight();
            if(!listaW.isEmpty()){
                mBilancia = listaW;
                return true;
            }else{
                mBilancia = null;
                return false;
            }
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            progressBar.setVisibility(View.GONE);
            if (success) {
                ArrayList<Entry> eBilancia = new ArrayList<>();
                ArrayList<String> lBilancia = new ArrayList<>();
                //Riempio i campi
                Collections.reverse(mBilancia);
                if(mBilancia.size()>10){
                    for(int i=mBilancia.size()-10;i<mBilancia.size();i++){
                        DataPoint item = mBilancia.get(i);
                        BigDecimal dec = new BigDecimal(item.getValue()).setScale(1, BigDecimal.ROUND_HALF_UP);
                        eBilancia.add(new Entry(i, dec.floatValue()));
                        lBilancia.add(String.valueOf(i));
                    }
                }else{
                    for (int i = 0; i < mBilancia.size(); i++) {
                        DataPoint item = mBilancia.get(i);
                        BigDecimal dec = new BigDecimal(item.getValue()).setScale(1, BigDecimal.ROUND_HALF_UP);
                        eBilancia.add(new Entry(i, dec.floatValue()));
                        lBilancia.add(String.valueOf(i));
                    }
                }
                LineDataSet dataset = new LineDataSet(eBilancia, "SPO2");
                dataset.setDrawFilled(true);
                dataset.setColor(Color.parseColor("#12416f"));
                dataset.setFillColor(Color.parseColor("#12416f"));
                dataset.setCircleColor(Color.parseColor("#12416f"));
                dataset.setValueTextSize(10f);
                dataset.setValueFormatter(new MyValueFormatter(device.getNome()));
                dataset.setValueTypeface(Typeface.DEFAULT_BOLD);
                dataset.setHighlightEnabled(false);
                LineData data = new LineData(dataset);
                grafico_dettagli.setData(data);
                grafico_dettagli.setVisibility(View.VISIBLE);
                txt_dispositivo.setText(device.getNome());
                grafico_dettagli1.setVisibility(View.GONE);
                grafico_dettagli2.setVisibility(View.GONE);
                Collections.reverse(mBilancia);
                Adapter_bilancia adapter = new Adapter_bilancia(mBilancia);
                lista_misurazioni.setAdapter(adapter);
                layout_dettagli.setVisibility(View.VISIBLE);
            } else {
                txt_dispositivo.setText(device.getNome());
                progressBar.setVisibility(View.GONE);
                layout_no_misurazioni.setVisibility(View.VISIBLE);
                layout_dettagli.setVisibility(View.INVISIBLE);
            }
        }
    }

}
