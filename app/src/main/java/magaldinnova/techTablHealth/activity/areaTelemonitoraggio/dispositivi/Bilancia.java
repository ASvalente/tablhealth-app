package magaldinnova.techTablHealth.activity.areaTelemonitoraggio.dispositivi;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import magaldinnova.techTablHealth.R;
import magaldinnova.techTablHealth.activity.detector.MobileInternetConnectionDetector;
import magaldinnova.techTablHealth.activity.detector.WIFIInternetConnectionDetector;
import magaldinnova.techTablHealth.database.dbHelper.DatabaseHelper;
import magaldinnova.techTablHealth.database.oggetti.DataPoint;
import magaldinnova.techTablHealth.database.oggetti.Dispositivi;
import magaldinnova.techTablHealth.library.BluetoothSPP;
import magaldinnova.techTablHealth.library.Device;
import magaldinnova.techTablHealth.moduli.CF351BT;
import magaldinnova.techTablHealth.database.record.WeightRecord;
import magaldinnova.techTablHealth.webService.oggetti.InsertDataMeasurementResponse;
import magaldinnova.techTablHealth.webService.oggetti.InsertMeasurementResponse;
import magaldinnova.techTablHealth.webService.oggetti.Rilevazione;
import magaldinnova.techTablHealth.webService.WebServiceTechTablHealth;

/**
 * Modulo Bilancia
 * @author Magaldi Innova srl
 */
public class Bilancia extends AppCompatActivity {

    //Caricamento Variabili
    private static final int REQUEST_ENABLE_BLUETOOTH = 1;
    private Context thisContext;
    private MobileInternetConnectionDetector dm;
    private WIFIInternetConnectionDetector dw;
    private android.app.AlertDialog.Builder alertDialogBuilder;

    //Caricamento Bluetooth
    private BluetoothSPP mSmoothBluetooth;
    private int i = 0;
    private int state = 0;
    private Dispositivi dispositivi_selezionato, Bilancia;
    private String MAC;
    private RoundCornerProgressBar progress_mesure;

    //Caricamento oggettoDispositivo
    private CF351BT cf351bt;
    private ArrayList<DataPoint> misurazioni;

    //Parametri risposta
    private int[] risposta = new int[32];
    private int j=0;

    //Autoconnessione
    private CountDownTimer countdown;

    //Caricamento Elementri grafici
    private RelativeLayout layout_header;
    private LinearLayout layout_menu, layout_sync, layout_mesure;
    private TextView sync_message;
    private TextView progress_text;
    private TextView data_text;
    private TextView peso_value;
    private ProgressBar progress_sync;
    private LinearLayout linearLayout_data;

    //Caricamento DB
    private DatabaseHelper databaseHelper;
    private ArrayList<DataPoint> sync_lista_NotSend;

    //Caricamento VOCE
    private TextToSpeech t1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bilancia);

        getSupportActionBar().setElevation(0);

        // --------- CARICAMENTO ELEMENTI GRAFICI ---------- //
        //MISURAZIONI
        layout_mesure = (LinearLayout) findViewById(R.id.layout_mesure);
        progress_text = (TextView) findViewById(R.id.progress_text);
        data_text = (TextView) findViewById(R.id.data_text);
        peso_value = (TextView) findViewById(R.id.peso_value);
        linearLayout_data = (LinearLayout) findViewById(R.id.linearLayout_peso);
        progress_mesure = (RoundCornerProgressBar) findViewById(R.id.progress_mesure);
        progress_mesure.setMax(5);
        progress_mesure.setProgressColor(getResources().getColor(R.color.status));
        progress_mesure.setProgressBackgroundColor(Color.parseColor("#e4ebff"));
        //STATUS CONNESSIONE
        layout_header = (RelativeLayout) findViewById(R.id.layout_header);
        progress_text = (TextView) findViewById(R.id.progress_text);
        // INVIO DATI
        layout_menu = (LinearLayout) findViewById(R.id.layout_menu);
        LinearLayout btn_fine = (LinearLayout) findViewById(R.id.btn_fine);
        LinearLayout btn_nuova_misurazione = (LinearLayout) findViewById(R.id.btn_nuova_misurazione);
        //SINCRONIZZAZIONE
        layout_sync = (LinearLayout) findViewById(R.id.layout_sync);
        sync_message = (TextView) findViewById(R.id.sync_message);
        progress_sync = (ProgressBar) findViewById(R.id.progress_sync);


        // Create a new instance of Bluetooth
        mSmoothBluetooth = new BluetoothSPP(this);
        mSmoothBluetooth.setListener(mListener);
        cf351bt = new CF351BT();

        misurazioni = new ArrayList<>();

        thisContext = this;

        //Controllo connessione
        dm=new MobileInternetConnectionDetector(thisContext);
        dw=new WIFIInternetConnectionDetector(thisContext);
        alertDialogBuilder = new android.app.AlertDialog.Builder(thisContext);

        //SetTitle
        getSupportActionBar().setTitle("Bilancia");

        //Caricamento MAC
        Intent intent = getIntent();
        Bundle b = intent.getExtras();

        if (b != null) {
            MAC = b.getString("MAC");
            Bilancia = (Dispositivi) b.getSerializable("Glucometro");
        }

        //Aperture DB
        databaseHelper = new DatabaseHelper(this);
        dispositivi_selezionato = databaseHelper.getDispositivoByName("Bilancia");

        if (Bilancia != null) {
            if (dispositivi_selezionato.getMac().equals(Bilancia.getMac())) {

            } else {
                dispositivi_selezionato = Bilancia;
                databaseHelper.updateDevice(Bilancia);
            }
        }

        //Autoconnesione
        countdown = new CountDownTimer(1000, 1000) {
            public void onTick(long millisUntilFinished) {
                Log.i("CountDown Thread: " + Thread.currentThread().getId(), getString(R.string.tryconnect) + (int) (millisUntilFinished / 1000) % 60);
                progress_text.setVisibility(View.VISIBLE);
                progress_text.setText(getString(R.string.connecting));
            }

            public void onFinish() {
                if (dispositivi_selezionato != null) {
                    progress_text.setVisibility(View.VISIBLE);
                    progress_text.setText(getString(R.string.connecting));
                    mSmoothBluetooth.connect(dispositivi_selezionato.getMac());
                } else {
                    Log.i("CountDown Thread: " + Thread.currentThread().getId(), getString(R.string.pair));
                    progress_text.setVisibility(View.VISIBLE);
                    progress_text.setText(getString(R.string.pair));
                }
                Thread.currentThread().interrupt();
            }
        };

        btn_fine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                databaseHelper.close();
                finish();
            }
        });

        btn_nuova_misurazione.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSmoothBluetooth.disconnect();
                if (misurazioni.size() != 0) {
                    misurazioni.clear();
                    countdown.start();
                }
                layout_header.setVisibility(View.VISIBLE);
                progress_text.setText(getString(R.string.connecting));
                layout_menu.setVisibility(View.INVISIBLE);
                layout_mesure.setVisibility(View.INVISIBLE);
                layout_sync.setVisibility(View.INVISIBLE);
                linearLayout_data.setVisibility(View.INVISIBLE);
                state = 0;
            }
        });

        countdown.start();

        //Sincronizzazione Rilevazioni
        layout_sync.setVisibility(View.GONE);
        sync_lista_NotSend = databaseHelper.getElencoMisurazioniHIS_NotSend();
        if (sync_lista_NotSend.size() > 0) {
            runTaskSync();
        } else {
            layout_sync.setVisibility(View.GONE);
        }

        //Caricamento Voce
        t1 = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status != TextToSpeech.ERROR) {
                    t1.setLanguage(Locale.ITALIAN);
                }
            }
        });
    }

    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSmoothBluetooth.stop();
        countdown.cancel();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onStop() {
        super.onStop();
        // Disconnect from the remote device and close the serial port
        mSmoothBluetooth.stop();
    }

    @Override
    public void onBackPressed() {
        Thread.currentThread().interrupt();
        countdown.cancel();
        state = -1;
        databaseHelper.close();
        finish();
        super.onBackPressed();  // optional depending on your needs
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_ENABLE_BLUETOOTH) {
            if (resultCode == RESULT_OK) {
                mSmoothBluetooth.tryConnection();
            }
        }
    }

    // Implementazione dei listener
    private final BluetoothSPP.Listener mListener = new BluetoothSPP.Listener() {

        @Override
        public void onBluetoothNotSupported() {
            //Toast.makeText(Ossimetro.this, "Bluetooth not found", Toast.LENGTH_SHORT).show();
            AlertDialog.Builder alert = new AlertDialog.Builder(Bilancia.this);
            alert.setMessage(getString(R.string.nobluetooth));
            alert.setPositiveButton(getString(R.string.exit), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
            alert.setCancelable(false);
            alert.show();
        }

        @Override
        public void onBluetoothNotEnabled() {
            Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBluetooth, REQUEST_ENABLE_BLUETOOTH);
        }

        @Override
        public void onConnecting(Device device) {
            progress_text.setVisibility(View.VISIBLE);
            progress_text.setText(getString(R.string.connecting));
        }

        @Override
        public void onConnected(Device device) {
            countdown.cancel();
            layout_header.setVisibility(View.INVISIBLE);
            //Controllo dati utente
            mSmoothBluetooth.send(cf351bt.createTxFECmd());

        }

        @Override
        public void onDisconnected() {

            if (state == 0) {
                layout_mesure.setVisibility(View.INVISIBLE);
                //Pulisco misurazioni
                misurazioni.clear();
                //inizio thread countdown
                countdown.start();
            }
        }

        @Override
        public void onConnectionFailed(Device device) {
            if (state == 0) {
                //Pulisco misurazioni
                misurazioni.clear();
                //inizio thread countdown
                countdown.start();
            } else {
                countdown.cancel();
            }
        }

        @Override
        public void onDiscoveryStarted() {
        }

        @Override
        public void onDiscoveryFinished() {

        }

        @Override
        public void onNoDevicesFound() {
        }

        @Override
        public void onDevicesFound(final List<Device> deviceList, final BluetoothSPP.ConnectionCallback connectionCallback) {
        }

        @Override
        public void onDataReceived(int[] data) {
            if (state != 2) {
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                data_text.setText(sdf.format(new Date().getTime()));
                misurazioni = new ArrayList<>();

                for (int i = 0; i < data.length; i++) {
                    risposta[j++] = data[i];
                }

                if(j==32){
                    layout_mesure.setVisibility(View.VISIBLE);
                    final WeightRecord result = cf351bt.getWeightRecord(data);

                    //Peso
                    peso_value.setText(String.valueOf(result.getPeso() + " Kg"));

                    DataPoint Weight_HIS = new DataPoint(new Date(), "Peso", "DOUBLE", (double) result.getPeso(), "kg", "false");
                    DataPoint FOOD = new DataPoint(new Date(), "Grasso", "INT", (double) result.getGrasso_viscerale(), "0", "false");
                    DataPoint BoadyFat_HIS = new DataPoint(new Date(), "Grasso_corporeo", "DOUBLE", (double) result.getGrasso_corporeo(), "%", "false");
                    DataPoint BoadyWater_HIS = new DataPoint(new Date(), "Acqua", "DOUBLE", (double) result.getAcqua_corporea(), "%", "false");
                    DataPoint BoadyMuscle_HIS = new DataPoint(new Date(), "Massa_muscolare", "DOUBLE", (double) result.getMuscoli(), "%", "false");

                    misurazioni.add(Weight_HIS);

                    if(result.getGrasso_viscerale()!=0) {
                        misurazioni.add(FOOD);
                    }
                    if(result.getGrasso_corporeo()!=0) {
                        misurazioni.add(BoadyFat_HIS);
                    }
                    if(result.getAcqua_corporea()!=0) {
                        misurazioni.add(BoadyWater_HIS);
                    }
                    if(result.getMuscoli()!=0) {
                        misurazioni.add(BoadyMuscle_HIS);
                    }

                    progress_mesure.setVisibility(View.VISIBLE);
                    linearLayout_data.setVisibility(View.VISIBLE);


                    if (misurazioni.size() == 5 || misurazioni.size()==1) {
                        new CountDownTimer(3000, 1000) {
                            public void onTick(long millisUntilFinished) {
                                progress_mesure.setProgress((int) (millisUntilFinished / 1000) % 60);
                            }

                            @Override
                            public void onFinish() {
                                progress_mesure.setProgress(0);
                                state = 1;
                                mSmoothBluetooth.disconnect();
                                j=0;
                                t1.speak("Peso " + result.getPeso() + " Chilogrammi", TextToSpeech.QUEUE_FLUSH, null);

                                runTask();
                            }
                        }.start();
                    }
                }

            }

        }

        @Override
        public void onDeviceFound(BluetoothDevice device, Context context, BroadcastReceiver mReceiver) {

        }
    };


    /**
     * Esecuzione dei task e controlli
     */
    private void runTask(){
        if(dm.checkMobileInternetConn() || dw.checkMobileInternetConn()) {
            new InvioDati(misurazioni).execute();
        }else{
            alertDialogBuilder.setTitle("Esito");
            alertDialogBuilder.setMessage("Nessuna Connessione Internet");
            alertDialogBuilder.setCancelable(false);
            alertDialogBuilder.setPositiveButton("Riprova", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                    runTask();
                }
            });
            alertDialogBuilder.setNeutralButton("Chiudi", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            android.app.AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
    }



    /**
     * Esecuzione dei task e controlli
     */
    private void runTaskSync(){
        Sincronizza sync = new Sincronizza(sync_lista_NotSend);
        sync.execute();
    }

    /**
     * Metodo di Invio Dati al Server
     */
    private class InvioDati extends AsyncTask<Void, Void, String> {
        ArrayList<DataPoint> misurazioni;

        public InvioDati(ArrayList<DataPoint> misurazioni) {
            this.misurazioni = misurazioni;
        }

        protected void onPreExecute() {
            layout_sync.setBackgroundColor(Color.parseColor("#FFCC00"));
            layout_sync.setVisibility(View.VISIBLE);
            sync_message.setText(getString(R.string.invio_misurazione));
        }

        protected String doInBackground(Void... params) {
            //Caricamento WS
            WebServiceTechTablHealth serviceEasyDom = new WebServiceTechTablHealth();
            SimpleDateFormat sdf_res = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            Rilevazione rilevazione = new Rilevazione(0, 0, 0, "1", "HIS", "0", "0", dispositivi_selezionato.getMac(), sdf_res.format(misurazioni.get(0).getData().getTime()), MAC, misurazioni);

            String message = "0"; //0 Errore 1 Solo Locale 2 WebLocale

            InsertMeasurementResponse IDRilevazione = serviceEasyDom.InsertMeasurement(rilevazione);
            if (IDRilevazione.getValue() != 0 && IDRilevazione.getValue() != -1) {
                Log.i("IDRilevazione", String.valueOf(IDRilevazione));
                Log.i("WS", "Inizio Invio Dati Rilevazione");

                //Inserimento in DB Locale
                ArrayList<Long> listID = new ArrayList<>();
                for (DataPoint misura : misurazioni) {
                    long idHIS = databaseHelper.inserisciMisuraHIS(misura);
                    listID.add(idHIS);
                    Log.i("insert DB mOss", String.valueOf(idHIS));
                    if (idHIS != 0) {
                        misura.setId((int) idHIS);
                    }
                }

                for (DataPoint misura : misurazioni) {
                    misura.setIdAssisted(IDRilevazione.getValue());
                    Log.e("ASSISTITO", String.valueOf(misura.getIdAssisted()));
                    InsertDataMeasurementResponse controllo = serviceEasyDom.InsertDataMeasurement(misura, IDRilevazione.getValue(), 1);
                    if (controllo != null) {
                        Log.i("Controllo WS", String.valueOf(controllo.isValue()));
                        if (controllo.isValue()) {
                            misura.setSend("true");
                            Log.i("UPDATE TRUE", String.valueOf(databaseHelper.updateBilancia(misura)));
                            message = controllo.getMessage();
                        } else {
                            misura.setSend("false");
                            Log.i("UPDATE FALSE", String.valueOf(databaseHelper.updateBilancia(misura)));
                            message = controllo.getMessage();
                        }
                    } else {
                        misura.setSend("false");
                        Log.i("UPDATE FALSE", String.valueOf(databaseHelper.updateBilancia(misura)));
                        message = controllo.getMessage();
                    }
                }
            } else {
                message = IDRilevazione.getMessage();
                if(IDRilevazione.getValue() != 0 && IDRilevazione.getValue() != -1){


                }else {
                    ArrayList<Long> listID = new ArrayList<>();
                    for (DataPoint misura : misurazioni) {
                        long idHIS = databaseHelper.inserisciMisuraHIS(misura);
                        listID.add(idHIS);
                        Log.i("insert DB mOss", String.valueOf(idHIS));
                        if (idHIS != 0) {
                            misura.setId((int) idHIS);
                        }
                    }
                    for (DataPoint misura : misurazioni) {
                        misura.setIdAssisted(0);
                        misura.setSend("false");
                        Log.i("UPDATE FALSE", String.valueOf(databaseHelper.updateBilancia(misura)));

                    }
                }
            }

            Log.i("Status Invio ", message);

            return message;
        }

        protected void onPostExecute (String status){
            Log.e("Status", String.valueOf(status));
            //0 Errore 1 Solo Locale 2 WebLocale
            new android.app.AlertDialog.Builder(thisContext)
                        .setTitle("Esito")
                        .setMessage(status)
                        .setNeutralButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                layout_sync.setVisibility(View.INVISIBLE);
                                layout_menu.setVisibility(View.VISIBLE);
                                progress_mesure.setVisibility(View.INVISIBLE);
                            }
                        })
                        .show();
                state = 2;

            layout_menu.setVisibility(View.VISIBLE);
            mSmoothBluetooth.disconnect();
        }
    }

    /**
     * Metodo di Sincronizzazione dei dati
     */
    private class Sincronizza extends AsyncTask<Void, Void, Boolean> {
        ArrayList<DataPoint> misurazioni;

        public Sincronizza(ArrayList<DataPoint> misurazioni) {
            this.misurazioni = misurazioni;
        }

        protected void onPreExecute() {
            layout_sync.setBackgroundColor(Color.parseColor("#FFCC00"));
            layout_sync.setVisibility(View.VISIBLE);
            progress_sync.setVisibility(View.VISIBLE);
        }

        protected Boolean doInBackground(Void... params) {
            //Caricamento WS
            WebServiceTechTablHealth serviceEasyDom = new WebServiceTechTablHealth();
            SimpleDateFormat sdf_res = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            int contatore = 0;
            for (DataPoint misura : misurazioni) {
                if (misura.getIdAssisted() != 0) {
                    InsertDataMeasurementResponse controllo = serviceEasyDom.InsertDataMeasurement(misura, misura.getIdAssisted(), 1);
                    if (controllo != null) {
                        if (controllo.isValue()) {
                            contatore++;
                            misura.setSend("true");
                            Log.i("UPDATE TRUE", String.valueOf(databaseHelper.updateBilancia(misura)));
                        } else {
                            misura.setSend("false");
                            Log.i("UPDATE FALSE", String.valueOf(databaseHelper.updateBilancia(misura)));
                        }
                    } else {
                        misura.setSend("false");
                        Log.i("UPDATE FALSE", String.valueOf(databaseHelper.updateBilancia(misura)));
                    }
                } else {
                    Rilevazione rilevazione = new Rilevazione(0, 0, 0, "1", "HIS", "0", "0", dispositivi_selezionato.getMac(), sdf_res.format(misura.getData().getTime()), MAC, misurazioni);
                    InsertMeasurementResponse IDRilevazione = serviceEasyDom.InsertMeasurement(rilevazione);
                    if (IDRilevazione.getValue() != 0 && IDRilevazione.getValue() != -1) {
                        misura.setIdAssisted(IDRilevazione.getValue());
                        InsertDataMeasurementResponse controllo = serviceEasyDom.InsertDataMeasurement(misura, IDRilevazione.getValue(), 1);
                        if (controllo.isValue()) {
                            contatore++;
                            misura.setSend("true");
                            Log.i("UPDATE TRUE", String.valueOf(databaseHelper.updateBilancia(misura)));
                        } else {
                            misura.setSend("false");
                            Log.i("UPDATE FALSE", String.valueOf(databaseHelper.updateBilancia(misura)));
                        }
                    } else {
                        misura.setIdAssisted(0);
                        misura.setSend("false");
                        Log.i("UPDATE FALSE", String.valueOf(databaseHelper.updateBilancia(misura)));
                        return false;
                    }
                }
            }
            Log.i("Contatore Sync", String.valueOf(contatore));
            if (contatore >= misurazioni.size()) {
                return true;
            } else {
                return false;
            }
        }

        protected void onPostExecute(Boolean success) {
            if (success) {
                layout_sync.setBackgroundColor(Color.parseColor("#078446"));
                sync_message.setText("Sincronizzazione Completata");
                progress_sync.setVisibility(View.INVISIBLE);
                new CountDownTimer(3000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        layout_sync.setVisibility(View.INVISIBLE);
                    }
                }.start();
            } else {
                sync_message.setText("Sincronizzazione Non Completata");
                progress_sync.setVisibility(View.INVISIBLE);
                new CountDownTimer(3000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }

                    @Override
                    public void onFinish() {
                        layout_sync.setVisibility(View.INVISIBLE);
                    }
                }.start();
            }
        }
    }
}
