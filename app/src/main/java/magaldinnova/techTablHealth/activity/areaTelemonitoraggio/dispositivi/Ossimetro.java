package magaldinnova.techTablHealth.activity.areaTelemonitoraggio.dispositivi;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.StrictMode;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import magaldinnova.techTablHealth.R;
import magaldinnova.techTablHealth.activity.adapter.DevicesAdapter;
import magaldinnova.techTablHealth.activity.adapter.MyValueFormatter;
import magaldinnova.techTablHealth.activity.detector.MobileInternetConnectionDetector;
import magaldinnova.techTablHealth.activity.detector.WIFIInternetConnectionDetector;
import magaldinnova.techTablHealth.database.dbHelper.DatabaseHelper;
import magaldinnova.techTablHealth.database.oggetti.DataPoint;
import magaldinnova.techTablHealth.database.oggetti.Dispositivi;
import magaldinnova.techTablHealth.library.BluetoothSPP;
import magaldinnova.techTablHealth.moduli.PC60NW;
import magaldinnova.techTablHealth.database.record.SpO2Record;
import magaldinnova.techTablHealth.webService.oggetti.InsertDataMeasurementResponse;
import magaldinnova.techTablHealth.webService.oggetti.InsertMeasurementResponse;
import magaldinnova.techTablHealth.webService.oggetti.Rilevazione;
import magaldinnova.techTablHealth.webService.WebServiceTechTablHealth;
import magaldinnova.techTablHealth.library.Device;

/**
 * Modulo Ossimetro
 * @author Magaldi Innova srl
 */
public class Ossimetro extends AppCompatActivity {

    //Caricamento Variabili
    private static final int REQUEST_ENABLE_BLUETOOTH = 1;
    Context thisContext;
    private MobileInternetConnectionDetector dm;
    private WIFIInternetConnectionDetector dw;
    private android.app.AlertDialog.Builder alertDialogBuilder;

    //Caricamento Bluetooth
    private BluetoothSPP mSmoothBluetooth;
    private int state = 0;
    private Dispositivi dispositivi_selezionato, ossimetro;
    private String MAC;

    //Caricamento oggettoDispositivo
    private PC60NW pc60nw;
    private ArrayList<DataPoint> misurazioni, rilevazioni;

    //Autoconnessione
    private CountDownTimer countdown;

    //Caricamento Elementi Grafici
    private TextView spo2;
    private TextView pr;
    private TextView pi;
    private TextView data_text;
    private TextView progress_text;
    private TextView sync_message;
    private RoundCornerProgressBar progress_mesure;
    private RelativeLayout layout_header;
    private LinearLayout layout_menu, layout_mesure, layout_sync;
    private AlertDialog.Builder alert;
    private ProgressBar progress_sync;
    private LineChart grafico_spo2, grafico_pr;
    private LineData data, data2;

    //Caricamento DB
    private DatabaseHelper databaseHelper;
    private ArrayList<DataPoint> sync_lista_NotSend;

    //Caricamento VOCE
    private TextToSpeech t1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ossimetro);

        getSupportActionBar().setElevation(0);

        // --------- CARICAMENTO ELEMENTI GRAFICI ---------- //
        //MISURAZIONI
        layout_mesure = (LinearLayout) findViewById(R.id.layout_mesure);
        spo2 = (TextView) findViewById(R.id.spo2_value);
        pr = (TextView) findViewById(R.id.pr_value);
        pi = (TextView) findViewById(R.id.pi_value);
        data_text = (TextView) findViewById(R.id.data_text);
        grafico_spo2 = (LineChart)findViewById(R.id.grafico_spo2);
        grafico_pr = (LineChart)findViewById(R.id.grafico_pr);
        progress_mesure = (RoundCornerProgressBar) findViewById(R.id.progress_mesure);
        progress_mesure.setMax(20);
        progress_mesure.setProgressColor(getResources().getColor(R.color.status));
        progress_mesure.setProgressBackgroundColor(Color.parseColor("#e4ebff"));
        //STATUS CONNESSIONE
        layout_header = (RelativeLayout)findViewById(R.id.layout_header);
        progress_text = (TextView)findViewById(R.id.progress_text);
        // INVIO DATI
        layout_menu = (LinearLayout) findViewById(R.id.layout_menu);
        LinearLayout btn_fine = (LinearLayout) findViewById(R.id.btn_fine);
        LinearLayout btn_nuova_misurazione = (LinearLayout) findViewById(R.id.btn_nuova_misurazione);
        //SINCRONIZZAZIONE
        layout_sync = (LinearLayout)findViewById(R.id.layout_sync);
        sync_message = (TextView)findViewById(R.id.sync_message);
        progress_sync = (ProgressBar)findViewById(R.id.progress_sync);
        // --------- CARICAMENTO ELEMENTI GRAFICI ---------- //


        layout_mesure.setVisibility(View.INVISIBLE);

        // Create a new instance of Bluetooth
        mSmoothBluetooth = new BluetoothSPP(this);
        mSmoothBluetooth.setListener(mListener);
        pc60nw = new PC60NW();

        misurazioni = new ArrayList<>();
        rilevazioni = new ArrayList<>();


        //Caricamento dati grafici
        data = new LineData();
        data2 = new LineData();

        thisContext = this;

        //Controllo connessione
        dm=new MobileInternetConnectionDetector(thisContext);
        dw=new WIFIInternetConnectionDetector(thisContext);
        alertDialogBuilder = new android.app.AlertDialog.Builder(thisContext);

        //SetTitle
        getSupportActionBar().setTitle("Ossimetro");

        //Caricamento mAssistito
        Intent intent= getIntent();
        Bundle b = intent.getExtras();

        if(b!=null)
        {
            MAC = b.getString("MAC");
            ossimetro = (Dispositivi)b.getSerializable("Ossimetro");
        }

        //Aperture DB
        databaseHelper = new DatabaseHelper(this);
        dispositivi_selezionato = databaseHelper.getDispositivoByName("Ossimetro");

        if (ossimetro != null) {
            if (!dispositivi_selezionato.getMac().equals(ossimetro.getMac())) {
                dispositivi_selezionato = ossimetro;
                databaseHelper.updateDevice(ossimetro);
            }
        }

        //Autoconnesione
        countdown = new CountDownTimer(1000, 1000) {
            public void onTick(long millisUntilFinished) {
                Log.e("CountDown", getString(R.string.tryconnect) + (int) (millisUntilFinished / 1000) % 60);
            }

            public void onFinish() {
                if (dispositivi_selezionato != null) {
                    layout_header.setVisibility(View.VISIBLE);
                    progress_text.setText(getString(R.string.connecting));
                    if(state!=2) {
                        mSmoothBluetooth.connect(dispositivi_selezionato.getMac());
                    }
                } else {
                    layout_header.setVisibility(View.VISIBLE);
                    progress_text.setText(getString(R.string.pair));
                }
            }
        };

        btn_fine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                databaseHelper.close();

                finish();

            }
        });

        btn_nuova_misurazione.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("Nuova Misurazione", String.valueOf(misurazioni.size()));
                if(misurazioni.size()!=0){
                    misurazioni.clear();
                    countdown.start();
                }
                layout_header.setVisibility(View.VISIBLE);
                progress_text.setText(getString(R.string.connecting));
                layout_menu.setVisibility(View.INVISIBLE);
                layout_mesure.setVisibility(View.INVISIBLE);
                layout_sync.setVisibility(View.INVISIBLE);
                state=0;
            }
        });

        countdown.start();

        //Sincronizzazione Rilevazioni
        layout_sync.setVisibility(View.INVISIBLE);
        sync_lista_NotSend = databaseHelper.getElencoMisurazioniSPO_NotSend();
        if(sync_lista_NotSend.size()>0){
            runTaskSync();
        }else{
            layout_sync.setVisibility(View.INVISIBLE);
        }

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);


        //Configurazione Grafici
        grafico_spo2.clear();
        grafico_spo2.setDrawGridBackground(false);
        grafico_spo2.setDrawBorders(false);
        grafico_spo2.setDescription("SPO2");
        grafico_spo2.setPinchZoom(false);
        grafico_spo2.setTouchEnabled(false);
        Legend l = grafico_spo2.getLegend();
        l.setEnabled(false);
        XAxis xAxis = grafico_spo2.getXAxis();
        xAxis.setEnabled(false);
        YAxis leftAxis = grafico_spo2.getAxisLeft();
        leftAxis.setDrawAxisLine(false);
        leftAxis.setDrawZeroLine(false);
        leftAxis.setDrawGridLines(false);
        leftAxis.setEnabled(false);
        grafico_spo2.getAxisRight().setEnabled(false);
        grafico_spo2.setData(data);

        grafico_pr.clear();
        grafico_pr.setDrawGridBackground(false);
        grafico_pr.setDrawBorders(false);
        grafico_pr.setDescription("PR");
        grafico_pr.setPinchZoom(false);
        grafico_pr.setTouchEnabled(false);
        Legend l2 = grafico_pr.getLegend();
        l2.setEnabled(false);
        XAxis xAxis2 = grafico_pr.getXAxis();
        xAxis2.setEnabled(false);
        YAxis leftAxis2 = grafico_pr.getAxisLeft();
        leftAxis2.setDrawAxisLine(false);
        leftAxis2.setDrawZeroLine(false);
        leftAxis2.setDrawGridLines(false);
        leftAxis2.setEnabled(false);
        grafico_pr.getAxisRight().setEnabled(false);
        grafico_pr.setData(data2);

        //Caricamento Voce
        t1=new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if(status != TextToSpeech.ERROR) {
                    t1.setLanguage(Locale.ITALIAN);
                }
            }
        });
    }

    protected void onStart() {
        super.onStart();
        if(mSmoothBluetooth.isBluetoothEnabled()==false){
            Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBluetooth, REQUEST_ENABLE_BLUETOOTH);
        }
    }

    public void onPause(){
        if(t1 !=null){
            t1.stop();
            t1.shutdown();
        }
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSmoothBluetooth.stop();
        countdown.cancel();
    }

    @Override
    protected void onStop() {
        super.onStop();
        // Disconnect from the remote device and close the serial port
        mSmoothBluetooth.stop();
    }

    @Override
    public void onBackPressed() {
        Thread.currentThread().interrupt();
        countdown.cancel();
        state = -1;
        super.onBackPressed();  // optional depending on your needs
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_ENABLE_BLUETOOTH) {
            if (resultCode == RESULT_OK) {
                mSmoothBluetooth.tryConnection();
            }
        }
    }


    // Implementazione dei listener
    private final BluetoothSPP.Listener mListener = new BluetoothSPP.Listener() {
        @Override
        public void onBluetoothNotSupported() {
            //Toast.makeText(Ossimetro.this, "Bluetooth not found", Toast.LENGTH_SHORT).show();
            alert = new AlertDialog.Builder(Ossimetro.this);
            alert.setMessage(getString(R.string.nobluetooth));
            alert.setPositiveButton(getString(R.string.exit), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
            alert.setCancelable(false);
            alert.show();
        }

        @Override
        public void onBluetoothNotEnabled() {
            Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBluetooth, REQUEST_ENABLE_BLUETOOTH);
        }

        @Override
        public void onConnecting(Device device) {
            Log.i("onConnecting", device.getName());
            layout_header.setVisibility(View.VISIBLE);
            progress_text.setText(getString(R.string.connecting));
        }

        @Override
        public void onConnected(Device device) {
            //Toast.makeText(Ossimetro.this, "Connesso a: " + dispositivi_selezionato.getNome(), Toast.LENGTH_SHORT).show();
            countdown.cancel();
            layout_header.setVisibility(View.INVISIBLE);
            data.clearValues();
            data2.clearValues();
            mSmoothBluetooth.send(pc60nw.createTx03Cmd());
        }

        @Override
        public void onDisconnected() {

            if (state == 0) {
                layout_mesure.setVisibility(View.INVISIBLE);
                //Pulisco misurazioni
                misurazioni.clear();
                //inizio thread countdown
                countdown.start();
            }

        }

        @Override
        public void onConnectionFailed(Device device) {
            //Toast.makeText(Ossimetro.this, "Failed to connect", Toast.LENGTH_SHORT).show();

            if (state == 0) {
                //Pulisco misurazioni
                misurazioni.clear();
                //inizio thread countdown
                countdown.start();
            } else {
                countdown.cancel();
            }
        }

        @Override
        public void onDiscoveryStarted() {
            //Toast.makeText(Ossimetro.this, "Searching", Toast.LENGTH_SHORT).show();
            layout_header.setVisibility(View.VISIBLE);
            progress_text.setText(getString(R.string.discovery_started));
        }

        @Override
        public void onDiscoveryFinished() {
        }

        @Override
        public void onNoDevicesFound() {
            //Toast.makeText(Ossimetro.this, "No device found", Toast.LENGTH_SHORT).show();
            alert = new AlertDialog.Builder(Ossimetro.this);
            alert.setTitle(getString(R.string.discovery_title));
            alert.setMessage(getString(R.string.discovery_notfound));
            alert.setPositiveButton(getString(R.string.riprova), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    mSmoothBluetooth.doDiscovery();
                }
            });
            alert.setNeutralButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    alert.show().cancel();
                    progress_text.setText("");
                }
            });
            alert.setCancelable(false);
            alert.show();
        }

        @Override
        public void onDevicesFound(final List<Device> deviceList,
                                   final BluetoothSPP.ConnectionCallback connectionCallback) {

            final MaterialDialog dialog = new MaterialDialog.Builder(Ossimetro.this)
                    .title(getString(R.string.devices))
                    .adapter(new DevicesAdapter(Ossimetro.this, deviceList), null)
                    .build();

            ListView listView = dialog.getListView();
            if (listView != null) {
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        dispositivi_selezionato = new Dispositivi("Ossimetro", deviceList.get(position).getAddress());
                        databaseHelper.createDevice(dispositivi_selezionato);
                        connectionCallback.connectTo(deviceList.get(position));
                        dialog.dismiss();
                    }

                });
            }
            dialog.show();
        }

        @Override
        public void onDataReceived(int[] data) {
            if(state!=2) {
                progress_mesure.setVisibility(View.VISIBLE);
                //Data Misurazione
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                data_text.setText(sdf.format(new Date().getTime()));

                if (data.length == 12) {
                    SpO2Record result = pc60nw.getSpO2Rec(data);
                    if (result.getmSpO2() != 0 && result.getmPulse() != 0) {
                        int mSpO2 = result.getmSpO2();
                        int mPulse = result.getmPulse();
                        String mAlarm = result.getmAlarm();
                        double mPerfIndex = result.getmPerfIndex().shortValue();
                        double mBatteria = result.getmBatteria().shortValue();
                        spo2.setText(String.valueOf(mSpO2) + " %");
                        pr.setText(String.valueOf(mPulse) + " bpm");
                        pi.setText(String.valueOf(mPerfIndex) + " %");

                        //Controllo batteria
                        Double b50 = 50.0;
                        Double b25 = 25.0;

                        if (mBatteria > b25 && mBatteria <= b50) {
                            //layout_batteria.setVisibility(View.VISIBLE);
                            //layout_batteria.setBackgroundColor(Color.parseColor("#CC0000"));
                        }

                        layout_header.setVisibility(View.INVISIBLE);
                        layout_mesure.setVisibility(View.VISIBLE);

                        DataPoint SPO = new DataPoint(new Date(), "Saturazione", "INT", (double)result.getmSpO2(), "%", "false");
                        DataPoint SPO_pulse = new DataPoint(new Date(), "Pulsazioni_ossimetro", "INT", (double)result.getmPulse(), "bpm", "false");

                        rilevazioni.add(SPO);
                        rilevazioni.add(SPO_pulse);

                        Log.i("onBluetoothSerialRead", "Size misurazioni: " + rilevazioni.size());

                        addEntrySPO2(SPO);
                        addEntryPR(SPO_pulse);

                        progress_mesure.setProgress(20-rilevazioni.size());

                        if (rilevazioni.size() == 20) {
                            Date dataRilevazione = new Date();
                            progress_mesure.setProgress(0);
                            int sumSpo2 = 0;
                            int sumPr = 0;
                            int sizeSpo2 = 0;
                            int sizePr = 0;
                            for (int i = 0; i < rilevazioni.size(); i++) {
                                if(rilevazioni.get(i).getName() == "Saturazione"){
                                    sumSpo2 += rilevazioni.get(i).getValue();
                                    sizeSpo2++;
                                }
                                if(rilevazioni.get(i).getName() == "Pulsazioni_ossimetro"){
                                    sumPr += rilevazioni.get(i).getValue();
                                    sizePr++;
                                }
                            }
                            int mediaSpo2 = sumSpo2 / sizeSpo2;
                            int mediaPr = sumPr / sizePr;

                            SimpleDateFormat sdf_res = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                            data_text.setText(sdf_res.format(new Date().getTime()));

                            DataPoint mSPO = new DataPoint(dataRilevazione, "Saturazione", "INT", (double)result.getmSpO2(), "%", "false");
                            DataPoint mSPO_pulse = new DataPoint(dataRilevazione, "Pulsazioni_ossimetro", "INT", (double)result.getmPulse(), "bpm", "false");

                            misurazioni.add(mSPO);
                            misurazioni.add(mSPO_pulse);

                            rilevazioni.clear();
                            spo2.setText(String.valueOf(mediaSpo2) + " %");
                            pr.setText(String.valueOf(mediaPr) + " bpm");
                            state = 1;
                            mSmoothBluetooth.disconnect();

                            t1.speak("Saturazione " + mediaSpo2 + " %", TextToSpeech.QUEUE_FLUSH, null);

                            runTask();

                        }
                    }
                }
            }
        }

        @Override
        public void onDeviceFound(BluetoothDevice device, Context context, BroadcastReceiver mReceiver) {

        }
    };

    /**
     * Esecuzione dei task e controlli
     */
    private void runTask(){
        if(dm.checkMobileInternetConn() || dw.checkMobileInternetConn()) {
            new InvioDati(misurazioni).execute();
        }else{
            alertDialogBuilder.setTitle("Esito");
            alertDialogBuilder.setMessage("Nessuna Connessione Internet");
            alertDialogBuilder.setCancelable(false);
            alertDialogBuilder.setPositiveButton("Riprova", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                            runTask();
                        }
                    });
            alertDialogBuilder.setNeutralButton("Chiudi", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            alertDialogBuilder.setNeutralButton("Memorizza sul Dispositivo", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    new InvioDati(misurazioni).execute();
                }
            });
            android.app.AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();
        }
    }

    /**
     * Esecuzione dei task e controlli
     */
    private void runTaskSync(){
        Sincronizza sync = new Sincronizza(sync_lista_NotSend);
        sync.execute();
    }

    /**
     * Creazione Grafico Saturazione
     * @return
     */
    private LineDataSet createSetSPO2(){
        LineDataSet dataset = new LineDataSet(null, "SPO2");
        dataset.setDrawFilled(true);
        dataset.setColor(Color.parseColor("#BE39C0"));
        dataset.setFillColor(Color.parseColor("#BE39C0"));
        dataset.setCircleColor(Color.parseColor("#BE39C0"));
        dataset.setValueTextSize(10f);
        dataset.setValueFormatter(new MyValueFormatter(dispositivi_selezionato.getNome()));
        dataset.setValueTypeface(Typeface.DEFAULT_BOLD);
        dataset.setHighlightEnabled(false);
        return dataset;
    }

    /**
     * Inserimento Dati Saturazione nel Grafico
     * @param oggetto
     */
    private void addEntrySPO2(DataPoint oggetto) {

        LineData data = grafico_spo2.getData();

        if (data != null) {

            ILineDataSet set = data.getDataSetByIndex(0);
            // set.addEntry(...); // can be called as well
            if (set == null) {
                set = createSetSPO2();
                data.addDataSet(set);
            }
            data.addEntry(new Entry(set.getEntryCount(), oggetto.getValue().floatValue()), 0);
            data.notifyDataChanged();
            // let the chart know it's data has changed
            grafico_spo2.notifyDataSetChanged();
            // limit the number of visible entries
            grafico_spo2.setVisibleXRangeMaximum(120);
            // mChart.setVisibleYRange(30, AxisDependency.LEFT);
            // move to the latest entry
            grafico_spo2.moveViewToX(data.getEntryCount());
            // this automatically refreshes the chart (calls invalidate())
            // mChart.moveViewTo(data.getXValCount()-7, 55f,
            // AxisDependency.LEFT);
        }
    }

    /**
     * Creazione Grafico Pressione
     * @return
     */
    private LineDataSet createSetPR(){
        LineDataSet dataset = new LineDataSet(null, "PR");
        dataset.setDrawFilled(true);
        dataset.setColor(Color.parseColor("#137613"));
        dataset.setFillColor(Color.parseColor("#137613"));
        dataset.setCircleColor(Color.parseColor("#137613"));
        dataset.setValueTextSize(10f);
        dataset.setValueFormatter(new MyValueFormatter(dispositivi_selezionato.getNome()));
        dataset.setValueTypeface(Typeface.DEFAULT_BOLD);
        dataset.setHighlightEnabled(false);
        return dataset;
    }

    /**
     * Inserimento Dati Pressione nel Grafico
     * @param oggetto
     */
    private void addEntryPR(DataPoint oggetto) {
        LineData data = grafico_pr.getData();
        if (data != null) {
            ILineDataSet set = data.getDataSetByIndex(0);
            // set.addEntry(...); // can be called as well
            if (set == null) {
                set = createSetPR();
                data.addDataSet(set);
            }
            data.addEntry(new Entry(set.getEntryCount(), oggetto.getValue().floatValue()), 0);
            data.notifyDataChanged();
            // let the chart know it's data has changed
            grafico_pr.notifyDataSetChanged();
            // limit the number of visible entries
            grafico_pr.setVisibleXRangeMaximum(120);
            // mChart.setVisibleYRange(30, AxisDependency.LEFT);
            // move to the latest entry
            grafico_pr.moveViewToX(data.getEntryCount());
            // this automatically refreshes the chart (calls invalidate())
            // mChart.moveViewTo(data.getXValCount()-7, 55f,
            // AxisDependency.LEFT);
        }
    }

    /**
     * Metodo di Invio Dati al Server
     */
    private class InvioDati extends AsyncTask<Void, Void, String> {
        ArrayList<DataPoint> misurazioni;

        public InvioDati(ArrayList<DataPoint> misurazioni) {
            this.misurazioni = misurazioni;
        }

        protected void onPreExecute() {
            layout_sync.setBackgroundColor(Color.parseColor("#FFCC00"));
            layout_sync.setVisibility(View.VISIBLE);
            sync_message.setText(getString(R.string.invio_misurazione));
        }

        protected String doInBackground(Void... params) {
            //Caricamento WS
            WebServiceTechTablHealth serviceEasyDom = new WebServiceTechTablHealth();
            SimpleDateFormat sdf_res = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            Rilevazione rilevazione = new Rilevazione(0, 0, 0, "2", "SPO", "0","0", dispositivi_selezionato.getMac() ,sdf_res.format(misurazioni.get(0).getData().getTime()), MAC, misurazioni);

            String message = "0"; //0 Errore 1 Solo Locale 2 WebLocale

            InsertMeasurementResponse IDRilevazione = serviceEasyDom.InsertMeasurement(rilevazione);
            if (IDRilevazione.getValue() != 0 && IDRilevazione.getValue()!=-1) {
                Log.i("IDRilevazione", String.valueOf(IDRilevazione));
                Log.i("WS", "Inizio Invio Dati Rilevazione");

                //Inserimento in DB Locale
                ArrayList<Long> listID = new ArrayList<>();
                if(misurazioni.size()==2){
                    for(DataPoint misura: misurazioni) {
                        long idSPO = databaseHelper.inserisciMisuraSPO(misura);
                        listID.add(idSPO);
                        Log.i("insert DB mOss", String.valueOf(idSPO));
                        if(idSPO!=0){
                            misura.setId((int) idSPO);
                        }
                    }
                }

                for (DataPoint misura: misurazioni) {
                    misura.setIdAssisted(IDRilevazione.getValue());
                    InsertDataMeasurementResponse controllo = serviceEasyDom.InsertDataMeasurement(misura, IDRilevazione.getValue(), 2);
                    if (controllo != null) {
                        Log.i("Controllo WS", String.valueOf(controllo.isValue()));
                        if (controllo.isValue()) {
                            misura.setSend("true");
                            Log.i("UPDATE TRUE", String.valueOf(databaseHelper.updateOssimetro(misura)));
                            message = controllo.getMessage();
                        } else {
                            misura.setSend("false");
                            Log.i("UPDATE FALSE", String.valueOf(databaseHelper.updateOssimetro(misura)));
                            message = controllo.getMessage();
                        }
                    } else {
                        misura.setSend("false");
                        Log.i("UPDATE FALSE", String.valueOf(databaseHelper.updateOssimetro(misura)));
                        message = controllo.getMessage();
                    }
                }
            }else{
                message = IDRilevazione.getMessage();
                if (IDRilevazione.getValue() == 0 || IDRilevazione.getValue() == -1) {
                    //Inserimento in DB Locale
                    ArrayList<Long> listID = new ArrayList<>();
                    if(misurazioni.size()==2){
                        for(DataPoint misura: misurazioni) {
                            long idSPO = databaseHelper.inserisciMisuraSPO(misura);
                            listID.add(idSPO);
                            Log.i("insert DB mOss", String.valueOf(idSPO));
                            if(idSPO!=0){
                                misura.setId((int) idSPO);
                            }
                        }
                    }

                    for (DataPoint misura : misurazioni) {
                        misura.setIdAssisted(0);
                        misura.setSend("false");
                        Log.i("UPDATE FALSE", String.valueOf(databaseHelper.updateOssimetro(misura)));
                    }
                }
            }

            Log.i("Status Invio ", message);

            return message;
        }
        protected void onPostExecute(String status) {
            Log.e("Status", String.valueOf(status));
            if(status.equals("Errore IOException WS")){
                status="Misurazione Salvata OFFLINE";
                state = 1;
            }else{
                state = 2;
            }
            //0 Errore 1 Solo Locale 2 WebLocale
            new android.app.AlertDialog.Builder(thisContext)
                    .setTitle("Esito")
                    .setMessage(status)
                    .setNeutralButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            layout_sync.setVisibility(View.INVISIBLE);
                            progress_mesure.setVisibility(View.INVISIBLE);
                        }
                    })
                    .show();

            layout_menu.setVisibility(View.VISIBLE);
            mSmoothBluetooth.disconnect();
        }
    }


    /**
     * Metodo di Sincronizzazione dei dati
     */
    private class Sincronizza extends AsyncTask<Void, Void, Boolean> {
        ArrayList<DataPoint> misurazioni;

        public Sincronizza(ArrayList<DataPoint> misurazioni) {
            this.misurazioni = misurazioni;
        }

        protected void onPreExecute() {
            layout_sync.setBackgroundColor(Color.parseColor("#FFCC00"));
            layout_sync.setVisibility(View.VISIBLE);
            progress_sync.setVisibility(View.VISIBLE);
        }

        protected Boolean doInBackground(Void... params) {
            //Caricamento WS
            WebServiceTechTablHealth serviceEasyDom = new WebServiceTechTablHealth();
            SimpleDateFormat sdf_res = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            int contatore = 0;
            for(DataPoint misura:misurazioni){
                if(misura.getIdAssisted()!=0){
                    InsertDataMeasurementResponse controllo = serviceEasyDom.InsertDataMeasurement(misura, misura.getIdAssisted(), 2);
                    if(controllo!=null) {
                        if (controllo.isValue()) {
                            contatore++;
                            misura.setSend("true");
                            Log.i("UPDATE TRUE", String.valueOf(databaseHelper.updateOssimetro(misura)));
                        } else {
                            misura.setSend("false");
                            Log.i("UPDATE FALSE", String.valueOf(databaseHelper.updateOssimetro(misura)));
                        }
                    }else{
                        misura.setSend("false");
                        Log.i("UPDATE FALSE", String.valueOf(databaseHelper.updateOssimetro(misura)));
                    }
                }else{
                    Rilevazione rilevazione = new Rilevazione(0, 0, 0, "2", "SPO", "0","0",dispositivi_selezionato.getMac(), sdf_res.format(misura.getData().getTime()),MAC, misurazioni);
                    InsertMeasurementResponse IDRilevazione = serviceEasyDom.InsertMeasurement(rilevazione);
                        if (IDRilevazione.getValue() != 0 && IDRilevazione.getValue()!=-1) {
                            misura.setIdAssisted(IDRilevazione.getValue());
                            InsertDataMeasurementResponse controllo = serviceEasyDom.InsertDataMeasurement(misura, IDRilevazione.getValue(), 2);
                            if (controllo.isValue()) {
                                contatore++;
                                misura.setSend("true");
                                Log.i("UPDATE TRUE", String.valueOf(databaseHelper.updateOssimetro(misura)));
                            } else {
                                misura.setSend("false");
                                Log.i("UPDATE FALSE", String.valueOf(databaseHelper.updateOssimetro(misura)));
                            }
                        } else {
                            misura.setIdAssisted(0);
                            misura.setSend("false");
                            Log.i("UPDATE FALSE", String.valueOf(databaseHelper.updateOssimetro(misura)));
                            return false;
                        }
                }
            }
            Log.i("Contatore Sync", String.valueOf(contatore));
            if(contatore>=misurazioni.size()){
                return true;
            }else{
                return false;
            }
        }
        protected void onPostExecute(Boolean success) {
            if(success) {
                layout_sync.setBackgroundColor(Color.parseColor("#078446"));
                sync_message.setText("Sincronizzazione Completata");
                progress_sync.setVisibility(View.INVISIBLE);
                new CountDownTimer(3000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }
                    @Override
                    public void onFinish() {
                        layout_sync.setVisibility(View.INVISIBLE);
                    }
                }.start();
            }else{
                sync_message.setText("Sincronizzazione Non Completata");
                progress_sync.setVisibility(View.INVISIBLE);
                new CountDownTimer(3000, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {

                    }
                    @Override
                    public void onFinish() {
                        layout_sync.setVisibility(View.INVISIBLE);
                    }
                }.start();
            }
        }
    }
}
