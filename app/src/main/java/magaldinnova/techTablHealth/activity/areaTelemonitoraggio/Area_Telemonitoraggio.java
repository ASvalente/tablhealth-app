package magaldinnova.techTablHealth.activity.areaTelemonitoraggio;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.StrictMode;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import magaldinnova.techTablHealth.R;
import magaldinnova.techTablHealth.activity.About;
import magaldinnova.techTablHealth.activity.adapter.Adapter_device;
import magaldinnova.techTablHealth.activity.adapter.DevicesAdapter;
import magaldinnova.techTablHealth.activity.areaTelemonitoraggio.dispositivi.Bilancia;
import magaldinnova.techTablHealth.activity.areaTelemonitoraggio.dispositivi.Glucometro;
import magaldinnova.techTablHealth.activity.areaTelemonitoraggio.dispositivi.Ossimetro;
import magaldinnova.techTablHealth.activity.areaTelemonitoraggio.dispositivi.Sfigmomanometro;
import magaldinnova.techTablHealth.database.dbHelper.DatabaseHelper;
import magaldinnova.techTablHealth.database.oggetti.Dispositivi;
import magaldinnova.techTablHealth.library.BluetoothSPP;
import magaldinnova.techTablHealth.library.BluetoothUtils;
import magaldinnova.techTablHealth.library.Device;

import static android.view.View.GONE;

/**
 * Modulo Area Monitoraggio con Elenco Device
 * @author Magaldi Innova srl
 */
public class Area_Telemonitoraggio extends AppCompatActivity implements BluetoothUtils {

    private static final String LOG_NAME = "Area_Telemonitoraggio";

    //Caricamento Bluetooth
    private static final int REQUEST_ENABLE_BLUETOOTH = 1;
    private BluetoothSPP mSmoothBluetooth;
    private String MAC;
    private CountDownTimer discovery;
    private LinearLayout btn_esci;
    private GridView lista_device;
    private Button btn_annulla;
    private TextView progress_text;
    private LinearLayout btn_account, btn_cerca;
    private DatabaseHelper databaseHelper;
    private Context thisContext;
    private String device_da_cercare;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_area_telemonitoraggio);

        getSupportActionBar().setElevation(0);

        //Caricamento MAC
        Intent intent = getIntent();
        Bundle b = intent.getExtras();

        if (b != null) {
            MAC = b.getString("MAC");
        }

        btn_esci = (LinearLayout) findViewById(R.id.btn_esci);
        lista_device = (GridView) findViewById(R.id.lista_device);
        progress_text = (TextView) findViewById(R.id.progress_text);
        btn_annulla = (Button) findViewById(R.id.btn_annulla);
        btn_account = (LinearLayout) findViewById(R.id.btn_account);
        btn_cerca = (LinearLayout) findViewById(R.id.btn_cerca);

        thisContext = this;

        //SetTitle
        getSupportActionBar().setTitle("Area TeleMonitoraggio");


        btn_esci.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        // Create a new instance of Bluetooth
        mSmoothBluetooth = new BluetoothSPP(this);
        mSmoothBluetooth.setListener(mListener);

        //Aperture DB
        databaseHelper = new DatabaseHelper(this);
        //Caricamento Variabili
        List<Dispositivi> lista_dispositivi = databaseHelper.getAllDispositivi();
        Log.d(LOG_NAME, "SIZE DEVICE: " + String.valueOf(lista_dispositivi.size()));
        if (lista_dispositivi != null) {
            for (int i = 0; i < lista_dispositivi.size(); i++) {
                Log.d(LOG_NAME, "----------------------------------");
                Log.d(LOG_NAME, String.valueOf(lista_dispositivi.get(i).getID()));
                Log.d(LOG_NAME, lista_dispositivi.get(i).getNome());
                Log.d(LOG_NAME, lista_dispositivi.get(i).getMac());
            }
        }

        final ArrayList<Dispositivi> listaDevice = new ArrayList<>();
        listaDevice.add(new Dispositivi("Ossimetro", R.drawable.ossimetro));
        listaDevice.add(new Dispositivi("Sfigmomanometro", R.drawable.sfigmomanometro));
        listaDevice.add(new Dispositivi("Glucometro", R.drawable.glucometro));
        listaDevice.add(new Dispositivi("Bilancia", R.drawable.bilancia));

        lista_device.setAdapter(new Adapter_device(listaDevice));
        lista_device.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Object o = lista_device.getItemAtPosition(position);
                Dispositivi selezionato = (Dispositivi) o;
                if (selezionato.getNome().equals("Ossimetro")) {
                    device_da_cercare = selezionato.getNome();
                    mSmoothBluetooth.cancelDiscovery();
                    if (mSmoothBluetooth.isBluetoothEnabled()) {
                        Dispositivi Ossimetro = databaseHelper.getDispositivoByName("Ossimetro");
                        if (Ossimetro != null) {
                            progress_text.setVisibility(View.INVISIBLE);
                            btn_annulla.setVisibility(View.INVISIBLE);
                            Intent a = new Intent(Area_Telemonitoraggio.this, Ossimetro.class);
                            a.putExtra("MAC", MAC);
                            a.putExtra("Ossimetro", Ossimetro);
                            startActivity(a);
                        } else {
                            new android.app.AlertDialog.Builder(thisContext)
                                    .setTitle(R.string.ossimetro)
                                    .setMessage("Dispositivo non associato, inizio ricerca?")
                                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            discovery.start();
                                        }
                                    })
                                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {

                                        }
                                    })
                                    .show();

                        }
                    } else {
                        Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                        startActivityForResult(enableBluetooth, REQUEST_ENABLE_BLUETOOTH);
                    }
                }
                if (selezionato.getNome().equals("Sfigmomanometro")) {
                    device_da_cercare = selezionato.getNome();
                    mSmoothBluetooth.cancelDiscovery();
                    if (mSmoothBluetooth.isBluetoothEnabled()) {
                        Dispositivi Sfigmomanometro = databaseHelper.getDispositivoByName("Sfigmomanometro");
                        if (Sfigmomanometro != null) {
                            progress_text.setVisibility(View.INVISIBLE);
                            btn_annulla.setVisibility(View.INVISIBLE);
                            Intent a = new Intent(Area_Telemonitoraggio.this, Sfigmomanometro.class);
                            a.putExtra("MAC", MAC);
                            a.putExtra("Sfigmomanometro", Sfigmomanometro);
                            startActivity(a);
                        } else {
                            new android.app.AlertDialog.Builder(thisContext)
                                    .setTitle(R.string.sfigmomanometro)
                                    .setMessage("Dispositivo non associato, inizio ricerca?")
                                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            discovery.start();
                                        }
                                    })
                                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {

                                        }
                                    })
                                    .show();
                        }
                    } else {
                        Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                        startActivityForResult(enableBluetooth, REQUEST_ENABLE_BLUETOOTH);
                    }
                }
                if (selezionato.getNome().equals("Glucometro")) {
                    device_da_cercare = selezionato.getNome();
                    mSmoothBluetooth.cancelDiscovery();
                    if (mSmoothBluetooth.isBluetoothEnabled()) {
                        Dispositivi Glucometro = databaseHelper.getDispositivoByName("Glucometro");
                        if (Glucometro != null) {
                            progress_text.setVisibility(View.INVISIBLE);
                            btn_annulla.setVisibility(View.INVISIBLE);
                            Intent a = new Intent(Area_Telemonitoraggio.this, Glucometro.class);
                            a.putExtra("MAC", MAC);
                            a.putExtra("Glucometro", Glucometro);
                            startActivity(a);
                        } else {
                            new android.app.AlertDialog.Builder(thisContext)
                                    .setTitle(R.string.glucometro)
                                    .setMessage("Dispositivo non Associato, inizio ricerca?")
                                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            discovery.start();
                                        }
                                    })
                                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {

                                        }
                                    })
                                    .show();
                        }
                    } else {
                        Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                        startActivityForResult(enableBluetooth, REQUEST_ENABLE_BLUETOOTH);
                    }
                }
                if (selezionato.getNome().equals("Bilancia")) {
                    device_da_cercare = selezionato.getNome();
                    mSmoothBluetooth.cancelDiscovery();
                    if (mSmoothBluetooth.isBluetoothEnabled()) {
                        Dispositivi Bilancia = databaseHelper.getDispositivoByName("Bilancia");
                        if (Bilancia != null) {
                            progress_text.setVisibility(View.INVISIBLE);
                            btn_annulla.setVisibility(View.INVISIBLE);
                            Intent a = new Intent(Area_Telemonitoraggio.this, Bilancia.class);
                            a.putExtra("MAC", MAC);
                            a.putExtra("Bilancia", Bilancia);
                            startActivity(a);
                        } else {
                            new android.app.AlertDialog.Builder(thisContext)
                                    .setTitle(R.string.bilancia)
                                    .setMessage("Dispositivo non Associato, inizio ricerca?")
                                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            discovery.start();
                                        }
                                    })
                                    .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {

                                        }
                                    })
                                    .show();
                        }
                    } else {
                        Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                        startActivityForResult(enableBluetooth, REQUEST_ENABLE_BLUETOOTH);
                    }
                }
            }
        });

        btn_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSmoothBluetooth.cancelDiscovery();
                Intent a = new Intent(Area_Telemonitoraggio.this, Area_Misurazioni.class);
                a.putExtra("Devices", listaDevice);
                startActivity(a);
            }
        });

        //Autoconnesione
        discovery = new CountDownTimer(0, 100) {
            public void onTick(long millisUntilFinished) {
                //Log.i("Area_Telemonitoraggio CountDown Thread: " + Thread.currentThread().getId(), getString(R.string.tryconnect) + (int) (millisUntilFinished / 1000) % 60);
            }

            public void onFinish() {
                Log.i("Telemonitoraggio", "Finished: Inizio Discovery");
                ;
                mSmoothBluetooth.doDiscovery();
                Thread.currentThread().interrupt();
            }
        };

        //Cerca Dispositivo
        btn_cerca.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                device_da_cercare = null;
                btn_annulla.setVisibility(GONE);
                discovery.start();
            }
        });

        //Annulla Ricerca Dispositivo
        btn_annulla.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progress_text.setText("");
                if (btn_annulla.getText().equals("Annulla")) {
                    btn_annulla.setVisibility(GONE);
                }
                if (btn_annulla.getText().equals("Riprova")) {
                    btn_annulla.setVisibility(GONE);
                    discovery.start();
                }
            }
        });

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.dispositivo_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.about:
                final AlertDialog.Builder alert = new AlertDialog.Builder(this);
                alert.setTitle("Area di Controllo");
                alert.setMessage("Inserire Password");
                final EditText input = new EditText(this);
                input.setTransformationMethod(PasswordTransformationMethod.getInstance());
                alert.setView(input);

                alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        String value = input.getText().toString();
                        if (!value.equals("admin")) {
                            String message = "Password non corretta.";
                            AlertDialog.Builder builder = new AlertDialog.Builder(thisContext);
                            builder.setTitle("Error");
                            builder.setMessage(message);
                            builder.setPositiveButton("OK", null);
                            builder.create().show();
                        } else {
                            Intent a = new Intent(thisContext, About.class);
                            startActivity(a);
                        }
                    }
                });
                alert.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSmoothBluetooth.stop();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mSmoothBluetooth.stop();
    }

    // Implementazione dei listener Bluetooth
    public final BluetoothSPP.Listener mListener = new BluetoothSPP.Listener() {

        @Override
        public void onBluetoothNotSupported() {
            //Toast.makeText(Ossimetro.this, "Bluetooth not found", Toast.LENGTH_SHORT).show();
            AlertDialog.Builder alert = new AlertDialog.Builder(Area_Telemonitoraggio.this);
            alert.setMessage(getString(R.string.nobluetooth));
            alert.setPositiveButton(getString(R.string.exit), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
            alert.setCancelable(false);
            alert.show();
        }

        @Override
        public void onBluetoothNotEnabled() {
            Intent enableBluetooth = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBluetooth, REQUEST_ENABLE_BLUETOOTH);
        }

        @Override
        public void onConnecting(Device device) {

        }

        @Override
        public void onConnected(Device device) {

        }

        @Override
        public void onDisconnected() {

        }

        @Override
        public void onConnectionFailed(Device device) {

        }

        @Override
        public void onDiscoveryStarted() {
            progress_text.setText(getString(R.string.discovery_started));
            progress_text.setVisibility(View.VISIBLE);
            btn_annulla.setVisibility(View.GONE);
            lista_device.setEnabled(false);
            btn_account.setEnabled(false);
            btn_cerca.setEnabled(false);
            btn_esci.setEnabled(false);
        }

        @Override
        public void onDiscoveryFinished() {

        }

        @Override
        public void onNoDevicesFound() {
            progress_text.setText(getString(R.string.discovery_notfound));
            btn_annulla.setText("Riprova");
            progress_text.setVisibility(View.VISIBLE);
            btn_annulla.setVisibility(View.VISIBLE);
            lista_device.setEnabled(true);
            btn_account.setEnabled(true);
            btn_cerca.setEnabled(true);
            btn_esci.setEnabled(true);

        }

        public void onDevicesFound(final List<Device> deviceList, BluetoothSPP.ConnectionCallback connectionCallback) {
            final Device[] device = new Device[1];
            progress_text.setVisibility(View.INVISIBLE);
            btn_annulla.setVisibility(View.GONE);
            lista_device.setEnabled(true);
            btn_account.setEnabled(true);
            btn_cerca.setEnabled(true);
            btn_esci.setEnabled(true);

            final List<Device> filtereDeviceList = checkListBluetooth(deviceList);
            if (device_da_cercare == null) {
                final MaterialDialog dialog;
                dialog = new MaterialDialog.Builder(Area_Telemonitoraggio.this)
                        .title("Seleziona Device : ")
                        .adapter(new DevicesAdapter(Area_Telemonitoraggio.this, filtereDeviceList), null)
                        .build();

                ListView listView = dialog.getListView();
                if (listView != null) {
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            device[0] = deviceList.get(position);
                            Log.e(LOG_NAME, "DEVICE: " + device[0].getName() + " " + device[0].getAddress());
                            Log.i(LOG_NAME, "AGGIUNGO DEVICE AL SISTEMA");
                            Dispositivi nuovoDispositivi = new Dispositivi();
                            if (device[0].getAddress().startsWith(BluetoothUtils.MAC_OSSIMETRO)) {
                                device_da_cercare = BluetoothUtils.OSSIMETRO;
                            } else if (device[0].getAddress().startsWith(BluetoothUtils.MAC_SFIGMOMANOMETRO)) {
                                device_da_cercare = BluetoothUtils.SFIGMOMANOMETRO;
                            } else if (device[0].getAddress().startsWith(BluetoothUtils.MAC_GLUCOMETRO)) {
                                device_da_cercare = BluetoothUtils.GLUCOMETRO;
                            } else if (device[0].getAddress().startsWith(BluetoothUtils.MAC_BILANCIA)) {
                                device_da_cercare = BluetoothUtils.BILANCIA;
                            }
                            nuovoDispositivi.setNome(device_da_cercare);
                            databaseHelper.resetDevice(device_da_cercare);
                            nuovoDispositivi.setMac(device[0].getAddress());
                            databaseHelper.createDevice(nuovoDispositivi);
                            startActivity(nuovoDispositivi);
                            dialog.dismiss();
                        }

                    });
                }
                dialog.show();

                if (device[0] != null) {
                    Log.i(LOG_NAME, "AGGIUNGO DEVICE AL SISTEMA");
                    Dispositivi nuovoDispositivi = new Dispositivi();
                    if (device[0].getAddress().startsWith(BluetoothUtils.MAC_OSSIMETRO)) {
                        nuovoDispositivi.setNome(BluetoothUtils.OSSIMETRO);
                        databaseHelper.resetDevice(BluetoothUtils.OSSIMETRO);
                    } else if (device[0].getAddress().startsWith(BluetoothUtils.MAC_SFIGMOMANOMETRO)) {
                        nuovoDispositivi.setNome(BluetoothUtils.SFIGMOMANOMETRO);
                        databaseHelper.resetDevice(BluetoothUtils.SFIGMOMANOMETRO);
                    } else if (device[0].getAddress().startsWith(BluetoothUtils.MAC_GLUCOMETRO)) {
                        nuovoDispositivi.setNome(BluetoothUtils.GLUCOMETRO);
                        databaseHelper.resetDevice(BluetoothUtils.GLUCOMETRO);
                    } else if (device[0].getAddress().startsWith(BluetoothUtils.MAC_BILANCIA)) {
                        nuovoDispositivi.setNome(BluetoothUtils.BILANCIA);
                        databaseHelper.resetDevice(BluetoothUtils.BILANCIA);
                    }
                    nuovoDispositivi.setMac(device[0].getAddress());

                    databaseHelper.createDevice(nuovoDispositivi);
                    startActivity(nuovoDispositivi);
                }
            }
        }

        @Override
        public void onDataReceived(int[] data) { }

        @Override
        public void onDeviceFound(BluetoothDevice device, Context mContext, BroadcastReceiver mReceiver) {
            lista_device.setEnabled(true);
            btn_account.setEnabled(true);
            btn_cerca.setEnabled(true);
            btn_esci.setEnabled(true);
            Dispositivi nuovoDispositivi = new Dispositivi();
            if (device.getAddress().startsWith(BluetoothUtils.MAC_OSSIMETRO)) {
                if (device_da_cercare == BluetoothUtils.OSSIMETRO) {
                    Log.e(LOG_NAME, "DEVICE: " + device.getName() + " " + device.getAddress());
                    Log.i(LOG_NAME, "AGGIUNGO OSSIMETRO AL SISTEMA");
                    nuovoDispositivi.setNome(device_da_cercare);
                    databaseHelper.resetDevice(device_da_cercare);
                    nuovoDispositivi.setMac(device.getAddress());
                    databaseHelper.createDevice(nuovoDispositivi);
                    mContext.unregisterReceiver(mReceiver);
                    startActivity(nuovoDispositivi);
                }
            } else if (device.getAddress().startsWith(BluetoothUtils.MAC_SFIGMOMANOMETRO)) {
                if (device_da_cercare == BluetoothUtils.SFIGMOMANOMETRO) {
                    Log.e(LOG_NAME, "DEVICE: " + device.getName() + " " + device.getAddress());
                    Log.i(LOG_NAME, "AGGIUNGO SFIGMOMANOMETRO AL SISTEMA");
                    nuovoDispositivi.setNome(device_da_cercare);
                    databaseHelper.resetDevice(device_da_cercare);
                    nuovoDispositivi.setMac(device.getAddress());
                    databaseHelper.createDevice(nuovoDispositivi);
                    mContext.unregisterReceiver(mReceiver);
                    startActivity(nuovoDispositivi);
                }
            } else if (device.getAddress().startsWith(BluetoothUtils.MAC_GLUCOMETRO)) {
                if (device_da_cercare == BluetoothUtils.GLUCOMETRO) {
                    Log.e(LOG_NAME, "DEVICE: " + device.getName() + " " + device.getAddress());
                    Log.i(LOG_NAME, "AGGIUNGO GLUCOMETRO AL SISTEMA");
                    nuovoDispositivi.setNome(device_da_cercare);
                    databaseHelper.resetDevice(device_da_cercare);
                    nuovoDispositivi.setMac(device.getAddress());
                    databaseHelper.createDevice(nuovoDispositivi);
                    mContext.unregisterReceiver(mReceiver);
                    startActivity(nuovoDispositivi);
                }
            } else if (device.getAddress().startsWith(BluetoothUtils.MAC_BILANCIA)) {
                if (device_da_cercare == BluetoothUtils.BILANCIA) {
                    Log.e(LOG_NAME, "DEVICE: " + device.getName() + " " + device.getAddress());
                    Log.i(LOG_NAME, "AGGIUNGO BILANCIA AL SISTEMA");
                    nuovoDispositivi.setNome(device_da_cercare);
                    databaseHelper.resetDevice(device_da_cercare);
                    nuovoDispositivi.setMac(device.getAddress());
                    databaseHelper.createDevice(nuovoDispositivi);
                    mContext.unregisterReceiver(mReceiver);
                    startActivity(nuovoDispositivi);
                }
            }
        }
    };

    /**
     * Entro nel dispositivo Medico in base alla ricerca automatica
     * @param dispositivi
     */
    public void startActivity(final Dispositivi dispositivi) {
        if (dispositivi == null) {
            progress_text.setText("Dispositivi non trovato nel Database");
        } else {
            Log.i(LOG_NAME, "DISPOSITIVI TROVATI: " + dispositivi.getNome());
            CountDownTimer countdownActivity = new CountDownTimer(1000, 1000) {
                public void onTick(long millisUntilFinished) {
                    progress_text.setVisibility(View.VISIBLE);
                    progress_text.setText("Trovato " + dispositivi.getNome());
                    btn_annulla.setText("Annulla");
                    btn_annulla.setVisibility(View.VISIBLE);
                }

                @Override
                public void onFinish() {
                    progress_text.setVisibility(View.INVISIBLE);
                    btn_annulla.setVisibility(View.INVISIBLE);
                    Thread.currentThread().interrupt();
                    Intent a = null;
                    if (dispositivi.getNome().equals(BluetoothUtils.OSSIMETRO)) {
                        a = new Intent(Area_Telemonitoraggio.this, Ossimetro.class);
                        a.putExtra("Ossimetro", dispositivi);
                        a.putExtra("MAC", MAC);
                    }
                    if (dispositivi.getNome().equals(BluetoothUtils.SFIGMOMANOMETRO)) {
                        a = new Intent(Area_Telemonitoraggio.this, Sfigmomanometro.class);
                        a.putExtra("Sfigmomanometro", dispositivi);
                        a.putExtra("MAC", MAC);
                    }
                    if (dispositivi.getNome().equals(BluetoothUtils.GLUCOMETRO)) {
                        a = new Intent(Area_Telemonitoraggio.this, Glucometro.class);
                        a.putExtra("Glucometro", dispositivi);
                        a.putExtra("MAC", MAC);
                    }
                    if (dispositivi.getNome().equals(BluetoothUtils.BILANCIA)) {
                        a = new Intent(Area_Telemonitoraggio.this, Bilancia.class);
                        a.putExtra("Bilancia", dispositivi);
                        a.putExtra("MAC", MAC);
                    }
                    device_da_cercare = null;
                    startActivity(a);
                }
            };
            countdownActivity.start();
        }
    }

    /**
     * Controllo Bluetooth Mac trovati e filtro i device medici
     * @param listaDevice device trovati
     * @return lista dei device medici trovati
     */
    public List<Device> checkListBluetooth(List<Device> listaDevice) {
        List<Integer> indexes = new ArrayList<>();
        List<Device> lista = listaDevice;
        for (Device device : lista) {
            if (device.getAddress().startsWith(BluetoothUtils.MAC_OSSIMETRO) ||
                    device.getAddress().startsWith(BluetoothUtils.MAC_SFIGMOMANOMETRO) ||
                    device.getAddress().startsWith(BluetoothUtils.MAC_GLUCOMETRO) ||
                    device.getAddress().startsWith(BluetoothUtils.MAC_BILANCIA)) {
            } else {
                indexes.add(listaDevice.indexOf(device));
            }
        }
        int cpt = 0;
        Iterator<Device> it = listaDevice.iterator();
        while (it.hasNext()) {
            it.next();
            if (indexes.contains(cpt)) {
                it.remove();
            }
            cpt++;
        }
        return lista;
    }
}
