package magaldinnova.techTablHealth.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

import magaldinnova.techTablHealth.R;
import magaldinnova.techTablHealth.database.dbHelper.DatabaseHelper;
import magaldinnova.techTablHealth.database.oggetti.DataPoint;
import magaldinnova.techTablHealth.database.oggetti.Dispositivi;
import magaldinnova.techTablHealth.webService.WebServiceTechTablHealthWS;

/**
 * Modulo Area Configurazione Admin
 * @author Magaldi Innova srl
 */
public class About extends AppCompatActivity {

    private static final String LOG_NAME = "Area_About";
    private EditText active_domain;
    private Context thisContext;
    private DatabaseHelper databaseHelper;
    private String strName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        if(getSupportActionBar() != null){
            getSupportActionBar().setElevation(0);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Area Misurazioni");
        }
        //Caricamento Elementi Grafici
        TextView value_idDispositivo = (TextView) findViewById(R.id.value_idDispositivo);
        TextView value_nomeDispositivo = (TextView) findViewById(R.id.value_nomeDispositivo);
        Button btn_inizializza = (Button) findViewById(R.id.btn_inizializza);
        Button btn_cancella = (Button) findViewById(R.id.btn_cancella);
        Button btn_remove_device = (Button) findViewById(R.id.btn_remove_device);
        active_domain = (EditText)findViewById(R.id.active_domain);
        Button btn_save_domain = (Button) findViewById(R.id.save_domain);

        //Dominio attivo
        active_domain.setText(WebServiceTechTablHealthWS.DOMAIN);

        btn_save_domain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                WebServiceTechTablHealthWS.DOMAIN = active_domain.getText().toString();
                Log.d(LOG_NAME, WebServiceTechTablHealthWS.DOMAIN);
                AlertDialog.Builder builder1 = new AlertDialog.Builder(thisContext);
                builder1.setMessage("Nuovo Dominio: " + WebServiceTechTablHealthWS.DOMAIN);
                builder1.setCancelable(true);
                builder1.setPositiveButton(
                        "Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });

        //SetTitle
        getSupportActionBar().setTitle("Area Controllo");
        value_idDispositivo.setText(getDeviceUniqueID(this));
        value_nomeDispositivo.setText(getPhoneName());

        thisContext = this;
        databaseHelper = new DatabaseHelper(this);

        btn_inizializza.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new android.app.AlertDialog.Builder(thisContext)
                        .setTitle("Inizializza App TechTablHealth")
                        .setMessage("Vuoi inizializzare l'app TechTablHealth\n(Cancellazione misurazioni e device associati)\nQuesta operazione non può essere annullata")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                InizializzaDevice task = new InizializzaDevice();
                                task.execute();

                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });

        btn_cancella.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialog.Builder(thisContext)
                        .setTitle("Cancella Rilevazioni")
                        .setMessage("Vuoi cancellare tutte le rilevazioni?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                               ResetMisurazioni task = new ResetMisurazioni();
                                task.execute();

                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });

        btn_remove_device.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<Dispositivi> elenco =  databaseHelper.getAllDispositivi();
                Log.d(LOG_NAME, "SIZE DEVICE: "+ String.valueOf(elenco.size()));
                AlertDialog.Builder builderSingle = new AlertDialog.Builder(thisContext);
                builderSingle.setTitle("Seleziona Dispositivo Associato:");
                if(elenco.size()==0){
                    builderSingle.setMessage("Nessun Dispositivo Associato!");
                }
                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(thisContext, android.R.layout.select_dialog_singlechoice);
                for (int i = 0; i < elenco.size(); i++) {
                    Log.d(LOG_NAME, "----------------------------------");
                    Log.d(LOG_NAME, String.valueOf(elenco.get(i).getID()));
                    Log.d(LOG_NAME, elenco.get(i).getNome());
                    Log.d(LOG_NAME, elenco.get(i).getMac());
                    arrayAdapter.add(elenco.get(i).getNome());
                }
                builderSingle.setNegativeButton("Annulla", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        strName = arrayAdapter.getItem(which);
                        AlertDialog.Builder builderInner = new AlertDialog.Builder(thisContext);
                        builderInner.setMessage("Rimuovere "+strName + "?");
                        builderInner.setTitle("Dissocia Dispositivo");
                        builderInner.setPositiveButton("SI", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,int which) {
                                RemoveDevice task = new RemoveDevice(strName);
                                task.execute();
                            }

                        });
                        builderInner.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        builderInner.show();
                    }
                });
                builderSingle.show();

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                if (getParentActivityIntent() == null) {
                    onBackPressed();
                } else {
                    NavUtils.navigateUpFromSameTask(this);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Get ID univoco del device
     * @param activity contesto
     * @return id
     */
    public String getDeviceUniqueID(Activity activity){
        @SuppressLint("HardwareIds") String device_unique_id = Settings.Secure.getString(activity.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        return device_unique_id;
    }

    /**
     * Get nome dispositivo
     * @return nome dispositivo
     */
    public String getPhoneName() {
        BluetoothAdapter myDevice = BluetoothAdapter.getDefaultAdapter();
        return myDevice.getName();
    }

    /**
     * Metodo di cancellazione di tutte le misurazioni
     */
    public class ResetMisurazioni extends AsyncTask<Void, Void, Boolean> {
        ProgressDialog progress;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = ProgressDialog.show(thisContext, "Cancellazione",
                    "Attendere...", true);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            databaseHelper.resetMisurazioni();
            ArrayList<DataPoint> listaSPO = databaseHelper.getElencoMisurazioniSPO();
            ArrayList<DataPoint> listaSPO_pulse = databaseHelper.getElencoMisurazioniSPO_Pulse();
            ArrayList<DataPoint> listaSYS = databaseHelper.getElencoMisurazioniBPM_Systolic();
            ArrayList<DataPoint> listaDIA = databaseHelper.getElencoMisurazioniBPM_Diastolic();
            ArrayList<DataPoint> listaGlucose = databaseHelper.getMisurazioniGLM_Glucose();
            ArrayList<DataPoint> listaW = databaseHelper.getMisurazioniWeight();
            if(listaSPO.size()==0 && listaSPO_pulse.size()==0 && listaSYS.size()==0 && listaDIA.size()==0 && listaGlucose.size()==0 && listaW.size()==0){
                return true;
            }else{
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if(aBoolean){
                progress.dismiss();
                new AlertDialog.Builder(thisContext)
                        .setTitle("Cancellazione")
                        .setMessage("Cancellazione Completata")
                        .setNeutralButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        }
    }

    /**
     * Metodo di inizializzazione - Cancello Tutte Misurazioni e Cancello Tutte le associazioni device
     */
    public class InizializzaDevice extends AsyncTask<Void, Void, Boolean> {
        ProgressDialog progress;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = ProgressDialog.show(thisContext, "Cancellazione",
                    "Attendere...", true);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            databaseHelper.resetDatabase();
            ArrayList<DataPoint> listaSPO = databaseHelper.getElencoMisurazioniSPO();
            ArrayList<DataPoint> listaSPO_pulse = databaseHelper.getElencoMisurazioniSPO_Pulse();
            ArrayList<DataPoint> listaSYS = databaseHelper.getElencoMisurazioniBPM_Systolic();
            ArrayList<DataPoint> listaDIA = databaseHelper.getElencoMisurazioniBPM_Diastolic();
            ArrayList<DataPoint> listaGlucose = databaseHelper.getMisurazioniGLM_Glucose();
            ArrayList<DataPoint> listaW = databaseHelper.getMisurazioniWeight();
            if(listaSPO.size()==0 && listaSPO_pulse.size()==0 && listaSYS.size()==0 && listaDIA.size()==0 && listaGlucose.size()==0 && listaW.size()==0){
                return true;
            }else{
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if(aBoolean){
                progress.dismiss();
                new android.app.AlertDialog.Builder(thisContext)
                        .setTitle("Inizializza App")
                        .setMessage("Inizializzazione Completata")
                        .setNeutralButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        }
    }

    /**
     * Disaccoppia un singolo device
     */
    public class RemoveDevice extends AsyncTask<Void, Void, Boolean> {
        String deviceName;

        public RemoveDevice(String deviceName){
            this.deviceName=deviceName;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            long result = databaseHelper.resetDevice(deviceName);
            if(result>0){
                return true;
            }else{
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            if(aBoolean){
                new android.app.AlertDialog.Builder(thisContext)
                        .setTitle("Rimozione Device")
                        .setMessage("Device Rimosso!!")
                        .setNeutralButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        }
    }

}
