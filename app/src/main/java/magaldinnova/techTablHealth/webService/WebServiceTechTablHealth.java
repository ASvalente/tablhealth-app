package magaldinnova.techTablHealth.webService;

import android.util.Log;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.SoapFault;
import org.ksoap2.serialization.MarshalFloat;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import magaldinnova.techTablHealth.database.oggetti.DataPoint;
import magaldinnova.techTablHealth.webService.oggetti.InsertDataMeasurementResponse;
import magaldinnova.techTablHealth.webService.oggetti.InsertMeasurementResponse;
import magaldinnova.techTablHealth.webService.oggetti.Rilevazione;

/**
 * Created by magaldinnova on 21/07/16.
 */

public class WebServiceTechTablHealth {

    private static Logger log = Logger.getLogger(WebServiceTechTablHealth.class.getName());

    public static String NAMESPACE;
    public String URL;

    public WebServiceTechTablHealth(){
        NAMESPACE= WebServiceTechTablHealthWS.NAMESPACE;
    }

    public InsertMeasurementResponse InsertMeasurement(Rilevazione P){
        NAMESPACE= WebServiceTechTablHealthWS.NAMESPACE;
        URL=WebServiceTechTablHealthWS.DOMAIN+WebServiceTechTablHealthWS.PATH;

        String soap_action = NAMESPACE.concat("InsertMeasurement");

        SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        soapEnvelope.implicitTypes = true;
        soapEnvelope.dotNet = true;
        SoapObject soapReq = new SoapObject(NAMESPACE, "InsertMeasurement");
        MarshalFloat marshalFloat = new MarshalFloat();
        marshalFloat.register(soapEnvelope);
        soapReq.addSoapObject(P.createSoapObject());
        Log.i("SOAP Request", soapReq.toString());

        soapEnvelope.setOutputSoapObject(soapReq);
        HttpTransportSE httpTransport = new HttpTransportSE(URL);
        try{
            httpTransport.call(soap_action, soapEnvelope);
            Object retObj = soapEnvelope.bodyIn;
            if (retObj instanceof SoapFault){
                SoapFault fault = (SoapFault)retObj;
                Exception ex = new Exception(fault.faultstring);
                log.log(Level.SEVERE, ex.getMessage(), ex);
            }else{
                SoapObject result=(SoapObject)retObj;
                if (result.getPropertyCount() > 0){
                    InsertMeasurementResponse resultVariable =  new InsertMeasurementResponse(result);
                    return resultVariable;
                }
            }
        }catch (IOException e) {
            Log.e("InsertMeasurement", e.getMessage());
            SoapObject result=new SoapObject();
            result.addProperty("InsertMeasurementResult",-1);
            result.addProperty("messaggio", "Errore IOException WS");
            InsertMeasurementResponse resultVariable =  new InsertMeasurementResponse(result);
            return resultVariable;
        } catch (XmlPullParserException e) {
            Log.e("InsertMeasurement", e.getMessage());
            SoapObject result=new SoapObject();
            result.addProperty("InsertMeasurementResult",-1);
            result.addProperty("messaggio", "Errore XmlPullParserException WS");
            InsertMeasurementResponse resultVariable =  new InsertMeasurementResponse(result);
            return resultVariable;
        }
        return null;
    }

    public InsertDataMeasurementResponse InsertDataMeasurement(DataPoint P, int ID, int idDispositivo){
        NAMESPACE= WebServiceTechTablHealthWS.NAMESPACE;
        URL=WebServiceTechTablHealthWS.DOMAIN+WebServiceTechTablHealthWS.PATH;

        String soap_action = NAMESPACE.concat("InsertDataMeasurement");

        SoapSerializationEnvelope soapEnvelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        soapEnvelope.implicitTypes = true;
        soapEnvelope.dotNet = true;
        SoapObject soapReq = new SoapObject(NAMESPACE, "InsertDataMeasurement");
        MarshalFloat marshalFloat = new MarshalFloat();
        marshalFloat.register(soapEnvelope);
        soapReq.addProperty("datoRilevato", P.createSoapObject(ID, idDispositivo));
        Log.i("SOAP Request", soapReq.toString());

        soapEnvelope.setOutputSoapObject(soapReq);
        HttpTransportSE httpTransport = new HttpTransportSE(URL);
        try {
            httpTransport.call(soap_action, soapEnvelope);
            Object retObj = soapEnvelope.bodyIn;
            if (retObj instanceof SoapFault){
                SoapFault fault = (SoapFault)retObj;
                Exception ex = new Exception(fault.faultstring);
                log.log(Level.SEVERE, ex.getMessage(), ex);
            }else{
                SoapObject result=(SoapObject)retObj;
                Log.e("WS", result.toString());
                if (result.getPropertyCount() > 0){
                    InsertDataMeasurementResponse resultVariable =  new InsertDataMeasurementResponse(result);
                    return resultVariable;
                }
            }
        } catch (IOException e) {
            Log.e("InsertDataMeasurement", e.getMessage());
            SoapObject result=new SoapObject();
            result.addProperty("InsertMeasurementResult",false);
            result.addProperty("messaggio", "Errore IOException WS");
            InsertDataMeasurementResponse resultVariable =  new InsertDataMeasurementResponse(result);
            return resultVariable;
        } catch (XmlPullParserException e) {
            Log.e("InsertDataMeasurement", e.getMessage());
            SoapObject result=new SoapObject();
            result.addProperty("InsertMeasurementResult",false);
            result.addProperty("messaggio", "Errore XmlPullParserException WS");
            InsertDataMeasurementResponse resultVariable =  new InsertDataMeasurementResponse(result);
            return resultVariable;
        }
        return null;
    }
}
