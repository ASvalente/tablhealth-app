package magaldinnova.techTablHealth.webService.oggetti;

import android.util.Log;

import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;

/**
 * Created by magaldinnova on 21/07/16.
 */

public class InsertDataMeasurementResponse implements InsertDataMeasurementResultWS{

    private boolean value;
    private String message;

    public InsertDataMeasurementResponse(SoapObject soapObject)
    {
        if (soapObject == null)
            return;
        if (soapObject.hasProperty(insertMeasurementResult))
        {
            Object obj = soapObject.getProperty(insertMeasurementResult);
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)){
                SoapPrimitive j =(SoapPrimitive) obj;
                value = Boolean.parseBoolean(j.toString());
            }else if (obj!= null && obj instanceof Boolean){
                value = (Boolean) obj;
            }
            Log.e(insertMeasurementResult, String.valueOf(value));
        }
        if (soapObject.hasProperty(InsertDataMeasurementResultWS.message))
        {
            Object obj = soapObject.getProperty(InsertMeasurementResultWS.message);
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)){
                SoapPrimitive j =(SoapPrimitive) obj;
                message = j.toString();
            }else if (obj!= null && obj instanceof String){
                message = (String) obj;
            }
            Log.e(InsertMeasurementResultWS.message, message);
        }
    }

    public boolean isValue() {
        return value;
    }

    public void setValue(boolean value) {
        this.value = value;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
