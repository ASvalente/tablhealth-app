package magaldinnova.techTablHealth.webService.oggetti;

/**
 * Created by magaldinnova on 21/07/16.
 */

import org.ksoap2.serialization.SoapObject;

import java.io.Serializable;
import java.util.List;

import magaldinnova.techTablHealth.database.oggetti.DataPoint;
import magaldinnova.techTablHealth.webService.WebServiceTechTablHealthWS;

public class Rilevazione implements Serializable {

    public int iD;
    public int idAssisted;
    public int idPai;
    public String idDispositivo;
    public String type;
    public String dbId;
    public String csState;
    public String btAddress;
    public String boxTime;
    public String deviceSN;
    public List<DataPoint> datiRilevazione;

    public Rilevazione() {
    }

    public Rilevazione(int iD, int idAssisted, int idPai, String idDispositivo, String type, String dbId, String csState, String btAddress, String boxTime, String deviceSN, List<DataPoint> datiRilevazione) {
        this.iD = iD;
        this.idAssisted = idAssisted;
        this.idPai = idPai;
        this.idDispositivo = idDispositivo;
        this.type = type;
        this.dbId = dbId;
        this.csState = csState;
        this.btAddress = btAddress;
        this.boxTime = boxTime;
        this.deviceSN = deviceSN;
        this.datiRilevazione = datiRilevazione;
    }

    public int getiD() {
        return iD;
    }

    public void setiD(int iD) {
        this.iD = iD;
    }

    public int getIdAssisted() {
        return idAssisted;
    }

    public void setIdAssisted(int idAssisted) {
        this.idAssisted = idAssisted;
    }

    public int getIdPai() {
        return idPai;
    }

    public void setIdPai(int idPai) {
        this.idPai = idPai;
    }

    public String getIdDispositivo() {
        return idDispositivo;
    }

    public void setIdDispositivo(String idDispositivo) {
        this.idDispositivo = idDispositivo;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDbId() {
        return dbId;
    }

    public void setDbId(String dbId) {
        this.dbId = dbId;
    }

    public String getCsState() {
        return csState;
    }

    public void setCsState(String csState) {
        this.csState = csState;
    }

    public String getBtAddress() {
        return btAddress;
    }

    public void setBtAddress(String btAddress) {
        this.btAddress = btAddress;
    }

    public String getBoxTime() {
        return boxTime;
    }

    public void setBoxTime(String boxTime) {
        this.boxTime = boxTime;
    }

    public String getDeviceSN() {
        return deviceSN;
    }

    public void setDeviceSN(String deviceSN) {
        this.deviceSN = deviceSN;
    }

    /**
     * Creazione SoapObject per invio rilevazioni
     * @return
     */
    public SoapObject createSoapObject(){
        String NAMESPACE = WebServiceTechTablHealthWS.NAMESPACE;
        SoapObject dati = new SoapObject(NAMESPACE, "rilevazione");
        dati.addProperty("ID", getiD());
        dati.addProperty("IdAssisted",getIdAssisted());
        dati.addProperty("IdPai", getIdPai());
        dati.addProperty("IdKit", "0");
        dati.addProperty("IdDispositivo", getIdDispositivo());
        dati.addProperty("Type", getType());
        dati.addProperty("DbId", getDbId());
        dati.addProperty("CsState", getCsState());
        dati.addProperty("BtAddress", getBtAddress());
        dati.addProperty("BoxTime", getBoxTime());
        dati.addProperty("DeviceSN", getDeviceSN());
        return dati;
    }

    public SoapObject createArraySoapObject(int idDispositivo){
        String NAMESPACE = WebServiceTechTablHealthWS.NAMESPACE;
        SoapObject dati = new SoapObject();
        for(int i=0;i<datiRilevazione.size();i++){
            dati.addProperty("DatoRilevato", datiRilevazione.get(i).createSoapObject(i, idDispositivo));
        }
        return dati;
    }


}
