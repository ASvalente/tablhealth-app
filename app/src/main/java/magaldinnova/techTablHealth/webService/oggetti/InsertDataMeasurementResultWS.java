package magaldinnova.techTablHealth.webService.oggetti;

/**
 * Created by magaldiinnova on 21/07/16.
 */

public interface InsertDataMeasurementResultWS {
    public String insertMeasurementResult = "InsertDataMeasurementResult";
    public String message = "messaggio";
}
