package magaldinnova.techTablHealth.webService.oggetti;

import android.util.Log;

import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;

/**
 * Created by magaldinnova on 21/07/16.
 */

public class InsertMeasurementResponse implements InsertMeasurementResultWS{

    private int value;
    private String message;

    public InsertMeasurementResponse(SoapObject soapObject)
    {
        if (soapObject == null)
            return;
        if (soapObject.hasProperty(InsertMeasurementResultWS.insertMeasurementResult))
        {
            Object obj = soapObject.getProperty(InsertMeasurementResultWS.insertMeasurementResult);
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)){
                SoapPrimitive j =(SoapPrimitive) obj;
                value = Integer.parseInt(j.toString());
            }else if (obj!= null && obj instanceof Integer){
                value = (int) obj;
            }
            Log.e(InsertMeasurementResultWS.insertMeasurementResult, String.valueOf(value));
        }
        if (soapObject.hasProperty(InsertMeasurementResultWS.message))
        {
            Object obj = soapObject.getProperty(InsertMeasurementResultWS.message);
            if (obj != null && obj.getClass().equals(SoapPrimitive.class)){
                SoapPrimitive j =(SoapPrimitive) obj;
                message = j.toString();
            }else if (obj!= null && obj instanceof String){
                message = (String) obj;
            }
            Log.e(InsertMeasurementResultWS.message, message);
        }
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
