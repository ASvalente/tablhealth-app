package magaldinnova.techTablHealth.webService.oggetti;

/**
 * Created by magaldiinnova on 21/07/16.
 */

public interface InsertMeasurementResultWS {
    public String insertMeasurementResult = "InsertMeasurementResult";
    public String message = "messaggio";
}
